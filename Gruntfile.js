module.exports = function(grunt) {

    grunt.initConfig({
        //pkg: grunt.file.readJSON('package.json'),
        nodewebkit: {
            options: {
                version:"0.12.3",
                build_dir: './public', // Where the build version of my node-webkit app is saved
                mac64: true, // We want to build it for mac
                //win: true, // We want to build it for win
                //linux32: false, // We don't need linux32
                //linux64: false ,// We don't need linux64
                mac_icns : './public/app/images/kipper_icon.icns'
            },
            src: './public/**/*' // Your node-webkit app
        },
    });

    grunt.loadNpmTasks('grunt-node-webkit-builder');
    grunt.registerTask('default', ['nodewebkit']);

};