/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'toGoSoftware\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-minus' : '&#xe000;',
			'icon-plus' : '&#xe001;',
			'icon-close' : '&#xe002;',
			'icon-chevron-left' : '&#xf053;',
			'icon-chevron-right' : '&#xf054;',
			'icon-exit' : '&#xe003;',
			'icon-minus-2' : '&#xe004;',
			'icon-plus-2' : '&#xe005;',
			'icon-folder' : '&#xe006;',
			'icon-phone' : '&#xe00c;',
			'icon-person' : '&#xe00d;',
			'icon-menu' : '&#xe00e;',
			'icon-group' : '&#xe00f;',
			'icon-mail' : '&#xe011;',
			'icon-directions' : '&#xe012;',
			'icon-user-add' : '&#xe013;',
			'icon-target' : '&#xe014;',
			'icon-disk' : '&#xe015;',
			'icon-trash' : '&#xf014;',
			'icon-list-alt' : '&#xf022;',
			'icon-refresh' : '&#xf021;',
			'icon-print' : '&#xf02f;',
			'icon-chevron-up' : '&#xf077;',
			'icon-chevron-down' : '&#xf078;',
			'icon-group-2' : '&#xf0c0;',
			'icon-pencil' : '&#xe016;',
			'icon-location' : '&#xe017;',
			'icon-bell' : '&#xe018;',
			'icon-stopwatch' : '&#xe019;',
			'icon-spinner' : '&#xe01a;',
			'icon-spinner-2' : '&#xe01b;',
			'icon-spinner-3' : '&#xe01c;',
			'icon-spinner-4' : '&#xe01d;',
			'icon-spinner-5' : '&#xe01e;',
			'icon-spinner-6' : '&#xe01f;',
			'icon-cog' : '&#xe020;',
			'icon-key' : '&#xe021;',
			'icon-enter' : '&#xe022;',
			'icon-checkmark' : '&#xe023;',
			'icon-camera' : '&#xe007;',
			'icon-image' : '&#xe010;',
			'icon-file' : '&#xe024;',
			'icon-search' : '&#xe025;',
			'icon-menu-2' : '&#xe026;',
			'icon-profile' : '&#xe027;',
			'icon-user' : '&#xe028;',
			'icon-office' : '&#xe029;',
			'icon-notebook' : '&#xe02a;',
			'icon-truck' : '&#xe02b;',
			'icon-cross' : '&#xe02c;',
			'icon-plus-3' : '&#xe02d;',
			'icon-minus-3' : '&#xe02e;',
			'icon-folder-open' : '&#xf07c;',
			'icon-tags' : '&#xf02c;',
			'icon-screen' : '&#xe008;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};