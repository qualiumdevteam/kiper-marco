var app = app || {};
var ColorTimeCollection = Backbone.Collection.extend({
    model: app.ColorTime
});

var defaults = [
    { color: 'green', from: 0, to: 15 },
    { color: 'yellow', from: 16, to: 30 },
    { color: 'orange', from: 31, to: 45 },
    { color: 'red', from: 46, to: 0 }
];

app.ColorsTime = new ColorTimeCollection(defaults);