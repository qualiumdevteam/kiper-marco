var app = app || {};
var PedidosProductosCollection = Backbone.Collection.extend({

    model: app.PedidoProducto,
    url : app.resourcesDir+"/dbfiles/pedidos_productos.json",
   // localStorage: new Backbone.LocalStorage('pedidos-productos-togo'),
    sync : function(method,model,options){
        //console.log(method,model)
        if(method === "read"){
            var db = fs.readFileSync(this.url);
             if (db.length>0) {
            this.reset(JSON.parse(db.toString()))
            //console.log(JSON.parse(db.toString()))
        }
        }

        if(method === "set"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }

        if(method === "remove"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }
    },

    create : function(attr){
        // console.log("create",attr);
        //primero se llama el methodo add para añadirlo a la coleccion
        //despues se llama el sync
        var model = this.add(attr);
        this.sync("set",this,attr);
        return model;
    },
    fieldOrder: 'name',
    order: 'ASC',

    comparator: function( pedidoProducto ) {
        if (this.order === 'ASC')
            return pedidoProducto.get( this.fieldOrder );
        else
            return ! pedidoProducto.get( this.fieldOrder );
    },

    orderBy: function(field, order) {
        this.fieldOrder = field;
        this.order = order;
        this.sort();
    },

    resetOrder: function() {
        this.fieldOrder = 'name';
        this.order = 'ASC';
        this.sort();
    }
});

app.PedidosProductos = new PedidosProductosCollection();