var app = app || {};
app.bitacoraItemView = Backbone.View.extend({
    tagName: 'tr',
    events: {},
    initialize: function() {
       return this;
    },
    render: function() {
    app.Helper.loadTemplate( 'list_day', this.model.toJSON(), this.$el );
   return this; 
    },
     fecha:function(){
        var date = new Date();
        mes = date.getDate();
        dia = date.getMonth()+1;
        año = date.getFullYear();
       var fe = (mes+"/"+dia+"/"+año);
        return fe
    },
});
var app = app || {};
app.DashboardView = Backbone.View.extend({

    el: '.section[data-section="dashboard"]',

    events: {
        "click .change-status1": "changeStatus1",
        "click .change-status2": "changeStatus2"
    },
  changeStatus1: function(e) {
        var x = $(e.currentTarget)[0];
        var id = $(x).data("id");
         var data = $(e.currentTarget)[0].id;
       if (data == 1) {
          e.preventDefault();
        app.Helper.showPopup({
            id: 'popup-change-pedido-status',
            msg: 'PedidoChangeStatusView',
            msgType: 'info',
            closable: true,
            data: { id: id, actions: 1 }
        });
       };
    },
    changeStatus2: function(e) {
        var x = $(e.currentTarget)[0];
        var id = $(x).data("id");
        var data = $(e.currentTarget)[0].id;
   if (data == 5) { 
          e.preventDefault();
        app.Helper.showPopup({
            id: 'popup-change-pedido-status',
            msg: 'PedidoChangeStatusView',
            msgType: 'info',
            closable: true,
            data: { id: id, actions: 1 }
        });
    };
    },

    getPedidos: function() {
        var date = new Date();
        mes = date.getDate();
        dia = date.getMonth()+1;
        año = date .getFullYear();
        fe = (mes+"/"+dia+"/"+año);
        var pedidos1 = app.Pedidos.where({status: 1});
        var pedidos2 = app.Pedidos.where({status: 2});
        var pedidos3 = app.Pedidos.where({status: 5});
        var pedidos = [];
         if (pedidos1.length>0 ) {
            for (var i = 0; i < pedidos1.length; i++) {
                 if (pedidos1[i].attributes.fechaLocale == fe) {
                       pedidos.push(pedidos1[i]);
            };
            };
        };
        if (pedidos2.length>0 ) {
            for (var i = 0; i < pedidos2.length; i++) {
                if (pedidos2[i].attributes.fechaLocale == fe) {
                pedidos.push(pedidos2[i]);
            };
        }
        };
        if (pedidos3.length>0 ) {
            for (var i = 0; i < pedidos3.length; i++) {
                if (pedidos3[i].attributes.fechaLocale == fe) {
                pedidos.push(pedidos3[i]);
            }
            };
        };
        var now = new Date();
        var today = new Date( (now.getMonth() + 1).toString() + '/' + (now.getDate()).toString() + '/' + (now.getFullYear()).toString());
        return _.filter(pedidos, function(pedido) {
            return pedido.get('fecha') > today.getMilliseconds();
        });
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$dayListWrapper = this.$('#day-list');
        this.$lastOrderWrapper = this.$('#last-order-wrapper');

        this.listenTo( app.Pedidos, "change", this.renderDayList );
        this.listenTo( app.Pedidos, "create", this.renderDayList );
        this.listenTo( app.Pedidos, "destroy", this.renderDayList );

        this.listenTo( app.Pedidos, "change", this.renderLastOrder );
        this.listenTo( app.Pedidos, "create", this.renderLastOrder );
        this.listenTo( app.Pedidos, "destroy", this.renderLastOrder );

        this.renderDayList();
        this.renderLastOrder();
        this.$List = this.$('.listaday');
         if (app.Pedidos.models.length > 0){
         this.renderlistado();
    }
    },

    render: function (){
        app.Helper.loadTemplate( 'sections/dashboard', {}, this.$el );
        return this;
    },

    renderDayList: function() {
        this.$dayListWrapper.html('');
        var pedid = this.getPedidos();
        var $this = this;
        var getPedidos = function(){
        $this.$dayListWrapper.html('');
        var date = new Date();
        mes = date.getDate();
        dia = date.getMonth()+1;
        año = date .getFullYear();
        fe = (mes+"/"+dia+"/"+año);
        for (var i = 0; i < pedid.length; i++) {
            if (pedid[0].attributes.fechaLocale != fe) {
            pedid.splice(pedid[0]);
            };
        };
            _.each(pedid, function(pedido) {
                var ped = pedido.toJSON();
                var old = new Date(ped.fecha);
                // console.log(old)
                var current = new Date().getTime();
                // console.log(current)
                var sub = (current - old) / 1000;
                // $this.secondsToString(sub)
                app.Helper.appendTemplate('pedido_day_list_item', ped, $this.$dayListWrapper);
                // $('#HoraSalida-'+ped.id).html('Salió hace '+$this.secondsToString(sub))
            });
        };
        //secondsToString(sub);
        setInterval(function(){
            getPedidos();
        },200000)

       getPedidos();

    },
    // renderitem: function(modelo){
    //     var itemView = new app.bitacoraItemView({model:modelo});
    //     $(".listaday").append(itemView.render().$el);
    // },

    renderlistado: function() {
    $(".listaday").html(' ');
    app.PedidosProductos.each(function(modelo){
       var pedidoStatus =  app.Pedidos.findWhere({id: modelo.get('pedidoID')}).get('status');
        if (pedidoStatus != "3" && pedidoStatus != "4") {
          // $(".listaday").append( new app.bitacoraItemView({model:modelo}).render().$el);
        var fecha = this.fecha();
        if( app.Pedidos.findWhere({id: modelo.get('pedidoID')}).get('fechaLocale') == fecha){
            $(".listaday").append( new app.bitacoraItemView({model:modelo}).render().$el);
        }
        };
     },this);
    //
    subtot = $(".subtotalDash");
    var total = 0;
    for (var i = 0; i < subtot.length; i++) {
       var x = subtot[i].textContent.replace("$","");
       total = total + Number(x);
    };
    $("#total").text(total);
    },

    fecha:function(){
        var date = new Date();
        mes = date.getDate();
        dia = date.getMonth()+1;
        año = date.getFullYear();
       var fe = (mes+"/"+dia+"/"+año);
        return fe
    },

    renderLastOrder: function() {
        this.$lastOrderWrapper.html('');
        this.$lastOrderWrapper.html( new app.LastOrderView().render().el );
    },

    secondsToString : function(seconds){
        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
        var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
        var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;
        var text = "";
        if(numdays > 0){
            text += numdays + " dia(s) ";
        }
        if(numhours > 0){
            text += numhours + " hora(s) ";
        }
        if(numminutes > 0){
            text += numminutes + " minuto(s) ";
        }
        return text;
    }
});