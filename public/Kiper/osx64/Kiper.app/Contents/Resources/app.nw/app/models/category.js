var app = app || {};

app.Category = Backbone.Model.extend({
    defaults: {
        id : 0,
        name: "",
        description: ""
    },

    save : function(attrs,options){
        //console.log("categorias",attrs);
        this.set(attrs,{validate : true});
        this.collection.sync("set",this,attrs);
    },

    validate: function( attrs, options ) {
        if ( attrs.name.length === 0 || attrs.name === "")
            return 'El campo nombre es obligatorio';
    }
});