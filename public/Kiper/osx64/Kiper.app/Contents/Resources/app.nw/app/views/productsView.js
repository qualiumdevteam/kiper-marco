var app = app || {};

app.ProductsView = Backbone.View.extend({
    el: '#productos',
    
    templateForm: _.template($('#product-form-template').html()),
    templateDetails: _.template($('#product-details-template').html()),
    
    events: {
        "click #btn-add-product": "showForm",
        "click #btn-save-product": "createProduct",
        "click #btn-update-product": "updateProduct",
        "click #close-form": "restoreList",
        "click #change-image": "showImageField",
        "click #restore-image": "restoreImage",
        "click .edit": "editProduct",
        "click .details": "showProduct",
        // "change .inputFile": "input"
    },
    
    initialize: function() {
        this.$handler = this.$('#product-collection ul');
        app.Products.fetch();
        this.render();
    },
    
    render: function() {
        var $this = this;
        _.each( app.Products.models, function(model) {
            var producto = new app.ProductView();
            producto.model = model;
            $this.$handler.append(producto.render().el);
        } );
    },
    
    /* -------- */

    
    newAttributes: function(isUpdate,valueId) {
    	this.$form = this.$('#product-form');
        var newImg = this.uploadImage( this.$form );
        var max = _.max(app.Products.toJSON(),function(max){ return max.id });
        var id = 1;
        var newSrc = "";
        if(isUpdate && valueId != ""){
            id = valueId;
            if(newImg != ""){
                newSrc = newImg;
            }else{
                //get actual image of the product if user dont select one
                var oldImg = app.Products.get(id).get("image");
                newSrc = oldImg;
            }
        }else{
            id =  ( max.id > 0 ) ? (max.id + 1) : 1;
            newSrc = this.$form[0].image.value;
        }

	    return {
            id : parseInt(id),
            categoryID: parseInt(this.$form[0].categoriaID.value),
            code: this.$form[0].codigo.value,
            name: this.$form[0].nombre.value,
            description: this.$form[0].descripcion.value,
            price: this.$form[0].precio.value,
		    image: newSrc,
            available: this.$form.find('input[name="disponibilidad"]').val()
	    }
    },

    createProduct: function(e) {
    	e.preventDefault();
        console.log("aqui2");

        if ( this.$form[0].codigo.length === 0
            || this.$form[0].nombre.length === 0
            || this.$form[0].precio.length === 0){
            app.Helper.showAlert({
                msg: "No se han completado todos los campos necesarios",
                msgType: 'error'
            });
            return false;
        }else{
            app.Products.create(this.newAttributes());
            this.$('#product-collection').html('<ul></ul>');
            this.initialize();
            // $(".response").text("");
        }
    },

    editProduct: function(e) {
        e.preventDefault();
        var el = $(e.target),
            productID = el.closest('.row').data('id'),
            producto = app.Products.get(productID),
            data = {
                titulo: 'Editar Producto',
                producto: producto.toJSON()
            };
        this.$('#product-collection').html( this.templateForm(data) );
    },

    deleteProduct: function(e) {
        e.preventDefault();
        var el = $(e.target),
            productID = el.closest('.row').data('id'),
            producto = app.Products.get(productID);

        producto.destroy();
    },

    restoreList: function(e) {
        e.preventDefault();
        this.$('#product-collection').html('<ul></ul>');
        this.initialize();
    },

    restoreImage: function(e) {
        e.preventDefault();
        var el = $(e.target),
            wrapper = el.closest('#image-field'),
            producto = app.Products.get(wrapper.data('product'));

        wrapper.html('<img src="assets/uploads/'+ producto.attributes.image +'" width="100"> <a href="" id="change-image" class="tiny button secondary radius"><i class="icon-pencil"></i></a>');
    },
    
    showForm: function( e ) {
        e.preventDefault();
        var producto = new app.Product(),
            data = {
                titulo: 'Nuevo Producto',
                producto: producto.toJSON()
            };
        this.$('#product-collection').html( this.templateForm(data) );
    },

    showImageField: function(e) {
        e.preventDefault();
        var el = $(e.target),
            wrapper = el.closest('#image-field');

        wrapper.html('<input type="file" name="image" id="image"> <a href="" class="tiny button secondary radius" id="restore-image">Cancelar</a>');
    },

    showProduct: function(e) {
        e.preventDefault();
        var el = $(e.target),
            productID = el.closest('.row').data('id'),
            producto = app.Products.get(productID).toJSON();
        console.log(producto);
        this.$('#product-collection').html( this.templateDetails(producto) );
    },

    updateProduct: function(e) {
        console.log("aqui");
        e.preventDefault();
        this.$form = this.$('#product-form');
        var btn = $(e.target),
            productID = btn.closest('form').find('[name=id]').val(),
            producto = app.Products.get(productID),
            newAttributes = this.newAttributes(true,productID);

        if ( producto.attributes.image.length > 0 && newAttributes.image.length > 0 ) {
            var currentImagePath = app.resourcesDir + '/uploads/' + producto.attributes.image;
            if(app.Helper.fileExist(currentImagePath)){
                app.Helper.deleteFile(currentImagePath);
            }
        }

        producto.save(newAttributes);
        this.$('#product-collection').html('<ul></ul>');
        this.initialize();

    },

    uploadImage: function(form) {
        var input = form[0].image,
            file = input.files[0],
            source = file.path,
            name = file.name,
            self = this;
            console.log("dsfws");

        if(input != null && input != undefined && input != ""){

            $.when(app.Helper.uploadUserFile(source, name, false)).done(function(dir){
                return dir;
            });
        }



    }
});