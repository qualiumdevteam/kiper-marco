var app = app || {};
app.ClientePedidoView = Backbone.View.extend({
    tagName: 'tr',

    events: {
        "click td": "details"
    },

    details: function(e) {
        e.preventDefault();

    },

    initialize: function( model ) {
        if ( model ) this.model = model;
    },

    render: function() {
        var model = this.model.toJSON();

        app.Helper.loadTemplate( 'pedido_cliente_list_item', model, this.$el );

        return this;
    }
});