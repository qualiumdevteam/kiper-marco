var app = app || {};

app.PedidoProducto = Backbone.Model.extend({
    defaults: {
        pedidoID: 0,
        productoID: 0,
        price: 0.00,
        name: '',
        quantity: 1
    },

    save : function(attrs,options){
        //alert("save pedido_p")
        this.set(attrs,{validate : true});
        this.collection.sync("set",this,attrs);
    },

    toJSON: function() {
        this.attributes.subtotal = (parseFloat(this.attributes.price).toFixed(2) * parseInt(this.attributes.quantity));
        this.attributes.priceFormated = '$' + parseFloat( this.attributes.price).toFixed(2);
        this.attributes.subtotalFormated = '$' + parseFloat( this.attributes.subtotal).toFixed(2);
        return this.attributes;
    },

    validate: function( attrs, options ) {
        if ( attrs.pedidoID.length === 0
            || attrs.productoID.length === 0
            || attrs.price.length === 0
            || attrs.name.length === 0)
            return 'Deben ingresarse todos los datos';
    }
});