var app = app || {};

app.DetailNotifications = Backbone.Model.extend({
    defaults: {
        id:"",
        id_cliente:"",
        telefono:"",
        leido: "",
        correo: "",
        twitter: "",
        nombre:"",
        fecha:"",
    },

    save : function(attrs,options){
        //console.log("categorias",attrs);
        this.set(attrs,{validate : true});
        this.collection.sync("set",this,attrs);
    },
     validate: function( attrs, options ) {
    }
});