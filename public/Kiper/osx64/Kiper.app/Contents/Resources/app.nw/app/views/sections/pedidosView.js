var app = app || {};

app.PedidosView = Backbone.View.extend({

    el: '.section[data-section="pedidos"]',

    events: {
        "submit #filterForm": "filterList"
    },

    createItem: function() {
        app.Helper.showPopup({
            id: 'popup-nuevo-pedido',
            msg: 'PedidoNewView',
            msgType: 'clear',
            closable: true,
            size: 'xlarge'
        });
    },

    filterList: function(e) {
        e.preventDefault();
        var form = e.target;
        var textFilter = form.textFilter.value,
            filterType = form.filterType.value,
            fromDate   = form.fromDate.value,
            toDate     = form.toDate.value,
            todate     = new Date(),
            today      = new Date( todate.getTime() + ( ( 24 - todate.getHours() - 1 ) * ( 1000 * 60 * 60 ) ) );

        var pattern    = new RegExp(textFilter, "ig");

        if ( textFilter.length === 0 && fromDate.length === 0 && toDate.length === 0) {
            this.renderPedidos(true);
        }

        // Filtramos por texto de consulta
        if ( filterType === 'client' ) {

            var clients = _.filter( app.Clientes.toJSON(), function(cliente) {
                return pattern.test( cliente.nombres + ' ' + cliente.apellidos );
            } );

            var pedidosFiltered = _.filter( this.getPedidos(), function(pedido) {

                var valid = false;

                _.each( clients, function(client) {
                    if (client.id === pedido.clienteID)
                        valid = true;
                } );

                return valid;

            } );

            this.pedidos = pedidosFiltered;

        } else if ( filterType === 'user' ) {

            var users = _.filter( app.Usuarios.toJSON(), function(user) {
                return pattern.test( user.nombres + ' ' + user.apellidos );
            } );

            var pedidosFiltered = _.filter( this.getPedidos(), function(pedido) {
                var valid = false;

                _.each( users, function( user ) {
                    if (user.id === pedido.usuarioID)
                        valid = true;
                } );

                return valid;
            } );

            this.pedidos = pedidosFiltered;
        } else if ( filterType === 'folio' ) {
            var pedidosFiltered = _.filter( this.getPedidos(), function(pedido) {
                return parseInt(textFilter.trim()) === pedido.folio;
            } );

            this.pedidos = pedidosFiltered;
        }

        // Filtramos por los parámetros de fecha
        if ( fromDate.length !== 0 ) {
            var date = new Date(fromDate);
            var pedidosFiltered = _.filter( this.pedidos, function(pedido) {
                return pedido.fecha >= date.getTime();
                console.log("ok");
            } );
            this.pedidos = pedidosFiltered;
        }

        if ( toDate.length !== 0 ) {
            var date = new Date(toDate);
            var dateNite = new Date(date.getTime() + ( ( 24 - todate.getHours() - 1 ) * ( 1000 * 60 * 60 ) ));
            var pedidosFiltered = _.filter( this.pedidos, function(pedido) {
                return pedido.fecha <= dateNite.getTime();
                console.log("ok");
            } );

            this.pedidos = pedidosFiltered;
        }

        this.renderPedidos(false);
    },

    getPedidos: function() {
        var activeUser = app.Usuarios.get( app.userSession );
        var userType = app.Profiles.get( activeUser.get('perfil')).get('name');

        if ( userType === 'admin' )
            return _.filter( app.Pedidos.toJSON(), function(pedido) { return pedido.status === 1 || pedido.status === 2 || pedido.status === 5 } );
        else {
            return _.filter( app.Pedidos.toJSON(), function(pedido) { return (pedido.status === 1 || pedido.status === 2 || pedido.status === 5) && pedido.usuarioID === activeUser.get('id') } );
        }
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();

        this.$listHandler = this.$('#pedidos-list tbody');

        this.renderPedidos(true);

        /*this.$filterForm = this.$('#filterForm');
        this.$filterForm.find('input[name="fromDate"]').datepicker();
        this.$filterForm.find('input[name="toDate"]').datepicker();*/

        this.listenTo( app.Pedidos, "change", this.renderPedidos, true );
        this.listenTo( app.Pedidos, "create", this.renderPedidos, true );
        this.listenTo( app.Pedidos, "destroy", this.renderPedidos, true );
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/pedidos', null, this.$el );
        return this;
    },

    renderPedidos: function( reset ) {
        var $this = this;
        var reset = reset || false;

        if ( ! $this.pedidos || reset ) {
            $this.pedidos = _.filter( this.getPedidos(), function(pedido) {
                return pedido.status !== 0;
            } );
        }

        this.$listHandler.html('');
        //console.log($this.pedidos);
        _.each($this.pedidos, function(pedido){
            $this.$listHandler.append( new app.PedidoItemView( pedido ).render().el );
        });

        // console.dir( this.pedidos );
    }

});