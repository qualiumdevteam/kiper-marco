var app = app || {};
app.AgeRangeReportView = Backbone.View.extend({
    el: '#age-range',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();
    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_ageRange').val()),
            toDate = new Date(this.$form.find('#toDate_ageRange').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate());
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1);

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },

    getClientData : function(clienteID){
        var collection;
        var name = '', age = '';

        collection = _.where(app.Clientes.toJSON(),{ id : clienteID });

        _.each(collection,function(cliente){
            name = cliente.nombres+' '+cliente.apellidos;
            age = cliente.edad;
        });

        return {
            edad : name,
            value : age
        }
    },

    getData: function(from,to,month) {
        var dataChart = [];
        var ages = [];
        var data = [];
        var $this = this;
        var monthRange = month ? this.getMonthRange(month) : this.getDayRange(from,to);

        data = _.filter(app.Pedidos.toJSON(),function(pedido){
            var valid = false;

            if ( pedido.pagado === 1 && pedido.fecha >= monthRange.from.getTime() && pedido.fecha <= monthRange.to.getTime() )
                valid = true;

            return valid;
        });

        _.each(data,function(cliente){
            dataChart.push($this.getClientData(cliente.clienteID))
        });

        return dataChart;
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        app.Helper.loadTemplate('reports/age_range', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        this.renderChart();
        this.renderFilters();
        app.appView.colorReport();
        return this;

    },

    renderChart: function(from,to) {
        var data = this.getData(from,to,null);
        this.$canvas.html('');

        Morris.Bar({
            element: 'age-range-canvas',
            data: data,
            xkey: 'edad',
            ykeys: ['value'],
            labels: ['No'],
            barColors: ['rgba(243,170,28,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');

        Morris.Bar({
            element: 'age-range-canvas',
            data: data,
            xkey: 'edad',
            ykeys: ['value'],
            labels: ['No'],
            barColors: ['rgba(243,170,28,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = (fromMonth !== undefined) ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});