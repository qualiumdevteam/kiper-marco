var app = app || {};
app.MetodoPago = Backbone.Model.extend({
    defaults: {
        id : 0,
        name: ""
    },

    save : function(attrs,options){
        this.set(attrs);
        this.collection.sync("set",this,attrs);
    },

    validate: function( attrs, options ) {
        if ( attrs.name.length === 0 )
            return 'Debe igresar un nombre';
    }
});