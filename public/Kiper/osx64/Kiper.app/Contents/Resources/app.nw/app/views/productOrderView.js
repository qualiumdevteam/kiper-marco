var app = app || {};
app.ProductOrderView = Backbone.View.extend({
    tagName: 'tr',

    events: {
        "keyup input": "updateQuantity",
        "click .delete": "clearItem"
    },

    clearItem: function() {
        if (confirm('¿Esta segur@ que desea eliminar este elemento?')) {
            this.model.destroy();
            this.remove();
        }
    },

    initialize: function(model) {
        if (model) this.model = model;
    },

    render: function() {
        var model = this.model.toJSON();
        model.subtotal = model.quantity * model.price ;
        model.subtotal = model.subtotal.toFixed(2);
        model.price = parseFloat( model.price).toFixed(2);
        app.Helper.loadTemplate( 'product_order_list', model, this.$el );
        return this;
    },

    updateQuantity: function(e) {
        e.preventDefault();
        this.model.save({quantity: e.target.value});
        this.$('.subtotal').html( '$' + (this.model.get('quantity') * this.model.get('price')).toFixed(2) );
    }
});