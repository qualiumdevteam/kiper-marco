var app = app || {};
app.Sucursal = Backbone.Model.extend({

    defaults: {
        id : 0,
        name: '',
        rfc: '',
        fiscalName: '',
        address: '',
        email: '',
        phone: '',
        active: 0
    },

    save : function(attrs,options){
        this.set(attrs);
        this.collection.sync("set",this,attrs);
    },

    validate: function( attrs, options ) {
        if ( attrs.name.length === 0 )
            return 'El campo "Nombre" es obligatorio';
    },

    toJSON : function(){
        var json = _.clone(this.attributes);
        for(var attr in json) {
            if((json[attr] instanceof Backbone.Model) || (json[attr] instanceof Backbone.Collection)) {
                json[attr] = json[attr].toJSON();
            }
        }
        // alert(JSON.stringify(json))
        return json;
    }
});