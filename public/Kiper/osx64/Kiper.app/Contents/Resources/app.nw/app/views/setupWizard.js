var app = app || {};

var gui = require('nw.gui');
app.SetupWizard = Backbone.View.extend({
    activeProductNav: false,
    tagName: 'div',
    className: 'full-screen',
    events: {
        "submit #categoryForm": "createCategory",
        "submit #productForm": "createProduct",
        "click .wizard-nav .forward": "loadProductsScreen",
        "click .wizard-nav .reverse": "loadCategoriesScreen",
        "click #products-list .edit": "editProduct",
        "click #productForm .cancel": "cancelEdition",
        "click [data-section='products'] .forward": "finishWizard",
        "change #image":"Textimage"
    },
Textimage:function(e){
      var img = $(e.target)[0].files[0].name;
     if (img) {
            $(".response").text(img);
            $(".img").text(img);
       };
},
    addOne: function( category ) {
        var handler = this.$('#categories-list table tbody');
        handler.append( new app.CategoryView( category).render().el );
        this.form[0].reset();
    },

    cancelEdition: function(e) {
        e.preventDefault();
        app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON() }, this.$productForm );
        this.$productForm.removeClass('updating');
    },

    categoryAttributes: function() {
        var max = _.max(app.Categories.toJSON(),function(max){ return max.id });
        return {
            id : (max.id > 0) ? (max.id + 1) : 1 ,
            name: this.form[0].name.value,
            description: this.form[0].description.value
        }
    },

    catExistence: function() {
        var total = app.Categories.models.length;
        if ( total >= 1 ) {
            if ( ! this.activeProductNav ) {
                this.showProductNav();
            }
        } else {
            this.hideProductNav();
        }

        this.form[0].name.focus();
    },

    createCategory: function( e ) {
        e.preventDefault();
        console.log(this.form[0].name.value);
        if(this.form[0].name.value !== "" ){
            app.Categories.create( this.categoryAttributes(), { wait: true } );
        }else{
            app.Helper.showAlert( {
                msg: "El campo nombre es obligatorio",
                msgType: 'notice'
            } );
        }
    },

    deleteProduct: function( ) {
        if ( this.$productForm.hasClass('updating') )
            app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON() }, this.$productForm );
    },

    editProduct: function (e) {
        e.preventDefault();
        var link = $(e.target).closest('a'),
            productID = link.data('item'),
            product = app.Products.get(productID),
            form = this.$('#productForm'),
            data = {
                product: product.toJSON(),
                categories: app.Categories.toJSON()
            };

        form.addClass('updating');
        app.Helper.loadTemplate('product_list_edit_item', data, form);
    },

    finishWizard: function( e ) {
        e.preventDefault();
        this.undelegateEvents();
        var confirm = app.Helper.showConfirm({
            msg: 'El catalogo inicial ha sido cargado correctamete, la aplicación se reiniciará',
            msgType: 'info'
        });
        confirm.done( function() {  
            location.reload(); 
        });
    },

    hideFinish: function() {
        this.$prodSection.find('.wizard-nav-2 .forward').remove();
    },

    hideProductNav: function() {
        this.activeProductNav = false;
        this.$wizardNav.find('.forward').remove();
    },

    initialize: function() {
        this.render();
        this.$catSection = this.$('[data-section="categories"]');
        this.$prodSection = this.$('[data-section="products"]');
        app.Helper.loadTemplate( 'wizards_parts/categories', null, this.$catSection );
        this.form = this.$('#categoryForm');
        this.$wizardNav = this.$('.wizard-nav');
        this.listenTo( app.Categories, 'add', this.renderCategories );
        this.listenTo( app.Categories, 'add', this.catExistence );
        this.listenTo( app.Categories, 'change', this.catExistence );
        this.listenTo( app.Products, 'change', this.renderProducts );
        this.listenTo( app.Categories, 'remove', this.catExistence );
        this.listenTo( app.Products, 'add', this.renderProducts );
        this.listenTo( app.Products, 'add', this.productExistense );
        this.listenTo( app.Products, 'remove', this.renderProducts  );
        this.listenTo( app.Products, 'remove', this.productExistense );
        if (app.Categories.models.length > 0) {
            this.renderCategories();
            this.catExistence();
        }
    },

    loadCategoriesScreen: function() {
        var options = {
                speed: 500,
                direction: 'reverse'
            },
            $this = this;

        var anim = app.Helper.animateScreen( this.$prodSection, this.$catSection, options );
        anim.done( function() {
            $(".sect").removeClass('activo');
            $(".sect").addClass('inactivo');
           $(".sect[data-section='categories']").addClass('activo');
            $this.form[0].name.focus();
        });
    },

    loadProductsScreen: function() {
        var data = {},
            $this = this;
        data.categories = [];
        data.today = new Date().toDateString()
        _.each( app.Categories.models, function( model ){
            data.categories.push( model.toJSON() );
        } );
        this.$prodSection.html('');
        app.Helper.loadTemplate( 'wizards_parts/products', data, this.$prodSection );
        this.$prodSection.hide();
        var anim = app.Helper.animateScreen( this.$catSection, this.$prodSection, { speed: 500 } );
        anim.done( function() {
            $this.$productForm = $this.$('#productForm');
            $this.$productForm[0].code.focus();
          $(".sect").removeClass('activo');
          $(".sect").addClass('inactivo');
          $(".sect[data-section='products']").addClass('activo');

            if ( app.Products.models.length > 0 )
                $this.renderProducts();
        } );
    },

    productAttributes: function() {
        if ( this.$productForm[0].image.value == "" ){
            var fileSrc = "";
           }else{
            var fileSrc = app.Helper.uploadFile( this.$productForm[0].image);
           }
        var max = _.max(app.Products.toJSON(),function(max){ return max.id });
        var attrs = {
                id : (max.id > 0) ? (max.id + 1) : 1 ,
                categoryID: parseInt(this.$productForm[0].categoryID.value),
                code: this.$productForm[0].code.value,
                name: this.$productForm[0].name.value,
                description: this.$productForm[0].description.value,
                price: this.$productForm[0].price.value,
                creationDate: new Date().toDateString(),
                updateDate: new Date().toDateString(),
                image: fileSrc
            };
        return attrs;

    },

    productExistense: function() {
        if ( app.Products.models.length > 0 )
            this.showFinish();
        else
            this.hideFinish();
    },

    render: function() {
        app.Helper.loadTemplate( 'popups/categoriesProductsWizard', null, this.$el );
        return this;
    },

    renderCategories: function() {
        var categories = app.Categories.models,
            handler = this.$('#categories-list table tbody');
        handler.html('');
        this.form[0].reset();
        this.form[0].name.focus();
        _.each( categories, function( cat ){
            var catView = new app.CategoryView( cat );
            handler.append( catView.render().el );
        });
    },

    renderProducts: function() {
        var products = app.Products.models,
            handler = this.$('#products-list table tbody');
        handler.html('');
        this.$productForm[0].reset();
        _.each( products, function( prod ){
            var productView = new app.ProductView( prod );
            handler.append( productView.render().el );
        } );
        this.productExistense();
    },

    showFinish: function() {
        var forwardButton = this.$prodSection.find('.wizard-nav-2 .forward');
        if (forwardButton.length === 0)
            app.Helper.appendTemplate( 'forward_button', { text: 'Finalizar', style: 'success', size: 'medium' }, this.$prodSection.find('.wizard-nav-2') );
    },

    showProductNav: function() {
        var alert = app.Helper.showConfirm( {
                msg: 'Ya puede dar de alta productos, ¿Desea agregar productos ahora?',
                msgType: 'info',
                closable: true,
                id: 'info-add-products'
            }),
            $this = this;
        this.activeProductNav = true;
        app.Helper.loadTemplate( 'forward_button', { text: 'Productos' }, this.$wizardNav );
        alert.done( function() {
         $this.loadProductsScreen(); 
        });
    },

    createProduct: function( e ) {
        e.preventDefault();
        var self = this;
        var form = $(e.target);
        if ( form.hasClass('updating')){
            return this.updateProduct();
         }   
        if (this.$productForm[0].categoryID.value !== "" &&
            this.$productForm[0].code.value !== "" &&
            this.$productForm[0].name.value !== "" &&
            this.$productForm[0].price.value !== "" ){
            if(/^([0-9])*[.]?[0-9]*$/.test(this.$productForm[0].price.value)){
               app.Products.create( this.productAttributes(), { wait: true } );
                setTimeout(function(){
                    self.renderProducts();
                    $(".response").text("imagen");
                    $(".img").text("imagen");
                },2000);
             }else{  
                app.Helper.showAlert({
                msg: "EL campo de precio solo puede contener numeros",
                msgType: 'error'
            });
            
           }
          }else{
            app.Helper.showAlert({
                msg: "No se han completado todos los campos necesarios",
                msgType: 'error'
            });
                
        }
    },

    updateProduct: function() {
        var self = this;
         if(!/^([0-9])*[.]?[0-9]*$/.test(this.$productForm[0][5].value)){
            app.Helper.showAlert({
                msg: "EL campo de precio solo puede contener numeros",
                msgType: 'error'
            });
        }else{        
        var productID = this.$productForm[0].productID.value,
        product = app.Products.get( productID );
        product.save(this.newAttributes(true,productID));
        app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON() }, this.$productForm );
        this.$productForm.removeClass('updating');
        setTimeout(function(){
        self.renderProducts();
        $(".response").text("imagen");
        $(".img").text("imagen");
        },2000);
        }
    },
     newAttributes: function(isUpdate,prodId) {
        var id = 1;
        var max = _.max(app.Products.toJSON(),function(max){ return max.id });
        id = parseInt(prodId);
        var actualProd = app.Products.get(id);
        var actImg = actualProd.get("image");
        if(this.$productForm[0].image.value == ""){
              fileSrc = actImg;
          }else{
            var oldImg = app.Helper.fileExist(actImg);
            console.log(actImg);
            console.log(oldImg);
            if (oldImg){
                app.Helper.deleteFile(actImg);
            }
            fileSrc = app.Helper.uploadFile( this.$productForm[0].image);
            console.log(fileSrc);
            this.renderProducts();
          }
        var attrs = {
            id : parseInt(id),
            categoryID: parseInt(this.$productForm[0].categoryID.value),
            code: this.$productForm[0].code.value,
            name: this.$productForm[0].name.value,
            description: this.$productForm[0].description.value,
            price: this.$productForm[0].price.value,
            creationDate: actualProd.get("creationDate"),
            updateDate: new Date().toDateString(),
            image: fileSrc
        };
        return attrs;
    },

});