var app = app || {};

app.PedidoNewView = Backbone.View.extend({
    tagName: 'div',

    events: {
        "submit #phoneForm":         "searchClient",
        "submit #phoneFormSmall":    "searchClient",
        "submit #clientForm":        "updateClient",
        "click .edit-client":        "showForm",
        "click #clientForm .cancel": "closeForm",
        "click #products-list li":   "addProduct",
        "click a.save-order":        "saveOrder",
        "change #categoryID":        "filterProducts",
        "keyup input[name='recibe']": "calcChange",
        "keyup input[name='search']": "searchProduct",
        "click .close": "close"
    },

    addProduct: function(e) {
        var $li = $(e.target).closest('li');
        var model = app.Products.get($li.data('id'));
        var pedidoProducto = new app.PedidoProducto( this.attrsPedidoProducto( model ) );
        //console.log(pedidoProducto);

        // alert(this.pedido.attributes.id);

        var existent = app.PedidosProductos.findWhere( {
            productoID: pedidoProducto.get('productoID'),
            pedidoID: this.pedido.attributes.id
        } );

        if (existent){
            existent.set({ quantity: parseInt(existent.get('quantity')) + 1 });
        }
        else{
            app.PedidosProductos.create( pedidoProducto.toJSON() );
        }
        this.renderPedidoProductos();
        this.setTotal();

    },

    attrsBitacora: function() {
        var today = new Date();
        var user = app.Usuarios.get( app.userSession);
        var details = [];
        var productos = app.PedidosProductos.where({pedidoID: this.pedido.get('id')});

        _.each(productos, function(producto){
            details.push( producto.toJSON() );
        });

        return {
            action:   'create',
            date:     today.toLocaleDateString(),
            username: user.get('username'),
            sucursal: app.Sucursales.get( user.get('sucursalID')).get('name'),
            amount:   this.getTotal(),
            details:  JSON.stringify( details ),
            pedidoID: this.pedido.get('id')
        }
    },

    attrsPedido: function() {
        var today = new Date();
        var folio = 1;
        var pedidoID = 1;

        app.Pedidos.orderBy('folio', 'DESC');
        var last = app.Pedidos.models[0];
        if (last) folio = last.attributes.folio + 1;
        app.Pedidos.resetOrder();

        var id = /*app.Pedidos.models[0];*/ _.max(app.Pedidos.toJSON(),function(max){ return max.id; });

       // alert(JSON.stringify(id))

        if(id.id != null) pedidoID = (id.id + 1);
        else pedidoID = 1;

        return {
            id : pedidoID ,
            clienteID: 0,
            usuarioID: app.Usuarios.get(app.userSession).get('id'),
            folio: folio,
            horaCaptura: today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds(),
            horaEntrega: '00:00:00',
            horaEnvio: '00:00:00',
            comentarios: '',
            cliente : ""
        }

    },

    attrsPedidoProducto: function( producto ) {
        return {
            pedidoID: this.pedido.get('id'),
            productoID: producto.get('id'),
            price: producto.get('price'),
            name: producto.get('name'),
            quantity: 1
        }
    },

    calcChange: function(e) {
        e.preventDefault();

        this.setChange();
    },

    searchProduct : function(e){
        e.preventDefault();

        var input = $('#searchProd').val();
        var filter = _.filter(app.Products.toJSON(),function(prod){
           var valid = false;

            if(prod.name.indexOf(input) != -1 || prod.price.indexOf(input) != -1 || prod.description.indexOf(input) != -1){
                valid = true;
            }
            return valid;
        });

        app.Products.resort('name');
        this.$menuHandler.html('');

        app.Helper.loadTemplate( 'menu_item', {productos: filter}, this.$menuHandler );

        app.Products.resetOrder();


    },

    clearGarbage: function() {
        var opened = app.Pedidos.where({status:0});
        _.each(opened, function(model){
            var pedidoProductos = app.PedidosProductos.where({pedidoID: model.get('id')});
            _.each( pedidoProductos, function(producto) { producto.destroy() } );
            model.destroy();
        });
    },

    closeForm: function(e) {
        e.preventDefault();
        var clientID = $(e.target).closest('form')[0].id.value,
            client = app.Clientes.get(clientID);

        this.renderClient(client);
    },

    createPedido: function() {
        var pedido = new app.Pedido();
        //if (!pedido.validationError){
        app.Pedidos.create(this.attrsPedido());
        //console.log("atributos pedido nuevo => ",this.attrsPedido());
        var lastPedido = app.Pedidos.at(app.Pedidos.length-app.Pedidos.length);
        this.pedido = lastPedido;//pedido; this.pedido = pedido;
        //console.log("si aqui");
        //alert(JSON.stringify(this.pedido))

        // alert(this.pedido.attributes.id)
        // console.log("pedido actual => ",this.pedido)
        //console.log(this.pedido,"asignacion")
        //}

    },

    filterProducts: function(e) {
        e.preventDefault();
        var sel = $(e.target),
            catID = (sel.val() === 'all') ? undefined : parseInt(sel.val());
            //console.log(catID);
        this.renderMenu(catID);
    },

    getTotal: function() {
        var total = 0;
        var productos = app.PedidosProductos.where( {pedidoID: this.pedido.get('id')} );
        _.each( productos, function(producto){
            total += producto.get('price') * producto.get('quantity');
        } );

        return parseFloat( total ).toFixed(2);
    },

    initialize: function() {
        this.clearGarbage();
        this.createPedido();
        this.render();

        this.$phone = this.$('#phoneSearch');
        this.$popup = this.$('#popupContent').hide();
        this.$metodoPago = this.$('[name="metodoPago"]');
        this.$recibe = this.$('[name="recibe"]');
        this.$cambio = this.$('[name="cambio"]');
        this.$aplicarNo = this.$('#apl1');
        this.$aplicarSi = this.$('#apl2');
        this.$clientForm = this.$popup.find('#clientForm');
        this.$menuHandler = this.$popup.find('#products-list');
        this.$orderTable = this.$popup.find('#order-table');
        this.$totalHandler = this.$popup.find('#total-amount span');

        this.listenTo( app.PedidosProductos, "change", this.setTotal );
        this.listenTo( app.PedidosProductos, "destroy", this.setTotal );
    },

    render: function() {
        var data = {
            categories: app.Categories.toJSON(),
            metodos: app.MetodosPago.toJSON()
        };
        app.Helper.loadTemplate( 'popups/pedido_new', data, this.$el );

        return this;
    },

    renderClient: function(model, edit) {
        var cliente = model ? model.toJSON() : {};
        var self = this;

        if (cliente.id === undefined || (cliente.id !== undefined && edit !== undefined)) {
            cliente.ordering = true;
            app.Helper.loadTemplate( 'client_form', cliente, this.$clientForm );

            //var lastId = _.max(app.Clientes.toJSON(),function(max){ return max.id });

            //var clienteID = (lastId.id > 0) ? lastId.id : 1;

            self.pedido.save({ clienteID: 0 });

            var phone = this.$('#phoneFormSmall')[0].phone.value;
            this.$('#popupContent').find('#clientForm')[0].telefono.value = phone;

        } else {
            cliente.edad = app.Helper.getYearsDiff( cliente.fechaNacimiento );
            app.Helper.loadTemplate( 'client_details', cliente, this.$clientForm );

            // Actualizamos los datos del pedido

            self.pedido.save({ clienteID : cliente.id });
            //  alert("agregar usuario al pedido => "+cliente.id)

            //actualizamos el ultimo pedido del cliente

            setTimeout(function(){
                var counter = 0;
                var filter = _.filter(app.Pedidos.toJSON(), function(pedido){
                    var valid = false;
                    var today = new Date();
                    var date = new Date(today.getFullYear(),today.getMonth(),today.getDate()).getTime();
                    if(date <= pedido.fecha && pedido.clienteID == cliente.id && pedido.cliente != "" && counter == 1){
                        valid = true;
                    }
                    counter++;
                    return valid;
                });

                if(filter.length > 0){
                    app.Helper.loadTemplate( 'client_last_order', { lastOrder : filter }, $('#lastOrder') );
                }else{
                    $('#lastOrder').html("<li>Sin resultados</li>")
                }
            },3000);



        }

    },

    renderMenu: function( catID ) {
        app.Products.resort('name');
        this.$menuHandler.html('');
        var productos = (catID !== undefined) ? JSON.parse(JSON.stringify(app.Products.where({categoryID: catID}))):app.Products.toJSON();
        
        //console.log(productos);
        app.Helper.loadTemplate( 'menu_item', {productos: productos}, this.$menuHandler);
        app.Products.resetOrder();
    },

    renderPedidoProductos: function() {

        var handler = this.$orderTable.find('tbody');
        var productos = app.PedidosProductos.where({pedidoID:this.pedido.get('id')});

        handler.html('');
        _.each( productos, function( producto ){
            handler.append( new app.ProductOrderView( producto ).render().el );
        } );
    },

    saveOrder: function(e) {
        e.preventDefault();
        var error = false;

        if ( ! this.validateOrder() ) {
            alert( 'Ha ocurrido un error al generar la orden' );
            error = true;
        }

        if ( ! this.validateClient() ) {
            alert( 'Debes seleccionar o dar de alta un cliente' );
            error = true;
        }

        if ( ! this.validateProducts() ) {
            alert( 'Debes agregar al menos un producto' );
            error = true;
        }

        if ( this.$recibe.val().length > 0 && parseFloat(this.$recibe.val()) < this.getTotal() ) {
            alert( 'La cantidad recibida debe ser mayor que el total de la orden' );
            error = true;
        }

        if ( this.$aplicarSi[0].checked && this.$recibe.val().length === 0 ) {
            alert( 'Debe ingresar un monto a pagar' )
            error = true;
        }
        if (!error){
             this.pedido.save({metodoPago: this.$metodoPago.val()});
        //obtener el nombre del cliente del pedido actual
        var clienteID =  this.pedido.get('clienteID');
        var cliente = _.findWhere(app.Clientes.toJSON(),{ id : clienteID  });
        var newAttrs = { status: 1, pagado: 0, comentarios: '', cliente : cliente.nombres+" "+cliente.apellidos };

        if ( this.$aplicarSi[0].checked && this.$recibe.val().length > 0){
                        newAttrs.pagado = 1;
        }else{
            newAttrs.comentarios = 'El cliente va a pagar con $' + this.$recibe.val() + '<br> El cambio correspondiente es de $' + this.$cambio.val();
        } 
        this.undelegateEvents(); 
        this.pedido.save( newAttrs );
        app.BitacoraRows.create( this.attrsBitacora() );
        app.dashboardView.renderlistado();
        app.reportesView.renderReports();
        app.notificationsView.createNoti(clienteID);
        // var clientes = new app.ClientesView();
        //console.log(clientes);
         $('#popup-nuevo-pedido').remove();
        $('.reveal-modal-bg').remove();
        }
       },

    searchClient: function(e) {
        e.preventDefault();
        var phone = e.target.phone.value;
        var cliente = app.Clientes.findWhere({ telefono: phone });

        if (e.target.id === 'phoneForm') {
            this.$phone.hide();
            this.$popup.show();
            this.$('input[name="phone"]').val(phone);
            this.renderMenu();
        }

        //alert(JSON.stringify(cliente))
        //console.log(cliente)

        this.renderClient(cliente);
    },

    setChange: function() {
        this.$cambio.val( (parseFloat(this.$recibe.val()) - this.getTotal()).toFixed(2) );
    },

    setTotal: function() {
        this.$totalHandler.html( this.getTotal() );

        if (this.$cambio.val().length > 0)
            this.setChange();
    },

    showForm: function(e) {
        e.preventDefault();
        var $link = $(e.target).closest('a'),
            clientID = $link.data('id'),
            client = app.Clientes.get(clientID);

        // alert(clientID)

        this.renderClient(client, true);
    },

    updateClient: function(e) {
        e.preventDefault();
        var form = e.target;
        if(form.id.value){
            //console.log("id definido");
            id = form.id.value;
        }else{
            //console.log("id indefinido");
            var max = _.max(app.Clientes.toJSON(),function(max){ return max.id });
            id = ( max.id > 0 ) ? (max.id+1) : 1;
        }
        //var clienteID = form.id.value ? form.id.value : 1;
        var attrs = {
            id :             parseInt(id),
            telefono:        form.telefono.value,
            nombres:         form.nombres.value,
            apellidos:       form.apellidos.value,
            direccion:       form.direccion.value,
            sexo:            form.sexo.value,
            fechaNacimiento: form.fechaNacimiento.value,
            twitter:         form.twitter.value,
            correo:          form.correo.value,
            comentarios:     form.comentarios.value
        };

        var err = false;

        if (form.telefono.value == ""){
            alert("El campo Teléfono es obligatorio");
            err = true;
        }
        if (form.nombres.value == "" ){
            alert("El campo Nombre es obligatorio");
            err = true;
        }
        if ( form.direccion.value == "" ){
            alert("El campo Dirección es obligatorio");
            err = true;
        }
        if ( form.sexo.value == "" ){
            alert("El campo Sexo es obligatorio");
            err = true;
        }

        if (!err && err == false) {
            var client = app.Clientes.get(parseInt(id));
            //console.log(client);
            if (client){
                //console.log("guardar");
                client.save(attrs, {wait: true});
            }else{
                 //console.log("crear");
                client = app.Clientes.create(attrs, {wait: true});
            }
            //if (!client.validationError) {
            var newcliente = app.Clientes.get(parseInt(id));
            this.renderClient(newcliente);
            app.clientesView.renderClients();
            //this.dispose();

            //console.log(form);
            //}

            //app.Helper.loadTemplate( 'client_details', app.Clientes.get(parseInt(clienteID)).toJSON(), this.$clientForm );
        }
    },

    validateClient: function() {
        return ( this.pedido.get('clienteID') === 0 ) ? false : true;
    },

    validateOrder: function() {
        return ( this.pedido === undefined ) ? false : true;
    },

    validateProducts: function() {
        var productos = app.PedidosProductos.where({ pedidoID: this.pedido.get('id') });
        return ( productos.length === 0 ) ? false : true;
    },
    close: function(){
        $("#popup-nuevo-pedido").remove();
        $('.reveal-modal-bg').remove();
                
    }
});
