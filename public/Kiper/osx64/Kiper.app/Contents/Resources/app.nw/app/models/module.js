var app = app || {};
app.Module = Backbone.Model.extend({
    defaults: {
        'name': ''
    },

    validate: function( attrs, options ) {
        if ( attrs.name.length === 0 )
            return 'Es necesario ingresar el nombre del módulo';
    }
});