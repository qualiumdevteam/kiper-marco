var app = app || {};
app.PedidoChangeStatusView = Backbone.View.extend({
    tagName: 'div',

    events: {
        "click .complete": "completeOrder",
        "click .send": "sendOrder",
        "click .cancel": "cancelOrder",
        "click .close" :"close"
    },
    close: function(){
        $("#popup-nuevo-pedido").remove();
        $('.reveal-modal-bg').remove();
        $("#popup-change-pedido-status").remove();           
    },
    cancelOrder: function(e) {
        e.preventDefault();
        if ( !confirm('¿Esta segur@ que desea cancelar esta orden?') )
            return false;

        this.model.save({status: 4}, { wait: true });
        if ( this.model.validationError )
            alert( this.model.validationError )

        app.pedidosView.renderPedidos(true);
        this.$el.closest('.popup').foundation('reveal', 'close');
        app.dashboardView.renderlistado();
    },

    completeOrder: function(e) {

        e.preventDefault();
        var hora = this.hora(hora);

        this.model.save({
            status:      2,
            pagado:      1,
            colorStatus: 'white',
            horaEntrega: hora,
        }, { wait: true });

        if (this.model.validationError)
            alert(this.model.validationError);

        app.pedidosView.renderPedidos(true);
        this.$el.closest('.popup').foundation('reveal', 'close');
        app.dashboardView.renderlistado();
    },
    hora:function(){
        var date = new Date();
        hor = date.getHours()+1;
        min = date.getMinutes()+1;
        seg = date.getSeconds()+1;
        Hora = (hor+":"+min+":"+seg);
        return Hora;
    },
    sendOrder: function(e) {
        e.preventDefault();
       var hora = this.hora(hora);
        this.model.save({
            status:      5,
            pagado:      0,
            colorStatus: 'white',
            horaEnvio: hora
        }, { wait: true });

        if (this.model.validationError)
            alert(this.model.validationError);

        app.pedidosView.renderPedidos(true);
        this.$el.closest('.popup').foundation('reveal', 'close');
        app.dashboardView.renderlistado();
    },
    initialize: function( attrs ) {
        if (attrs.id !== undefined)
            this.model = app.Pedidos.get( attrs.id );

        this.render( attrs );
    },

    render: function( attrs ) {
        var modelo = this.model.toJSON();

        if (attrs.actions !== undefined && attrs.actions === 1)
            modelo.actions = 1;

        app.Helper.loadTemplate( 'popups/pedido_change_status', modelo, this.$el );
    }
});
/*estatus
1 = pendiente
5 = enviado
2 = entregado*/
