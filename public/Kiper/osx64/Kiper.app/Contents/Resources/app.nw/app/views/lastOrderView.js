var app = app || {};
app.LastOrderView = Backbone.View.extend({

    tagName: 'div',

    template: 'pedido_last_order',

    events: {},

    initialize: function() {

    },

    render: function() {
        var pedido = app.Pedidos.models[0];
        var data = {};
        var products = _.filter( app.PedidosProductos.toJSON(), function(product) { return product.pedidoID === pedido.get('id') } );

        if (pedido !== undefined) {
            data.pedido = pedido.toJSON();
            data.products = products;
        } else {
            data.pedido = [];
            data.products = [];
        }

        switch (data.pedido.status) {
            case 0: data.pedido.statusText = 'Abierto'; break;
            case 1: data.pedido.statusText = 'pendiente'; break;
            case 2: data.pedido.statusText = 'Entregado'; break;
            case 3: data.pedido.statusText = 'Eliminado'; break;
            case 4: data.pedido.statusText = 'Cancelado'; break;
            case 5: data.pedido.statusText = 'Enviado'; break;
        }

        app.Helper.loadTemplate( this.template, data, this.$el );
        return this;
    }
});