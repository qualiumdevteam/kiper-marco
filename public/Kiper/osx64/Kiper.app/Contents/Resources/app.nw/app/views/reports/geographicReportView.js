var app = app || {};

app.GeographicReportView = Backbone.View.extend({
    el: '#geographic',

    events: {},

    initialize: function() {
        this.render();
    },

    render: function() {

        app.Helper.loadTemplate('reports/geographic', {}, this.$el);

        Morris.Bar({
            element: 'geographic-canvas',
            data: [
                { y: '2006', a: 100 },
                { y: '2007', a: 75 },
                { y: '2008', a: 50 },
                { y: '2009', a: 75 },
                { y: '2010', a: 50 },
                { y: '2011', a: 75 },
                { y: '2012', a: 100 }
            ],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Series A']
        });
        app.appView.colorReport();
          
        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');

        return this;
    }
});