var app = app || {};

app.PedidoItemView = Backbone.View.extend({
    tagName: 'tr',

    events: {
        "click .delete": "clearItem",
        "click .preview": "details",
        "click .print": "print",
        "click .edit": "editOrder",
        "click .change-status1": "changeStatus1",
        "click .change-status2": "changeStatus2"
    },

    changeStatus1: function(e) {
        var x = $(e.currentTarget)[0];
        var data = $(x).data('status');
        // console.log(data);
       if (data == 1) {
        e.preventDefault();
        app.Helper.showPopup({
            id: 'popup-change-pedido-status',
            msg: 'PedidoChangeStatusView',
            msgType: 'info',
            closable: true,
            data: { id: this.getModel().id, actions: 1 }
        });
       };
    },
    changeStatus2: function(e) {
        var x = $(e.currentTarget)[0];
        var data = $(x).data('status');
                // console.log(data);
   if (data == 5) { 
        e.preventDefault();
        app.Helper.showPopup({
            id: 'popup-change-pedido-status',
            msg: 'PedidoChangeStatusView',
            msgType: 'info',
            closable: true,
            data: { id: this.getModel().id, actions: 1 }
        });
    };
    },

    clearItem: function(e) {
        e.preventDefault();
        var $this = this;

        var confirm = app.Helper.showConfirm({
            msg: '¿Esta segur@ que desea eliminar este elemento?',
            msgType: 'notice',
            closable: true
        });

        confirm.done(function() {
            var modelo = app.Pedidos.get( $this.getModel().id );
            modelo.save({status: 3}, {wait: true});
            app.dashboardView.renderlistado();
            if ( modelo.validationError )
                alert(this.model.validationError);
        });
    },

    details: function(e) {
        e.preventDefault();
        app.Helper.showPopup({
            id: 'popup-change-pedido-status',
            msg: 'PedidoChangeStatusView',
            msgType: 'info',
            closable: true,
            data: { id: this.getModel().id, actions: 0 }
        });
    },

    editOrder: function(e) {
         var x = $(e.currentTarget)[0];
        var data = $(x).data('status');
        if (data == 1) {
        e.preventDefault();
        app.Helper.showPopup({
            id: 'popup-editar-pedido',
            msg: 'PedidoEditView',
            msgType: 'clear',
            closable: true,
            size: 'xlarge',
            data: { id: this.getModel().id }
        });
        }else{
             app.Helper.showAlert( {
                msg: "No puede editar un pedido entregado",
                msgType: 'notice'
            });
        };
    },

    getModel: function() {
        return (this.model.attributes) ? this.model.toJSON() : this.model;
    },

    initialize: function( model ) {
        if (model) this.model = model;
    },

    print: function( e ) {
        e.preventDefault();
       // var curr = Ti.UI.getCurrentWindow();
        var printSettings = app.Settings.findWhere({itemName: 'printerSize'}); // sustituir por el valor de un select
        var gui = require('nw.gui');



        if (printSettings != undefined)
            printSettings = parseInt( printSettings.get('itemValue') );
        else
            printSettings = 44;

        //mm=(pixels*25.4)/DPI
        var mmequivPx = 3.779528;
        var size = Math.ceil(printSettings * mmequivPx);
        var newWin = gui.Window.open("impresion_pedido.html", {
            position: 'center',
            width: size
        });




        //var newWin = curr.createWindow( { url: 'app://impresion_pedido.html', width: size } );
        //console.log(size, printSettings, Ti.API.get('mmEquivPx'));

        //Ti.API.set('pedidoToPrint', this.model);
        var ticketData = this.model;
        var productos = [];
        //ticketData.settings = app.Settings.toJSON();
        ticketData.fechaString = new Date( ticketData.fecha ).toJSON().replace(/T/ig, ' ').replace(/Z/ig, ' ').replace(/\.\d*/ig, '');
        ticketData.image = app.Settings.findWhere({itemName: 'logotipo'}).toJSON().itemValue;
        ticketData.nombreEmpresa = app.Settings.findWhere({itemName: 'nombre_empresa'}).toJSON().itemValue;
        ticketData.sucursal = app.Sucursales.findWhere({active: 1}).toJSON();

        _.each(app.PedidosProductos.where({pedidoID: ticketData.id}), function(producto){
            productos.push( producto.toJSON() );
        });

        ticketData.productos = productos;

        newWin.pedido = ticketData;
        //newWin.open();
        //newWin.focus();

        //console.log( newWin.HTTP_DATA_RECEIVED, newWin.getDOMWindow() );
    },

    render: function() {
        var modelo = this.getModel();
        var cliente = app.Clientes.get( modelo.clienteID);

        //modelo.cliente  = cliente.get('nombres') + ' ' + cliente.get('apellidos');

        app.Helper.loadTemplate( 'pedido_item', modelo, this.$el );
        return this;
    }
});