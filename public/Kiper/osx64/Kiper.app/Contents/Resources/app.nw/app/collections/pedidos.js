var app = app || {};
var PedidosCollection = Backbone.Collection.extend({
    model: app.Pedido,
    url : app.resourcesDir+"/dbfiles/pedidos.json",
    fieldOrder: 'fecha',
    order: 'DESC',
    
    //localStorage: new Backbone.LocalStorage('pedidos-togo'),
    sync : function(method,model,options){
        //console.log(method,model)
        if(method === "read"){
            var db = fs.readFileSync(this.url);
            if (db.length>0) {
            this.reset(JSON.parse(db.toString()));
        }
            //console.log(JSON.parse(db.toString()))
        }

        if(method === "set"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }

        if(method === "remove"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }
    },

    create : function(attr){
        // console.log("create",attr);
        //primero se llama el methodo add para añadirlo a la coleccion
        //despues se llama el sync
        var model = this.add(attr);
        this.sync("set",this,attr);
        return model;
    },
    comparator: function( a, b ) {
        var a = a.get(this.fieldOrder);
        var b = b.get(this.fieldOrder);

        if (this.order == 'DESC') {
            return a > b ? -1 : 1;
        } else {
            return a > b ? 1 : -1;
        }
    },

    orderBy: function(field, order) {
        this.fieldOrder = field;
        this.order = order;
        this.sort();
    },

    resetOrder: function() {
        this.fieldOrder = 'fecha';
        this.order = 'DESC';
        this.sort();
    }
});

app.Pedidos = new PedidosCollection();
