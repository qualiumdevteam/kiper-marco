var app = app || {};
app.SucursalView = Backbone.View.extend({
    tagName: 'tr',
    events: {
        "click .edit": "edit",
        "click .delete": "clear",
        "click .default": "setDefault",
        "click .close": "closeForm",
        "submit form": "updateSucursal"
    },

    clear: function(e) {
        e.preventDefault();

        var usuarios = app.Usuarios.where({sucursalID: this.model.get('id')});
        if (usuarios.length > 0)
            return app.Helper.showAlert({
                msg: 'No se puede eliminar esta sucursal porque existen usuarios asociados a ésta',
                msgType: 'error',
                closable: true
            });

        var confirm = app.Helper.showConfirm({
            msg: '¿Esta segur@ que desea eliminar este elemento?, esta acción provocará que se elimine también la relación de usuarios con esta Sucursal',
            msgType: 'notice',
            closable: true
        });

        confirm.done($.proxy(function() {
            this.model.destroy();
            this.collection.sync("remove");
            this.remove();
        }, this));
    },

    closeForm: function (e) {
        e.preventDefault();
        this.render();
    },

    edit: function(e) {
        e.preventDefault();
        app.Helper.loadTemplate( 'sucursal_form', this.model.toJSON(), this.$el );
    },

    initialize: function( model ) {
        if (model !== undefined)
            this.model = model;
    },

    render: function() {
        app.Helper.loadTemplate( 'sucursal_item_list', this.model.toJSON(), this.$el );
        return this;
    },

    setDefault: function(e) {
        e.preventDefault();

        var actualDefault = app.Sucursales.findWhere({ active: 1 });

        if ( actualDefault !== undefined )
            actualDefault.save({ active: 0 });

        this.model.save({ active: 1 });
        console.log("cambio");
    },

    updateSucursal: function(e){
        e.preventDefault();

        var form = e.target;
        var attrs = {
            name: form.name.value,
            rfc: form.rfc.value,
            fiscalName: form.fiscalName.value,
            address: form.address.value,
            email: form.email.value,
            phone: form.phone.value
        }

        this.model.save( attrs, { wait: true } );

        if ( this.model.validationError )
            return app.Helper.showAlert( { msg: this.model.validationError } );


        this.render();
    }
});