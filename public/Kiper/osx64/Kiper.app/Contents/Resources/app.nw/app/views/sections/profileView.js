var app = app || {};
app.ProfileView = Backbone.View.extend({
    el: '.section[data-section="profile"]',

    events: {
        "click .edit": "editar"
    },

    createItem: function() {
    },

    editar: function(e) {
        e.preventDefault();
        var popup = app.Helper.showPopup({
            msg:'UsuarioNewView',
            id:'popup-editar-usuario',
            msgType: 'clear',
            size: 'large',
            closable: true,
            data: { id: this.model.get('id') }
        });
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.model = app.Usuarios.get( app.userSession );
        this.render();
        this.listenTo( this.model, "change", this.render );
    },

    render: function() {
        var data = {
            usuario: this.model.toJSON()
        }

        data.usuario.sucursalName = app.Sucursales.get( data.usuario.sucursalID ).get('name');
        data.usuario.perfilName = app.Profiles.get( data.usuario.perfil).get('name');

        app.Helper.loadTemplate( 'sections/profile', data, this.$el );
        return this;
    }
});