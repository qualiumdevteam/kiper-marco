var app = app || {};
app.ProfileEmpresaView = Backbone.View.extend({
    el: '.section[data-section="profileEmpresa"]',

    events: {
        "click .edit-name": "editName",
        "click .edit-foto": "editFoto",
        "click .add-sucursal": "addSucursal",
        "click .close": "closeForm",
        "change #foto-field": "updateFoto",
        "submit #formSettingsName": "updateName",
        "submit #suc-form": "saveSucursal"
    },

    addSucursal: function(e){
        e.preventDefault();
        var $link = $(e.target).closest('a');
        if ($link.hasClass('save'))
            return this.saveSucursal();
        $("#sucursales-table").css("display","none");
        app.Helper.loadTemplate( 'sucursal_form', {}, this.$sucFormWrapper );
        this.$btnAddSucursal.hide();
    },

    closeForm: function(e) {
        e.preventDefault();

        this.$sucFormWrapper.html('');
        this.$btnAddSucursal.show();
        $("#sucursales-table").css("display","table");
    },

    editFoto: function(e) {
        e.preventDefault();
        this.$fotoField.trigger('click');
    },

    editName: function(e) {
        e.preventDefault();
        var data = { nombreEmpresa: app.Settings.findWhere({itemName:'nombre_empresa'}).toJSON().itemValue }
        app.Helper.loadTemplate( 'settings_name_edit', data, this.$nameWrapper );
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$sucursalesHandler = this.$('#sucursales-table tbody');
        this.$fotoField = this.$('#foto-field');
        this.$preview = this.$('#image-preview');
        this.$nameWrapper = this.$('#name-wrapper');
        this.$sucFormWrapper = this.$('#suc-form-wrapper');
        this.$btnAddSucursal = this.$('.add-sucursal');
        this.renderSucursales();

        this.listenTo(app.Sucursales, "change:active", this.renderSucursales);
    },

    render: function() {
        var logomodelo =  app.Settings.findWhere({itemName:'logotipo'}),
        logotipo = '';
        //app.Settings.findWhere( { itemName: 'logotipo' })

        if(logomodelo != null && logomodelo != undefined){ logotipo = logomodelo.get('itemValue') }
        var data = {
            nombreEmpresa: app.Settings.findWhere({itemName:'nombre_empresa'}).get('itemValue'),
            logotipo: logotipo
        }
        app.Helper.loadTemplate( 'sections/profile_empresa', data, this.$el );

        return this;
    },

    renderSucursales: function(e) {
        var $this = this;

        this.$sucursalesHandler.html(' ');
        _.each( app.Sucursales.models, function( sucursal ) {
            $this.$sucursalesHandler.append( new app.SucursalView( sucursal ).render().el );
        } );
    },

    saveSucursal: function(e) {
        e.preventDefault();
        var form = e.target;

        var max = _.max(app.Sucursales.toJSON(),function(max){ return max.id; })

        var attrs = {
            id : (max.id > 0) ? (max.id + 1) : 1,
            name: form.name.value,
            rfc: form.rfc.value,
            fiscalName: form.fiscalName.value,
            address: form.address.value,
            email: form.email.value,
            phone: form.phone.value,
            active: app.Sucursales.models.length > 0 ? 0: 1
        };

        var suc = app.Sucursales.create(attrs, {wait: true});

        if (suc.validationError)
            return app.showAlert({ msg: suc.validationError });

        this.$sucFormWrapper.html('');
        this.$btnAddSucursal.show();

        this.renderSucursales();
        $("#sucursales-table").css("display","table");
    },

    updateFoto: function(e) {
        e.preventDefault();
        var field = e.target,
            file  = app.Helper.uploadFile(field),
            //newName = 'img_' + app.Helper.getFulltimeString(),
            logotipo = app.Settings.findWhere({itemName: 'logotipo'})
            $this = this;
        // console.log(file);
        //var newNameExt = app.Helper.rename( file, newName);
        //var path = newNameExt;
        var nameLog = logotipo.get('itemValue');
        // console.log(nameLog);
        if (nameLog !== "") {
        var oldImg = app.Helper.fileExist();
        // console.log(oldImg);
        if (oldImg) app.Helper.deleteFile(logotipo.get('itemValue')); 
        };
        logotipo.set({itemValue: file});
        $.when(logotipo.save()).done(function(){
            setTimeout(function(){
                $this.$preview.css({
                    'background-image': 'url('+app.uploadDir+file+')',
                    'backgorund-repeat': 'no-repeat',
                    'background-position': 'top center',
                    'background-size': 'cover'
                });
                $this.renderSucursales();
                app.layoutView.refreshLogo();
            },2000)
        });
    },

    updateName: function(e) {
        e.preventDefault();
        var name = this.$('#formSettingsName')[0].itemValue.value;
        if (name == "") {
              app.Helper.showAlert({
              msg: "EL campo no puede estar vacio",
              msgType: 'error'
              });

        }else{
            var setting = app.Settings.findWhere({itemName: 'nombre_empresa'});
            setting.set({itemValue: name});
            setting.save();
            var data = {
                nombreEmpresa: setting.get('itemValue')
            }
            app.Helper.loadTemplate( 'settings_name', data, this.$nameWrapper );    
        };
    }
});