var app = app || {};
app.SendCorreoView = Backbone.View.extend({
    tagName: 'div',

    events: {
        "change #categoryID": "filterMenu",
        "click .close": "Close",
        "submit #sendcorreo": "sendMessage",

    },
     sendMessage: function(e) {
        e.preventDefault();
        $form = e.target;
        var $this = this;
        if ($form.asunto.value !="" && $form.mensaje.value != "") {
        var data = {
            identificador: "email",
            nombre:  $form.nombrecocina.value,
            destinatario: $form.destinatario.value,
            asunto: $form.asunto.value,
            mensaje: $form.mensaje.value
        };
        console.log(data);
        var request = $.ajax({
                data:  data,
                url:   "http://kiper.mx/fedback/",
                type:  'post',
        });
        if (request) {
            alert("su correo ah sido enviado correctamente");
            $this.Close();
        } else {
            $this.Close();
            alert("Ha ocurrido un error, intente nuevamente mas tarde");
        }//cierre if reques
    }else{//cierre el form
         alert("DEbe llenar todos los campos");
    }
    },
    initialize: function( attrs ) {
        this.render(attrs);
    },

    render: function(attrs) {
        app.Helper.loadTemplate( 'popups/send_correo', attrs.detail.attributes, this.$el );
    },

    Close:function(){
        $('#popup-send-correo').remove();
        $('.reveal-modal-bg').remove();

    },
});