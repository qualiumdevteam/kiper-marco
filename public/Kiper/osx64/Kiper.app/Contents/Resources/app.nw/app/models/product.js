var app = app || {};

app.Product = Backbone.Model.extend({
    defaults: {
        id : 0,
        categoryID: 0,
        code: '000',
        name: 'Nuevo Producto',
        description: 'Nuevo Producto',
        price: 1.00,
        creationDate: new Date(),
        updateDate: new Date(),
        image: '',
        available: 1
    },

    save : function(attrs,options){
        this.set(attrs,{validate : true});
        this.collection.sync("set",this,attrs);
    },

    initialize: function() {
        this.on("destroy", this.removeImage);
    },

  removeImage: function(){
    var img = this.get('image');  
        if (img !== "" ) {
            var exist = app.Helper.fileExist(img);  
            if (exist) {
                app.Helper.deleteFile(img);
            };
        }
    },


    validate: function( attrs, options ) {
        if ( attrs.code.length === 0
            || attrs.name.length === 0
            || attrs.price.length === 0)
            return 'No se han completado todos los campos necesarios';
    }
});