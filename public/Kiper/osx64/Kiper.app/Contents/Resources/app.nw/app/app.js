var app = app || {};
var ENTER_KEY = 13;


(function($) {
    // Ti.API.set('mmEquivPx', 3.779528);

    // app.resourcesDir = Ti.Filesystem.getResourcesDirectory();
    // app.uploadsDir   = 'app://' + Ti.API.Application.getID() + '/assets/uploads/';
    // app.tempDir      = 'app://' + Ti.API.Application.getID() + '/assets/temp/';
    

    // se puede cargar un elemento de html con esta funcion
    // var fs = require('fs');

    // fs.readdir('assets',function (error, files) {
    //     alert(JSON.stringify(error))
    //     alert(JSON.stringify(files))
    //     document.write(contents);
    // });
    // app.resourcesDir = '/assets/uploads/';



    // Iniciamos todos las colecciones
    app.Categories.fetch();
    app.Clientes.fetch();
    app.MetodosPago.fetch();
    app.Pedidos.fetch();
    app.PedidosProductos.fetch();
    app.Products.fetch();
    app.Profiles.fetch();
    app.Settings.fetch();
    app.Sucursales.fetch();
    app.Usuarios.fetch();
    app.Printers.fetch();
    app.Notifications.fetch();
    app.DetailNotification.fetch();

    if ( app.Sucursales.models.length === 0 ){
        app.Sucursales.create( { name: 'matriz', address: '' } );
    }

    if ( app.Profiles.models.length === 0 ) {
        app.Profiles.create( { name: 'admin' } );
        app.Profiles.create( { name: 'user' } );
    }
    if(app.Notifications.models.length === 0 ){
     app.Notifications.create({id:1,
            firstMessage:"false",
            firstConfig:"",
            activo: "false",
            time: ""});
    }
    app.appView = new app.AppView();

})(jQuery);