var app = app || {};

app.FidelityReportView = Backbone.View.extend({
    el: '#fidelity',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();
    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_fidelity').val()),
            toDate = new Date(this.$form.find('#toDate_fidelity').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate());
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1);

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },

    getData: function(from,to,month) {
        var dataChart = [];
        var monthRange = month ? this.getMonthRange(month) : this.getDayRange(from,to);
        var data = [];
        var dataByClients = [];

        data = _.filter( app.reportesView.getPedidos(), function(pedido){
            var valid = false;

            if ( pedido.pagado === 1 && pedido.fecha > monthRange.from.getTime() && pedido.fecha < monthRange.to.getTime() )
                valid = true;

            return valid;
        });

        dataByClients = _.countBy(data, function(pedido){ return pedido.clienteID });

        var media = parseInt(this.getMedia( dataByClients ));

        if(media > 0 && media != 0 && media != ''){
            dataChart.push({
                label: 'Promedio',
                value: media
            });
        }else{
            dataChart.push({
                label: 'Promedio',
                value: 0
            });
        }

        return dataChart;
    },

    getMedia: function(collection) {
        var sum = 0,
            len = Object.keys(collection).length;

        _.each(collection, function(val, key){ sum += val });

        return Math.round( sum/len );
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        app.Helper.loadTemplate('reports/fidelity', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        this.renderChartPerMonth();
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(from,to){
        var data = this.getData(from,to,null);
        this.$canvas.html('');
        Morris.Donut({
            element: 'fidelity-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)'],
            formatter: function(y, data) { return y + ' visitas' },
            labelColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');

        Morris.Donut({
            element: 'fidelity-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)'],
            formatter: function(y, data) { return y + ' visitas' },
            labelColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = (fromMonth !== undefined) ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});