var app = app || {};

app.ClientesView = Backbone.View.extend({

    el: '.section[data-section="clientes"]',

    events: {
        "submit #clientFormPrin": "createClient",
        "click form button.cancel": "cancelEdition"
    },

    cancelEdition: function(e) {
        e.preventDefault();
        this.$form.closest('fieldset').find('h5').text('Nuevo Cliente');
        this.renderForm();
        $('#clientFormPrin').removeClass("update");
    },

    createClient: function(e) {
        e.preventDefault();
        var form = this.$form[0];
        if (this.$form.hasClass('update'))
            return this.updateClient();

        if(form.telefono.value !== "" &&
            form.nombres.value !== "" &&
            form.apellidos.value !== "" &&
            form.direccion.value !== "" &&
            form.sexo.value !== ""){
            app.
            app.Clientes.create( this.newAttributes(), { wait: true } );
            this.$form[0].reset();
            this.renderClients();
        }else{
            alert("Debe rellenar todos los campos");
             this.$form.off();
        }
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();

        this.$form = $('#clientFormPrin');
        this.$clientsList = this.$('#clients-list tbody');

        this.renderForm( null );
        if (app.Clientes.models.length > 0)
            this.renderClients();
    },

    newAttributes: function(isUpdate,formvalue) {
        var form = this.$form[0];
        var id = 1;
        if(isUpdate && formvalue != 0){
            id = formvalue;
            //parte notificaciones
            //console.log(form.twitter.value);
            if (form.twitter.value != "" ) {
            detail = app.DetailNotification.findWhere({"id_cliente":parseInt(id)});
            console.log(detail);
                if (detail != undefined) {
                     detail.set({twitter: form.twitter.value});
                     detail.save(); 
                     app.notificationsView.renderDetailNoti();
                };
             };
              if (form.correo.value != "" ) {
                    if (detail != undefined) {
                    detail = app.DetailNotification.findWhere({"id_cliente":parseInt(id)});
                    detail.set({correo: form.correo.value});
                    detail.save(); 
                    app.notificationsView.renderDetailNoti();
                  }
             };
             //
        }else{
            var max = _.max(app.Clientes.toJSON(),function(max){ return max.id });
            id = ( max.id > 0 ) ? (max.id+1) : 1;
        }
        return {
            id : parseInt(id),
            telefono: form.telefono.value,
            nombres: form.nombres.value,
            apellidos: form.apellidos.value,
            direccion: form.direccion.value,
            sexo: form.sexo.value,
            fechaNacimiento: form.fechaNacimiento.value,
            twitter: form.twitter.value,
            correo: form.correo.value,
            comentarios : form.comentarios.value
        }
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/clientes', {}, this.$el );

        return this;
    },

    renderClients: function() {
        var $this = this;
        this.$clientsList.html(' ');
        _.each( app.Clientes.models, function( cliente ) {
            $this.$clientsList.append( new app.ClientItemView( cliente ).render().el );
        } );
    },

    renderForm: function( model ) {
        app.Helper.loadTemplate( 'client_form_principal', model || {}, $('#clientFormPrin'));
        if (model) {
             $('#clientFormPrin').addClass('update');
            this.$form.closest('fieldset').find('h5').text('Actualizar Cliente');
        }
    },

    showErrors: function(client) {
        var errors = client.validationError;

        _.each(errors, function(error) {
            alert( error.msg.replace('%f', error.field) );
        });
    },

    updateClient: function() {
        if(this.$form[0].id.value != null && this.$form[0].id.value != undefined){
            var client = app.Clientes.get(this.$form[0].id.value);
            client.save(this.newAttributes(true,this.$form[0].id.value), {wait: true});
            if ( client.validationError ) {
                this.showErrors( client );
                return;
            }
        }

        this.$form.removeClass("update");
        this.$form.closest('fieldset').find('h5').text('Nuevo Cliente');
        this.renderForm();
        this.renderClients();
        //this.form.off();
    }
});