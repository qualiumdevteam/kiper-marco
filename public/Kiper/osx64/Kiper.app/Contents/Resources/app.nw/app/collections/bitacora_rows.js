var app = app || {};
var BitacoraRowCollection = Backbone.Collection.extend({
    model: app.BitacoraRow,

    fieldOrder: 'date',
    order: 'DESC',

    localStorage: new Backbone.LocalStorage('bitacorarows-togo'),

    comparator: function( pedido ) {
        if (this.order == 'DESC')
            return ! pedido.get( this.fieldOrder );
        else
            return pedido.get( this.fieldOrder );
    },

    orderBy: function(field, order) {
        this.fieldOrder = field;
        this.order = order;
        this.sort();
    },

    resetOrder: function() {
        this.fieldOrder = 'date';
        this.order = 'DESC';
        this.sort();
    }
});

app.BitacoraRows = new BitacoraRowCollection();