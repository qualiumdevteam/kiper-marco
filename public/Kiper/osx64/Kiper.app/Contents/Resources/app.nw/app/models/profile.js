var app = app || {};
app.Profile = Backbone.Model.extend({
    defaults: {
        id : 0,
        name: ''
    },

    save : function(attrs,options){
        this.set(attrs,{validate : true});
        this.collection.sync("set",this,attrs);
    },

    validate: function( attrs, options ) {
        if ( attrs.name.length === 0 )
            return 'El campo nombre es obligatorio';
    }
});
