var app = app || {};
app.ClientsAmountReportView = Backbone.View.extend({
    el: '#clients-amount',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    getData: function(from,to,month) {
        var dataChart = [];
        var $this = this;

        _.each(app.Clientes.toJSON(), function(client){
            dataChart.push({
                client: client.nombres + ' ' + client.apellidos,
                value: $this.getTotalByClient(client.id,from,to,month)
            })
        });

        return dataChart;
    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_clientsAmount').val()),
            toDate = new Date(this.$form.find('#toDate_clientsAmount').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate()).getTime();
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1).getTime();

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },

    filterSelected: function(e) {
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();
    },


    getTotalByClient: function(clientID,from,to,month) {
        var total = 0;
        var today = new Date();
        var collection;
        var from = from;
        var to = to;
        var firstMonthDay = month ? new Date(today.getFullYear(),parseInt(month),1).getTime() : new Date(today.getFullYear(),today.getMonth(),1).getTime();
        var lastMonthDay = month ? new Date(today.getFullYear(),parseInt(month) +1 ,0).getTime() : new Date(today.getFullYear(),today.getMonth() +1 ,0).getTime();

        collection = _.where(app.reportesView.getPedidos(), {clienteID: clientID});

        if(firstMonthDay != undefined && firstMonthDay != '' && from == null && to == null && firstMonthDay != null && lastMonthDay != undefined && lastMonthDay != null && lastMonthDay != ''){
            //alert('es mes')
            _.each(collection, function(pedido){
                if(pedido.fecha >= firstMonthDay && pedido.fecha <= lastMonthDay){
                    total += parseFloat( pedido.costo.replace(/\$/, '') );
                    //alert(total)
                }
            });
        }else{
            //alert('es fechas')
            _.each(collection, function(pedido){
                if(pedido.fecha >= from && pedido.fecha <= to){
                    total += parseFloat( pedido.costo.replace(/\$/, '') );
                }
            });
        }

        return total.toFixed(2);
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    initialize: function() {
        this.render();

        this.listenTo( app.Pedidos, "add", this.renderChart );
        this.listenTo( app.Pedidos, "change", this.renderChart );
        this.listenTo( app.Pedidos, "remove", this.renderChart );
    },

    render: function() {
        app.Helper.loadTemplate('reports/clients_amount', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.renderChart();
        this.$form = this.$('#filter-form');
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(from,to,month) {
        var data = this.getData(from,to,null);
        this.$canvas.html('');
        Morris.Bar({
            element: 'clients-amount-canvas',
            data: data,
            xkey: 'client',
            ykeys: ['value'],
            labels: ['Total'],
            barColors: ['rgba(81,219,120,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');
        this.chart = new Morris.Bar({
            element: 'clients-amount-canvas',
            data: data,
            xkey: 'client',
            ykeys: ['value'],
            labels: ['Total'],
            barColors: ['rgba(81,219,120,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});