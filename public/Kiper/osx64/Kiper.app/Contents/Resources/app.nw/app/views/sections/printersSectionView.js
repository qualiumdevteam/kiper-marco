var app = app || {};
app.PrintersSectionView = Backbone.View.extend({
    el: '.section[data-section="printers"]',

    events: {
        //"submit #printer-selection": "saveSettings",
        "submit #printerForm" : "createPrinter",
        "submit #printsettings" : "saveSettings"
    },

    createPrinter : function(e){
        var paper = e.target.papel.value;
        //alert(paper)
        if(paper === ""){
            alert("Debe escribir el tamaño del papel de la impresora");
            return false;
        }else{
            app.Printers.create(this.newAttributes());
            this.$form[0].reset();
            this.renderPrinters();
            //app.Helper.loadTemplate('printers_list',{ printers : app.Printers.toJSON(), section : true }, this.$printerview);
            return false;
        }
    },

    saveSettings : function(e){
        e.preventDefault();
        var selected = parseInt(this.$settingsForm[0].ticket_size.value);
        var printer = app.Settings.findWhere({ itemName : "printerSize" })
        var max = _.max(app.Settings.toJSON(),function(max){ return max.id });
        if(printer != undefined){
         printer.set({itemValue: selected});
        }else{
            app.Settings.create({
                id : (max.id > 0) ? (max.id+1) : 1 ,
                itemName : "printerSize",
                itemValue :  selected
            });
        }
        return false;

    },

    newAttributes : function(){
        var max = _.max(app.Printers.toJSON(),function(max){ return max.id });
        return {
            id : (max.id > 0) ? (max.id+1) : 1,
            papel : this.$form[0].papel.value
        }
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {

        this.render();
        this.$settingsForm = this.$('#printsettings');
        this.$form = this.$('#printerForm');
        this.$printerview = this.$('#ticket-list tbody');
        // this.renderPrinters();

        //app.Helper.loadTemplate('printers_list',{ printers : app.Printers.toJSON(), section : true }, this.$printerview);
        this.renderPrinters();
        /*this.render();
        this.$form = this.$('#productForm');
        this.$productsHandler = this.$('#product-list tbody');
        app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON(), section:true }, this.$form );
        this.renderProducts();

        this.listenTo( app.Products, 'add', this.renderProducts );
        this.listenTo( app.Products, 'remove', this.deleteProduct );*/


    },

    renderPrinters : function(){
        var printer = app.Printers.models,
            $this = this;

        this.$printerview.html('');

        _.each( printer, function( prt ){
            var printView = new app.PrinterView( prt );
            $this.$printerview.append( printView.render().el );
        } );
    },

    render: function() {
        var printerSettings = app.Settings.findWhere({itemName: 'ticket_size'}) || {};
        if (printerSettings.attributes !== undefined) printerSettings = printerSettings.toJSON();
        app.Helper.loadTemplate( 'sections/printers', printerSettings, this.$el );
        //console.log(printerSettings);
        return this;
    }

    /*saveSettings: function(e) {
        e.preventDefault();
        var printerSettings = app.Settings.findWhere({itemName: 'ticket_size'});

        if ( printerSettings ) {
            printerSettings.save({itemValue: this.$form[0].ticket_size.value}, {wait: true});
        } else {
            printerSettings = app.Settings.create({
                itemName: 'ticket_size',
                itemValue: this.$printerSelect.val()
            }, {wait: true});
        }

        if ( printerSettings.validationError )
            app.Helper.showAlert({
                msg: printerSettings.validationError,
                size: 'small',
                type: 'error'
            });
        else {
            app.Helper.showAlert({
                msg: 'Las configuraciones se han guardado correctamete',
                size: 'small',
                type: 'info'
            });
        }
    }*/
});