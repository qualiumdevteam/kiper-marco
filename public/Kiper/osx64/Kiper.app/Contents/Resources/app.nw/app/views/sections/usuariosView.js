var app = app || {};

app.UsuariosView = Backbone.View.extend({

    el: '.section[data-section="usuarios"]',

    events: {
        "click .addUser": "openform",
    },
    openform: function( e ) {
        e.preventDefault();
            var popup = app.Helper.showPopup({
                msg:'UsuarioNewView',
                id:'popup-editar-usuario',
                msgType: 'clear',
                size: 'large',
                closable: true,
            });
    },

    createItem: function() {
        var popup = app.Helper.showPopup({
            msg:'UsuarioNewView',
            id:'popup-nuevo-usuario',
            msgType: 'clear',
            size: 'large',
            closable: true});
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Usuario');
        addButton.data('action','usuarios');
    },

    initialize: function() {
        this.render();
        this.$usersHandler = this.$('#users-list');
        this.renderUsers();
        this.listenTo( app.Usuarios, "add", this.renderUsers);
        this.listenTo( app.Usuarios, "change", this.renderUsers);
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/usuarios', null, this.$el );
        return this;
    },
    // renderUser:function(user){
    //     if (user.cid !== app.userSession) {
    //     this.$usersHandler.append(new app.UserGalleryView( user ).render().el );
    //   };
    // },


    renderUsers: function() {
        // this.$usersHandler.html(' ');
        // app.Usuarios.each(this.renderUser, this);
        var usuarios = app.Usuarios.models,
            $this = this;
        usuarios = _.reject( usuarios, function( user ) {
            return user.cid === app.userSession
        } );
        this.$usersHandler.html(' ');
          _.each( usuarios, function( user ) {
               var userView = new app.UserGalleryView( user );
             //console.log(user.id);
             /*var li = $('#users-list');
             for (var i = 0; i < li[0].children.length; i++) {
             console.log(user.id);
             var lid = $('#users-list li').attr("id");
             console.log(lid);
             if(lid != user.id){
              $this.$usersHandler.append( userView.render().el );    
             }else{console.log("no");}
              };*/
             $this.$usersHandler.append( userView.render().el ); 
        });
        //console.log('en el evento render');
    }
});