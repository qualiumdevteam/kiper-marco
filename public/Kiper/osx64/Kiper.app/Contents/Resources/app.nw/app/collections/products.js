var app = app || {};
var ProductCollection = Backbone.Collection.extend({
    model: app.Product,
    url : app.resourcesDir+"/dbfiles/productos.json",
    sync : function(method, model, attrs){
        //console.log(method,model)
        if(method === "read"){
            var db = fs.readFileSync(this.url);
            if (db.length>0) {
             this.reset(JSON.parse(db.toString()));
            };
            //console.log(JSON.parse(db.toString()))
        }

        if(method === "set"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }

        if(method === "remove"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }
    },
    create : function(attr){
        console.log("create",attr);
        //primero se llama el methodo add para añadirlo a la coleccion
        //despues se llama el sync
        var model = this.add(attr);
        this.sync("set",this,attr);
        return model;
    },
    //localStorage: new Backbone.LocalStorage('products-togo'),

    comparator: function( product ) {
        return product.get( 'code' );
    }
    
});

app.Products = new ProductCollection();
app.Products.resetOrder = function() {
    this.comparator = function(model) { return model.get('code') };
    this.sort();
};
app.Products.resort = function( prop ) {
    this.comparator = function(model) { return model.get(prop) };
    this.sort();
};