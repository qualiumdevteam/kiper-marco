var app = app || {};
app.PedidoEditView = Backbone.View.extend({
    tagName: 'div',

    events: {
        "change #categoryID": "filterMenu",
        "click #products-list li": "addProduct",
        "click a.update-order": "updateOrder",
        "keyup [name='recibe']": "payEntered",
        "keyup input[name='search']": "searchProduct",
        "click .close": "Close"
    },
        searchProduct : function(e){
        e.preventDefault();
        var input = $(e.target).context.value;
        var filter = _.filter(app.Products.toJSON(),function(prod){
           var valid = false;
            if(prod.name.indexOf(input) != -1 || prod.price.indexOf(input) != -1 || prod.description.indexOf(input) != -1){
                valid = true;
            }
            return valid;
        });
         
        this.$menuHandler.html('');
        app.Helper.loadTemplate( 'menu_item', {productos: filter}, this.$menuHandler );
        app.Products.resetOrder();
    },
    attrsBitacora: function() {
        var today = new Date();
        var user = app.Usuarios.get( app.userSession);
        var details = [];
        var productos = app.PedidosProductos.where({pedidoID: this.model.get('id')});

        _.each(productos, function(producto){
            details.push( producto.toJSON() );
        });

        return {
            action:   'edit',
            date:     today.toLocaleDateString(),
            username: user.get('username'),
            sucursal: app.Sucursales.get( user.get('sucursalID')).get('name'),
            amount:   this.getTotal(),
            details:  JSON.stringify( details ),
            pedidoID: this.model.get('id')
        }
    },

    addProduct: function(e) {
        var $li = $(e.target).closest('li');
        var model = app.Products.get($li.data('id'));
        var pedidoProducto = new app.PedidoProducto( this.attrsPedidoProducto( model ) );

        var existent = app.PedidosProductos.findWhere( {
            productoID: pedidoProducto.get('productoID'),
            pedidoID: this.model.get('id')
        } );

        if (existent)
            existent.set({ quantity: parseInt(existent.get('quantity')) + 1 });
        else
            app.PedidosProductos.create( pedidoProducto.toJSON() );

        this.renderProducts();
        this.setTotal();
    },

    attrsPedidoProducto: function( producto ) {
        return {
            pedidoID:   this.model.get('id'),
            productoID: producto.get('id'),
            price:      producto.get('price'),
            name:       producto.get('name'),
            quantity:   1
        }
    },

    calcChange: function() {
        this.$cambio.val( (parseFloat( this.$recibe.val() ) - this.getTotal()).toFixed(2) );
    },

    filterMenu: function(e) {
        e.preventDefault();
        var sel = $(e.target),
            catID = (sel.val() === 'all') ? undefined :  parseInt(sel.val());
        this.renderMenu(catID);
    },

    getTotal: function() {
        var total = 0;
        var productos = app.PedidosProductos.where( {pedidoID: this.model.get('id')} );
        _.each( productos, function(producto){
            total += producto.get('price') * producto.get('quantity');
        } );

        return parseFloat( total).toFixed(2);
    },

    initialize: function( attrs ) {
        if ( attrs.id === undefined )
            return false;

        this.model = app.Pedidos.get(attrs.id);
        this.render();

        this.$menuHandler = this.$('#products-list');
        this.$productsHandler = this.$('#order-list');
        this.$totalHandler = this.$('#total-amount');
        this.$recibe = this.$('[name="recibe"]');
        this.$cambio = this.$('[name="cambio"]');
        this.$aplicar = this.$('#apl2');

        this.renderMenu();
        this.renderProducts();

        this.listenTo( app.PedidosProductos, "create", this.updateSection );
        this.listenTo( app.PedidosProductos, "change", this.setTotal );
        this.listenTo( app.PedidosProductos, "destroy", this.setTotal );
        this.listenTo( app.PedidosProductos, "destroy", this.renderProducts );
    },

    payEntered: function(e) {
        e.preventDefault();
        this.calcChange();
    },

    render: function() {
        var pedido = this.model.toJSON();
        var categories = app.Categories.toJSON();
        var cliente = app.Clientes.get(pedido.clienteID).toJSON();
        var metodos = app.MetodosPago.toJSON();

        var data = {
            categories: categories,
            cliente: cliente,
            pedido: pedido,
            metodos: metodos
        };

        app.Helper.loadTemplate( 'popups/pedido_edit', data, this.$el );
    },

    renderMenu: function( catID ) {
        app.Products.resort('name');
        this.$menuHandler.html('');
        var productos = (catID !== undefined) ? JSON.parse(JSON.stringify(app.Products.where({categoryID: catID}))) : app.Products.toJSON();
        app.Helper.loadTemplate( 'menu_item', {productos: productos}, this.$menuHandler );
        app.Products.resetOrder();
    },

    renderProducts: function() {
        var handler = this.$productsHandler.find('tbody');
        var productos = app.PedidosProductos.where({pedidoID:this.model.get('id')});

        handler.html('');
        _.each( productos, function( producto ){
            handler.append( new app.ProductOrderView( producto ).render().el );
        } );
    },

    setTotal: function() {
        this.$totalHandler.html( this.getTotal() );

        if ( this.$recibe.val().length > 0 )
            this.calcChange();
    },

    updateOrder: function(e) {
        e.preventDefault();
        var error = false;

        if ( ! this.validateProducts() ) {
            alert('Debes agregar al menos un producto');
            error = true;
        }

        if ( this.$recibe.val().length > 0 && parseFloat(this.$recibe.val()) < this.getTotal() ) {
            alert( 'La cantidad recibida debe ser mayor que el total de la orden' );
            error = true;
        }

        if ( this.$aplicar[0].checked && this.$recibe.val().length === 0 ) {
            alert( 'Debe ingresar un monto a pagar' )
            error = true;
        }

        if (error) return;

        var newAttrs = { pagado: 0, comentarios: '' };

        if ( this.$aplicar[0].checked && this.$recibe.val().length > 0)
            newAttrs.pagado = 1;
        else {
            //console.log(this.$recibe.val(), this.$cambio.val());
            newAttrs.comentarios = 'El cliente va a pagar con $' + this.$recibe.val() + '<br> El cambio correspondiente es de $' + this.$cambio.val();
        }

        this.model.save( newAttrs );
        app.BitacoraRows.create( this.attrsBitacora() );
        app.dashboardView.renderlistado();
        app.pedidosView.renderPedidos( true );
        app.reportesView.renderReports();
        this.undelegateEvents();
        this.$el.closest('.popup').foundation('reveal', 'close');
        this.Close();
    },
    Close:function(){
              $('#popup-editar-pedido').remove();
        $('.reveal-modal-bg').remove();

    },

    updateSection: function() {
        app.pedidosView.renderProducts();
    },

    validateProducts: function() {
        var productos = app.PedidosProductos.where({pedidoID: this.model.get('id')});
        if (productos.length > 0)
            return true;
        else
            return false;
    }
});