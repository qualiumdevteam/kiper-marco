var app = app || {};
app.UserGalleryView = Backbone.View.extend({

    tagName: 'li',

    events: {
        "click .gallery-pic": "details",
        "click figure .details": "details",
        "click figure .edit": "edit",
        "click figure .delete": "clear"
    },

    clear: function( e ) {
        e.preventDefault();


        var confirm = app.Helper.showConfirm({
            msg: '¿Esta segur@ que desea eliminar este elemento?',
            msgType: 'notice',
            closable: true
        });

        confirm.done($.proxy(function() {
            this.model.destroy();
            this.collection.sync("remove");
            this.remove();
        }, this));
    },

    details: function( e ) {
        e.preventDefault();
        var div = $('<div></div>'),
            data = {
                usuario: this.model.toJSON()
            };
        data.usuario.nombreCompleto = this.model.get('nombres') + ' ' + this.model.get('apellidos');
        data.usuario.sucursalName = app.Sucursales.get( this.model.get('sucursalID')).get('name');
        data.usuario.perfilName = app.Profiles.get( this.model.get('perfil')).get('name');

        app.Helper.loadTemplate( 'user_details_popup', data, div );
        app.Helper.showAlert({
            msg: div.html(),
            msgType: 'info',
            closable: true,
            size: 'large'
        });
    },

    edit: function( e ) {
        e.preventDefault();

        var popup = app.Helper.showPopup({
            msg:'UsuarioNewView',
            id:'popup-editar-usuario',
            msgType: 'clear',
            size: 'large',
            closable: true,
            data: { id: this.model.get('id') }
        });
    },

    initialize: function( model ) {
        this.model = model;
    },
    render: function() {
        sucursalN = app.Sucursales.get(this.model.get('sucursalID')).get('name');
        this.model.set("sucursalN",sucursalN);
        app.Helper.loadTemplate( 'user_gallery_item', this.model.toJSON(), this.$el);
        return this;
    }

});