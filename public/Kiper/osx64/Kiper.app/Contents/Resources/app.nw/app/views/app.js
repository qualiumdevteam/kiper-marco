var app = app || {};

var gui = require('nw.gui');
var win = gui.Window.get();

app.AppView = Backbone.View.extend({
    el: '#app',
    
    events: {
        "click .close" : "remove"
    },
    initialize: function() {
        this.nav = this.$('nav');
        var nombreEmpresa = app.Settings.findWhere({ itemName: 'nombre_empresa' });
        if (nombreEmpresa) {
                this.loadLogin();
        } else {
            this.loadSettingsForm();
        }

    },
    colorReport:function(){
    //console.log("color");
    $("tspan").attr({fill: "#000",});
    },

    logout: function() {
        sessionStorage.clear('togo-session-id');
        app.userSession = undefined;
        //this.loadLogin();
        location.reload();
    },

    render: function() {},

    /* =Load Category Wizard
     ------------------------------- */
    loadCategoriesWizard: function() {
        var alert = app.Helper.showConfirm( {
                msg: 'Aún no has ingresado CATEGORÍAS Y PRODUCTOS, es necesario ingresarlos para comenzar a usar el programa, ¿desea ingresarlos ahora?',
                msgType: 'notice',
                closable: false,
                id: 'notice-close-categories'
            }),
            $this = this;
        alert.done( function() {
            $this.$('#app-content').html(new app.SetupWizard().el);
        })
            .fail( function() {
                app.Helper.showAlert( { msg: 'Se cerrará el programa', closable: false, id: 'alert-closing' } )
                    .done( function() {
                        win.on('close', function() {
                            this.hide(); // Pretend to be closed already
                            this.close(true);
                        });
                        win.close();
                    });
            } );
    },

    /* =Load Login
    ------------------------------ */
    loadLogin: function() {
        app.Helper.loadTemplate( 'login', null, this.$('#app-content'));
        new app.LoginView();
    },

    /* =Load Screens
     ------------------------------ */
    loadScreens: function() {
        app.layoutView = new app.LayoutView();
    },

    /* =Load Settings Form
    ------------------------------- */
    loadSettingsForm: function() {
        app.Helper.loadTemplate( 'settings', {
            sucursales:app.Sucursales.toJSON()}, this.$('#app-content'));
        new app.SettingsView();
    },


    /* =Setup Autoheight
    ------------------------------- */
    setupAutoheight: function() {
        var autoHeights = this.$('.auto-height'),
            autoHeightSections = this.$('.auto-height-section');

        _.each(autoHeights, function(item){
            var $item = $(item),
                cell = $item.closest('.cell');
            $item.height(cell.height() - $item.position().top);
        });

        _.each(autoHeightSections, function(item){
            var $item = $(item).find('.scroll-section'),
                cell = $item.closest('.cell'),
                prev = $item.prev(),
                next = $item.next(),
                adjust = $item.data('adjust') || 0,
                height = 0;
            height = cell.height() - $item.position().top;
            if ( next.length > 0 ){ 
                height -= next[0].clientHeight;
            };
            if (prev.length > 0 && $item.position().top === 0 ){ 
                height -= 45;
            };
            $item.height( height - adjust );
            $item.css('max-height', height);
        });
    }
});