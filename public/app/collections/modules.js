var app = app || {};
var ModuleCollection = Backbone.Collection.extend({
    model: app.Module,

    comparator: function( module ) {
        return module.get('name');
    }
});

app.Modules = new ModuleCollection([
    { name: 'categorias' },
    { name: 'clientes' }
]);