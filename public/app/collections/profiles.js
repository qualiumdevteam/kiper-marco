var app = app || {};
var ProfileCollection = Backbone.Collection.extend({
    model: app.Profile,

    //localStorage: new Backbone.LocalStorage('profiles-togo'),
   url : app.resourcesDir+"/dbfiles/profiles.json",
    sync : function(method, model, attrs){
        //console.log(method,model)
        if(method === "read"){
            var db = fs.readFileSync(this.url);
             if (db.length>0) {
            this.reset(JSON.parse(db.toString()));
            //console.log(JSON.parse(db.toString()))
        }
        }

        if(method === "set"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }

        if(method === "remove"){
            //console.log("set",JSON.stringify(attrs));
            //console.log(this.toJSON());
            //borramos el contenido de la tabla y lo sobreescribimos
            fs.truncateSync(this.url,0);
            fs.writeFileSync(this.url, JSON.stringify(this.toJSON()));
        }
    },
    create : function(attr){
        console.log("create",attr);
        //primero se llama el methodo add para añadirlo a la coleccion
        //despues se llama el sync
        var model = this.add(attr);
        this.sync("set",this,attr);
        return model;
    },
    comparator: function( profile ) {
        return profile.get('name');
    }
});


app.Profiles = new ProfileCollection();