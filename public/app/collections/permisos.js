var app = app || {};
var PermisoCollection = Backbone.Collection.extend({
    model: app.Permiso,

    comparator: function( permiso ) {
        return permiso.get('name');
    }
});

app.Permisos = new PermisoCollection([
    { name: 'user' },
    { name: 'admin' }
]);