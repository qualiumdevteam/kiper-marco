var app = app || {};
var fs = require('fs');

;(function($){

    app.Helper = {
        animateScreen: function( from, to, options ) {
            var defaults = {
                    speed: options.speed || 300,
                    effect: 'easeInOutExpo',
                    direction: options.direction || 'forward'
                },
                dfd = $.Deferred();


            if ( defaults.direction === 'forward' ) {
                defaults.fromEndPos = '-100%'
                defaults.toIniPos = '100%'
            } else {
                defaults.fromEndPos = '100%'
                defaults.toIniPos = '-100%';
            }

            from.css({
                'position': 'absolute',
                'width': '100%'
            });
            from.animate({
                'left': defaults.fromEndPos,
                'opacity': 0
            }, defaults.speed, defaults.effect, function() { from.hide() });

            to.css({
                'opacity': 0,
                'position': 'absolute',
                'left': defaults.toIniPos,
                'top': 0,
                'width': '100%'
            });
            to.show();
            to.animate({
                'opacity': 1,
                'left': 0
            }, defaults.speed, defaults.effect, function(){ dfd.resolve() });

            return dfd.promise();
        },

        appendTemplate: function( tpl, attrs, layer ) {
            var template = '';
            var file = fs.readFileSync(app.templatesDir+tpl+'.html', 'utf-8');
            template = Handlebars.compile( file.toString() );
            layer.append(template(attrs));
        },

        getMonthText: function( month ) {
            var arrMonths = {
                '01': 'Enero',
                '1' : 'Enero',
                '02': 'Febrero',
                '2' : 'Febrero',
                '03': 'Marzo',
                '3' : 'Marzo',
                '04': 'Abril',
                '4' : 'Abril',
                '05': 'Mayo',
                '5' : 'Mayo',
                '06': 'Junio',
                '6' : 'Junio',
                '07': 'Julio',
                '7' : 'Julio',
                '08': 'Agosto',
                '8' : 'Agosto',
                '09': 'Septiembre',
                '9' : 'Septiembre',
                '10': 'Octubre',
                '11': 'Noviembre',
                '12': 'Diciembre'
            };

            return arrMonths[month];
        },

        getYearsDiff: function( fecha ) {
            var fechaParts = fecha.split('-');
            var fDate = new Date(fechaParts[0], (parseInt(fechaParts[1]) - 1).toString(), fechaParts[2]);
            var today = new Date();
            var timeDiff = Math.abs(today.getTime() - fDate.getTime());
            var yearDiff = Math.floor( timeDiff / (1000 * 3600 * 24 * 365) );
            return yearDiff;
        },

        getFile: function( fileName ) {
            var file = fs.readFileSync(fileName, 'utf-8');
            return file ? file : false;
        },

        getFulltimeString: function() {
            var date = new Date();
            return date.getFullYear().toString()
                + date.getMonth().toString()
                + date.getDate().toString()
                + date.getHours().toString()
                + date.getMinutes().toString()
                + date.getSeconds().toString()
                + date.getMilliseconds().toString();
        },

        getTimeDiff: function(from, to, timeType) {
            var type = timeType || 'mins';
            var typeValues = {
                'segs':  1000,
                'mins':  1000 * 60,
                'hours': 1000 * 60 * 60,
                'day':   1000 * 60 * 60 * 24
            };

            return (to - from) / typeValues[type];
        },

        getTempFile: function( fileName ) {
            var file = fs.readFileSync(fileName, null);
            return file ? file : false;
        },

        getTemplate: function( tpl ) {
            var file = fs.readFileSync(app.templatesDir+tpl, 'utf-8');
            return Handlebars.compile( file.toString() );
        },

        loadTemplate: function( tpl, attrs, layer ) {
            var template = '';
            var file = fs.readFileSync(app.templatesDir+tpl+'.html', 'utf-8');

            //console.log(file)

            template = Handlebars.compile( file.toString() );
            layer.html(template(attrs));
        },

        moveFile: function( fileName ) {
                var file = fs.readFileSync(fileName,null);

                if(file != null && file != undefined){
                    return fs.renameSync(fileName, fileName.replace('/temp/','/uploads/'));
                }else{return false;}
        },

        rename: function( filename ) {
            var file = filename;
            //file = file.replace(' ','_');
            file = file.replace('-','_');
            file = file.replace('à','a');
            file = file.replace('è','e');
            file = file.replace('ì','i');
            file = file.replace('ò','o');
            file = file.replace('ù','u');
            file = file.replace('ú','u');
            file = file.replace('í','i');
            file = file.replace('ó','o');
            file = file.replace('é','e');
            file = file.replace('á','a');
            file = file.replace('&','_');
            return file;
        },
        fileExist : function(fileName){
            var file = fs.existsSync(app.uploadDir2+fileName);
            return file ? file : false;
        },
        deleteFile : function(fileDir){
            fileDir = fileDir.replace('%20',/\s/g).replace(/\(/g,"%28").replace(/\)/g,"%29");
            if(this.fileExist(fileDir)){
                 return fs.unlinkSync(app.uploadDir2+fileDir);
            }
        },
        showAlert: function( attrs ) {
            return this.showMessage( attrs, 'alert' );
        },

        showConfirm: function( attrs ) {
            return this.showMessage( attrs, 'confirm' );
        },

        showPopup: function( attrs ) {
            return this.showMessage( attrs, 'popup' );
        },

        showMessage: function( attrs, modalType ) {
            $("#myModal").remove();
            var dfd       = $.Deferred(),
                msg       = attrs.msg || '',
                type      = attrs.msgType || 'info',
                closable  = attrs.closable || false,
                id        = attrs.id || 'myModal',
                size      = attrs.size || 'small',
                modalType = modalType || 'alert',
                template  = 'message_' + modalType,
                data      = attrs.data || {};

            
            var templateAttrs = { msg: msg, id: id, type: type, size: size, modalType: modalType };
            if ( closable === true ) templateAttrs.close = 1;
            if ( modalType === 'popup' ) templateAttrs.msg = '';
            
            app.Helper.appendTemplate( template, templateAttrs, $('body') );
            var modal = $('#' + id),
                okBtn = modal.find('.ok-btn'),
                noBtn = modal.find('.no-btn'),
                closeBtn = modal.find('.close-reveal-modal');


            if ( modalType === 'popup' ) {
                modal.find('.view-wrapper').html( new app[msg]( data ).el );
            }
            modal.foundation( 'reveal', { animation: 'fade', animationSpeed: 10 } );
            modal.foundation( 'reveal', 'open' );
            okBtn.on('click', function(){
                modal.foundation( 'reveal', 'close');
                $('.reveal-modal-bg').remove();
                $(".reveal-modal").remove();
                setTimeout(function(){
                    dfd.resolve();
                    modal.remove();
                }, 10);
            });

            noBtn.on('click', function(){
                modal.foundation( 'reveal', 'close');
                $('.reveal-modal-bg').remove();
                setTimeout(function(){
                    dfd.reject();
                    modal.remove();
                }, 10);
            });

            closeBtn.on('click', function(){
                modal.foundation( 'reveal', 'close');
                $('.reveal-modal-bg').remove();
                setTimeout(function(){
                    dfd.reject();
                    modal.remove();
                }, 10);
            });
            return dfd.promise();
        },

        timeDiff: function(from, to) {
            var fromMilli = from.getTime();
            var toMilli = to.getTime();
            return Math.ceil((toMilli - fromMilli) / (1000 * 60));
        },


        getFileExtension : function(fileName){
            return fileName.split('.').pop();
        },

        uploadDbBackup : function(fileInput,type){
            var file = fileInput.files[0],
                source = file.path,
                destiny = app.uploadDir2,
                fileName = "",
                newPath = destiny;


           // console.log(file);

            var self =this;

         //validar extension de archivo zip
            if(self.getFileExtension(file.name) == "zip"){
                if(type != "db"){
                    fileName = 'backup_images.zip';
                }else{
                    fileName = 'backup_db.zip';
                }

                fs.readFile(source, function (err, data) {
                    var imagesName = fileName;
                        if(!imagesName){

                            self.showAlert({ msg : "Ha ocurido un error al procesar su imagen!" })

                        } else {
                            var newPath = destiny + imagesName;

                            /// write file to uploads/fullsize folder
                            fs.writeFile(newPath, data, function (err) {
                                 newPath = destiny+fileName.toString();
                            });
                        }

                });

                return newPath;
            }else{
                self.showAlert({ msg : "Por favor seleccione archivos con extensión '.zip'" })
            }
        },
     uploadFile: function( fileInput, isTemp ) {
            var file = fileInput.files[0],
                source = file.path,
                isTemp = isTemp || false,
                destiny = app.uploadDir2,
                fileName = 'img_'+app.Helper.getFulltimeString()+'.jpg',
                newPath = fileName.toString();
                //fs.renameSync(source,destiny+fileName);
            fs.readFile(source, function (err, data) {
                var imageName = fileName;
                /// If there's an error
                if(!imageName){
                    this.showAlert({ msg : "Ha ocurido un error al procesar su imagen!" })
                } else {
                    var newPath = destiny + imageName;
                    /// write file to uploads/fullsize folder
                    fs.writeFile(newPath, data, function (err) {
                        newfile = destiny+ fileName.toString();
                        /// let's see it
                        //res.redirect("/uploads/fullsize/" + imageName);
                        //window.location = newPath;
                    });
                }
            });
            return newPath;
        },
        uploadUserFile: function(path, name, isTemp ) {
            var file = name,
                source = path,
                isTemp = isTemp || false,
                destiny = app.uploadDir2,
                fileName = 'img_'+app.Helper.getFulltimeString()+'.jpg',
                newPath = fileName.toString();
            //console.log(source,destiny+fileName);
            //fs.renameSync(source,destiny+fileName);
            fs.readFile(source, function (err, data) {
                var imageName = fileName;
                /// If there's an error
                if(!imageName){
                    this.showAlert({ msg : "Ha ocurido un error al procesar su imagen!" })
                } else {
                            var newPath = destiny + imageName;

                    /// write file to uploads/fullsize folder
                    fs.writeFile(newPath, data, function (err) {
                        newPath = destiny+fileName.toString();
                        /// let's see it
                        //res.redirect("/uploads/fullsize/" + imageName);
                        //window.location = newPath;
                    });
                }
            });


            return newPath;
        },

        stripslashes : function(str) {
        //       discuss at: http://phpjs.org/functions/stripslashes/
        //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        //      improved by: Ates Goral (http://magnetiq.com)
        //      improved by: marrtins
        //      improved by: rezna
        //         fixed by: Mick@el
        //      bugfixed by: Onno Marsman
        //      bugfixed by: Brett Zamir (http://brett-zamir.me)
        //         input by: Rick Waldron
        //         input by: Brant Messenger (http://www.brantmessenger.com/)
        // reimplemented by: Brett Zamir (http://brett-zamir.me)
        //        example 1: stripslashes('Kevin\'s code');
        //        returns 1: "Kevin's code"
        //        example 2: stripslashes('Kevin\\\'s code');
        //        returns 2: "Kevin\'s code"

        return (str + '')
            .replace(/\\(.?)/g, function(s, n1) {
                switch (n1) {
                    case '\\':
                        return '\\';
                    case '0':
                        return '\u0000';
                    case '':
                        return '';
                    default:
                        return n1;
                }
            });
     }
    }

    app.Helper.OffCanvas = function( id ) {
        this.$el = $('<div></div>').addClass('off-canvas').attr('id', id).appendTo($('body'));
        console.log(this, this.$el);
        return this;
    };

    app.Helper.OffCanvas.prototype.hide = function() {
        this.$el.removeClass('hide');
        return this;
    }

    app.Helper.OffCanvas.prototype.remove = function() {
        this.$el.remove();
    }

    app.Helper.OffCanvas.prototype.show = function() {
        this.$el.addClass('show');
        return this;
    }

    Handlebars.registerHelper('categoryName', function( id, options ){
        return app.Categories.get(id).get('name');
    });

    Handlebars.registerHelper('ifCond', function(v1, operator, v2, options){
        switch(operator) {
            case '==':
                return (v1 == v2) ? options.fn(this) : options.inverse(this);
                break;
            case '===':
                return (v1 === v2) ? options.fn(this) : options.inverse(this);
                break;
            case '!==':
                return ( v1 !== v2 ) ? options.fn(this) : options.inverse(this);
                break;
            case '<':
                return (v1 < v2) ? options.fn(this) : options.inverse(this);
                break;
            case '<=':
                return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                break;
            case '>':
                return (v1 > v2) ? options.fn(this) : options.inverse(this);
                break;
            case '>=':
                return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                break;
            default:
                return options.inverse(this);
                break;
        }
    });

    Handlebars.registerHelper('getImagePath', function(imageName, options ) {
        return imageName;
    });
     Handlebars.registerHelper('direction', function(foto, options ) {
        return app.uploadDir+"/"+foto;
    });

    Handlebars.registerHelper('nombreCompleto', function( userID, options) {
        var user = app.Usuarios.get( userID );
        return user.get('apellidos') + ' ' + user.get('nombres');
    });

})(jQuery);