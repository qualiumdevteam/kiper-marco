var app = app || {};

app.Cliente = Backbone.Model.extend({
    defaults: {
        id : 0,
        telefono: '',
        nombres: '',
        apellidos: '',
        direccion: '',
        sexo: 'm',
        fechaNacimiento: '',
        twitter: '',
        correo: '',
        comentarios : ''
    },

    toJSON: function() {
        var  attrs = this.attributes;

        attrs.edad = this._calculateAge( attrs.fechaNacimiento );

        return attrs;
    },

    save : function(attrs){
        this.set(attrs);
        this.collection.sync("set",this,attrs);
    },

    _calculateAge : function(birthday) { // birthday is a date
        var birthday = new Date(birthday);
        var ageDifMs = Date.now() - new Date(birthday.getFullYear(),birthday.getMonth(),birthday.getDate()).getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    },

    validate: function( attrs ) {
        var errors = [];

        if ( attrs.telefono.length === 0 )
            errors.push({ field: 'telefono', msg: 'El Campo %f es Obligatorio' });

        if ( attrs.nombres.length === 0 )
            errors.push({ field: 'nombres', msg: 'El Campo %f es Obligatorio' });

        if ( attrs.direccion.length === 0 )
            errors.push({ field: 'direccion', msg: 'El Campo %f es Obligatorio' });

        if ( attrs.sexo.length === 0 )
            errors.push({ field: 'sexo', msg: 'El Campo %f es Obligatorio' });

        return errors.length === 0 ? false : errors;
    }
});