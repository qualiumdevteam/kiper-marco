var app = app || {};

app.Usuario = Backbone.Model.extend({

    defaults: {
        id : 0,
        nombres: '',
        apellidos: '',
        correo: '',
        username: '',
        password: '',
        perfil: 0,
        foto: '',
        sucursalID: 0
    },

    save : function(attrs,options){
        this.set(attrs);
        this.collection.sync("set",this,attrs);
    },

    initialize: function() {
        this.on("destroy", this.removeImage);
    },

   removeImage: function(){
    var img = this.get('foto');  
        if (img !== "" ) {
            var exist = app.Helper.fileExist(img);  
            if (exist) {
                app.Helper.deleteFile(img);
            };
        }
    },

    validate: function( attrs, options ) {
        if ( attrs.nombres.length === 0
            || attrs.apellidos.length === 0
            || attrs.correo.length === 0
            || attrs.username.length === 0
            || attrs.password.length === 0
            || attrs.perfil.length === 0
            || attrs.sucursalID.length === 0)
            return 'Debe llenar todos los campos';

        if ( attrs.password !== attrs.confirmar )
            return 'Las contraseñas no coinciden';
    }
});