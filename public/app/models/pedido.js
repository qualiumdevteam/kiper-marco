var app = app || {};

/**
 * Opciones de Status
 *
 * 0    => Open
 * 1    => Pending
 * 2    => Delivered
 * 3    => Deleted
 * 4    => Canceled
 *
 * -------------------
 *
 * Color Status
 *
 * green   => until 15 minutes
 * yellow  => from 15 to 30 minutes
 * orange  => from 30 to 45 minutes
 * red     => since 45 minutes
 *
 * */

app.Pedido = Backbone.Model.extend({
    defaults: {
        id : 0,
        clienteID: 0,
        usuarioID: 0,
        fecha: new Date().getTime(),
        folio: 0,
        horaCaptura: '',
        horaEntrega: '',
        horaEnvio: '',
        comentarios: '',
        metodoPago: 0,
        pagado: 0,
        status: 0,
        cliente : "",
        colorStatus: 'green'
    },

    getTotal: function() {
        var total = 0;
        var productos = app.PedidosProductos.where({pedidoID: this.get('id')});

        _.each(productos, function(producto){ total += parseFloat(producto.get('price')) * parseFloat(producto.get('quantity')) });

        return total;
    },

    getVendedor: function() {
        return app.Usuarios.get( this.get('usuarioID') );
    },

    save : function(attrs,options){
        this.set(attrs);
        this.collection.sync("set",this,attrs);
    },

    toJSON: function() {
        var attrs = this.attributes;
        var vendedor = this.getVendedor();

        attrs.costo = '$' + this.getTotal().toFixed(2);
        attrs.texto = this.toText();
        attrs.fechaLocale = new Date( this.get('fecha') ).toLocaleDateString();
        attrs.vendedor = vendedor.get('nombres') + ' ' + vendedor.get('apellidos');

        if (attrs.status === 2)
            attrs.colorStatus = 'white';

        return attrs;
    },

    toText: function() {
        var text = '';
        var productos = app.PedidosProductos.where({pedidoID: this.get('id')});

        _.each(productos, function(producto){
            text += '<p>' + producto.get('quantity') + ' ' + producto.get('name') + '</p>';
        });

        return text;
    },

    validate: function( attrs ) {
        if ( attrs.clienteID.length === 0
            || attrs.usuarioID.length === 0)
            return 'Debe seleccionar un cliente';
    }
});