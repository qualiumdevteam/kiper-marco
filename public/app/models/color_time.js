var app = app || {};

app.ColorTime = Backbone.Model.extend({
    defaults: {
        color: '',
        from: 0,
        to: 0
    }
});