var app = app || {};

app.Setting = Backbone.Model.extend({
    defaults: {
        id : 0,
        itemName: '',
        itemValue: ''
    },

    save : function(attrs,options){
        //console.log("categorias",attrs);
        this.set(attrs,{validate : true});
        this.collection.sync("set",this,attrs);
    },

    validate: function( attrs, options ) {
        if ( attrs.itemName.length === 0 || attrs.itemValue.length === 0 )
            return 'Debe proporcionar todos los datos de configuración.';
    }
});