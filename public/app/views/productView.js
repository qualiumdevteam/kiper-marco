var app = app || {};

app.ProductView = Backbone.View.extend({

    tagName: 'tr',
    
    events: {
        "click .delete": "clear",
        "click .view": "details",
        "submit form.update-product": "updateProduct",
    },

    clear: function( e ) {
        e.preventDefault();
        var form = $('#productForm').hasClass("updating");
         if (form !== true) {
        var confirm = app.Helper.showConfirm({
            msg: '¿Esta segur@ que desea eliminar este elemento?',
            msgType: 'notice',
            closable: true
        });

        confirm.done($.proxy(function() {
            this.model.destroy();
            this.collection.sync("remove");
            this.remove();
        }, this));
       }else{
        app.Helper.showAlert( {
                msg: "No puede eliminar elementos mientras edita otro",
                msgType: 'notice'
            });
       }  
    },

    details: function( e ) {
        e.preventDefault();
        var div = $('<div></div>');
        app.Helper.loadTemplate( 'product_details_popup', this.model.toJSON(), div );
        app.Helper.showAlert({
            msg: div.html(),
            msgType: 'info',
            closable: true,
            id: 'details-product-' + this.model.get('id'),
            size: 'large'
        });
    },

    initialize: function( model ) {
        this.model = model;
    },
    
    render: function() {
        var data = this.model.toJSON();
        data.imgSrc = app.uploadDir + data.image;
        app.Helper.loadTemplate( 'product_list_item', data, this.$el );
        return this;
    },

    updateProduct: function( e ) {
     console.log("ok");
    }
});