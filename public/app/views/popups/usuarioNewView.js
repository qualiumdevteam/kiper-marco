var app = app || {};
app.UsuarioNewView = Backbone.View.extend({
    tagName: 'div',

    events: {
        "click #btnFoto": "addFile",
        "click #remove-image": "removeImage",
        "click .cancel": "close",
        "change #foto": "setImage",
        "submit #user-form": "createUser"
    },

    addFile: function( e ) {
        e.preventDefault();
        this.$form.find('#foto').trigger('click');
    },

    addRemoveButton: function() {
        if (this.$('#remove-image').length === 0)
            app.Helper.appendTemplate( 'remove_button', { id: 'remove-image' }, this.$preview );
    },

    close: function(){
        var modal = this.$el.closest('.reveal-modal');

        if (this.user !== undefined)
            this.user = undefined;

        modal.foundation('reveal', 'close');
        $('.reveal-modal-bg').remove();
        setTimeout(function(){
            modal.remove();
        }, 10);
    },

    createUser: function( e ) {
        e.preventDefault();
        if (this.$form.hasClass('updating'))
            return this.updateUser();

        if(e.target[3].value === "" ||
           e.target[4].value === "" ||
           e.target[5].value === "" ||
           e.target[6].value === "" ||
           e.target[7].value === "" ||
           e.target[8].value === "" ||
           e.target[9].value === "" ||
           e.target[10].value === ""){
            alert( "Debe rellenar todos los campos" );
            return false;
        }else{
            if(e.target[7].value !== e.target[8].value){
                alert("Las contraseñas no coinciden");
                return false;
            }else{
                 if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test( e.target[5].value)){
                alert("correo invalido");
                return false;
                }else{
                  app.Usuarios.create( this.userAttributes(), { wait: true } );
                setTimeout(function(){
                        app.usuariosView.renderUsers();
                        app.profileView.render();
                    },50);
                // if ( this.$fileName ) {
                //     //app.Helper.moveFile( this.$fileName );
                // }
                this.close();
                }
        }
    }
    },

    initialize: function( attrs ) {
        if ( attrs.id !== undefined ){
            this.user = app.Usuarios.findWhere({id: attrs.id });
    }
        this.isMoved = 0;
        this.render();
        this.$form = this.$('#user-form');
        this.$preview = this.$('#image-preview');
        this.$fileName = "";

        this.listenTo( app.Usuarios, "add", this.close );
        this.listenTo( app.Usuarios, "sync", this.close );
    },

    newAttributes: function( isUpdate, valueID ) {

           var fileSrc = "";
           var id = valueID;
           var actualProd = app.Usuarios.get(id);
           var actImg = actualProd.get("foto");
           if(this.$form[0].foto.value == ""){
              fileSrc = actImg;
            }else{
            if (actImg !== "") {
              var oldImg = app.Helper.fileExist(actImg);
               console.log(oldImg);
               if (oldImg) app.Helper.deleteFile(actImg);  
            };
              fileSrc = app.Helper.uploadFile(this.$form[0].foto);
           }

        var attrs = {
            id : id,
            nombres: this.$form[0].nombres.value,
            apellidos: this.$form[0].apellidos.value,
            correo: this.$form[0].correo.value,
            username: this.$form[0].username.value,
            perfil: this.$form[0].perfil.value,
            sucursalID: this.$form[0].sucursalID.value,
            foto : fileSrc
        }

       return attrs;
    },
//
 userAttributes: function() {
     if ( this.$form[0].foto.value == ""){
           var fileSrc = "";
           }else{
            var fileSrc = app.Helper.uploadFile(this.$form[0].foto);
           }
        var max = _.max(app.Usuarios.toJSON(),function(max){ return max.id });
        var attrs = {
                id : (max.id > 0) ? (max.id + 1) : 1 ,
                nombres: this.$form[0].nombres.value,
                apellidos: this.$form[0].apellidos.value,
                correo: this.$form[0].correo.value,
                username: this.$form[0].username.value,
                perfil: this.$form[0].perfil.value,
                sucursalID: this.$form[0].sucursalID.value,
                foto : fileSrc
                };
        return attrs;

    },
    removeImage: function(e) {
        if (e) e.preventDefault();
        if ( this.$fileName !== undefined && this.$fileName.length > 0 ) {
            app.Helper.deleteFile(this.$fileName);

            this.$preview.find('a').remove().end().css('background-image','none');
            this.$form.find('#btnFoto').html('<i class=""></i> Subir Imagen');
            this.$fileName = '';

        } else if ( this.user !== undefined ) {
            if ( this.user.get('foto') !== undefined ) {
                app.Helper.deleteFile(this.user.get('foto'));

                this.user.set({ foto: '' });

                this.$preview.find('a').remove().end().css('background-image','none');
                this.$form.find('#btnFoto').html('<i class="icon-image"></i> Subir Imagen');
            }
        }
    },

    render: function() {
        var data = {
            sucursales: app.Sucursales.toJSON(),
            perfiles: app.Profiles.toJSON()
        };

        if ( this.user !== undefined && this.user.get('id') !== undefined ) {
            data.usuario = this.user.toJSON();
           //var elid = data.usuario.perfil;
           //data.usuario.profileName = app.Profiles.findWhere({id : elid }).get('name');
        }
       //data.userActive = app.Usuarios.findWhere({cid :app.userSession});
       //console.log(data.userActive);

        //data.userActive.profileName = app.Profiles.get(data.userActive.perfil).get('name');
        app.Helper.loadTemplate( 'popups/usuario_new', data, this.$el );
        return this;
    },

   setImage: function( e ) {
       e.preventDefault();
        self = this;
        var field = e.target;
        var file =  this.$form[0].foto.value.replace(/\\/g,"\/").replace(" ","%20");//app.Helpers.uploadUserFile(this.$form[0].foto.path, fileName, isTemp ), //this.$form[0].foto.value,//app.Helper.uploadFile(field, true),
            //date = new Date(),
            //console.log(+app.uploadDir+ path +);
        if (file != "") {
          setTimeout(function(){
                self.$preview.css({
                    'background-image': 'url('+file+')',
                    'background-repeat': 'no-repeat',
                    'background-position': 'top center',
                    'background-size': 'contain'
                });
                self.$form.find('#btnFoto').html('<i class=""></i> Cambiar Imagen');
                //self.addRemoveButton();
                //self.$fileName = path;
            },1000);
       };
    },

    updateUser: function() {
        var userID = this.$form[0].usuarioID.value,
            user = app.Usuarios.get( userID );
        if ( this.$form[0].nombres.value === "" ||
            this.$form[0].apellidos.value === "" ||
            this.$form[0].correo.value === "" ||
            this.$form[0].perfil.value === "" ||
            this.$form[0].sucursalID.value === ""
            ){
            alert( "Debe rellenar todos los campos" );
            return false;
        }else{
            if(this.$form[0].password.value !== this.$form[0].confirmar.value){
                alert("Las contraseñas no coinciden");
                return false;
            }else{
                if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(this.$form[0].correo.value)){
                alert("correo invalido");
                }else{
                this.close();
                user.save( this.newAttributes( true , parseInt(this.$form[0].usuarioID.value) ), {wait:true} );
                 setTimeout(function(){
                        app.usuariosView.renderUsers();
                        app.profileView.render();
                    },50);
                 if(user.cid == app.userSession){
                    var img = user.get("foto");
                    var path = app.uploadDir; 
                    app.footerView.refreshImgPerfil(img,path);
                    }
                }
            }
        }
    }
});