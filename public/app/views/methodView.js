var app = app || {};

app.MethodView = Backbone.View.extend({

    tagName: 'tr',

    events: {
        "click .delete": "clear",
        "click .edit": "edit",
        "submit form.update-method": "updateMethod"
    },

    clear: function() {
        this.model.destroy();
        this.collection.sync("remove");
        this.$el.remove();
    },

    edit: function(e) {
        e.preventDefault();
        app.Helper.loadTemplate( 'method_list_edit_item', this.model.toJSON(), this.$el );
    },

    initialize: function( model ) {
        this.model = model;
    },

    render: function() {
        app.Helper.loadTemplate( 'method_list_item', this.model.toJSON(), this.$el );
        return this;
    },

    updateMethod: function( e ) {
        e.preventDefault();
        var form = $(e.target);

        this.model.set( { name: form[0].name.value } );
        this.model.save();
        this.render();
    }
});