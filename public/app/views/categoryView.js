var app = app || {};

app.CategoryView = Backbone.View.extend({

    tagName: 'tr',

    events: {
        "click .delete": "clear",
        "click .edit": "edit",
        "click .view": "details",
        "submit form.update-category": "updateCategory"
    },

    clear: function() {

        var confirm = app.Helper.showConfirm({
            msg: '¿Esta segur@ que desea eliminar este elemento?, se ELIMINARÁN TODOS LOS PRODUCTOS VINCULADOS',
            msgType: 'notice',
            closable: true
        });
        var self = this;
        confirm.done($.proxy(function() {
            var catId = self.model.get('id');
            self.model.destroy();
            self.collection.sync("remove");
            self.$el.remove();
            //si se elimina una categoría se deben eliminar los productos
            $.when(self.delProds(catId)).done(function(){
                console.log("eliminados",app.Products.toJSON());
            });

        },this))
    },

    delProds : function(catId){

        var find = _.findWhere(app.Products.toJSON(),{ "categoryID" : catId });

        _.each(find,function(k,v){
            // console.log(v,k)
            if(v === "id"){
                app.Products.remove(app.Products.get(k));
                app.Products.sync("remove");
            }
        })
    },

    details: function() {
        var div = $('<div></div>');
        app.Helper.loadTemplate( 'category_details_popup', this.model.toJSON(), div );
        app.Helper.showAlert({
            msg: div.html(),
            msgType: 'info',
            closable: false,
            id: 'info-details-' + this.model.get('id')
        });
    },

    edit: function() {
        app.Helper.loadTemplate( 'category_list_edit_item', this.model.toJSON(), this.$el );
    },

    initialize: function( model ) {
        this.model = model;
    },

    render: function() {
        app.Helper.loadTemplate( 'category_list_item', this.model.toJSON(), this.$el );
        return this;
    },

    updateCategory: function( e ) {
        e.preventDefault();
        var form = $(e.target);

        this.model.set( { name: form[0].name.value } );
        this.model.set( { description: form[0].description.value } );

        this.model.save();
        this.render();
    }

});