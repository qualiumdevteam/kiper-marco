var app = app || {};

app.Footer2 = Backbone.View.extend({

    el: '#app-footer2 .cell',

    events: {
        "click .create": "createDynamic",
        "click .my-account": "loadAccountPage"
    },

    changeDate: function() {
        var d = new Date(),
            f = d.toDateString();
        this.dateHandler.html( f );
    },

    createDynamic: function( e ) {
        e.preventDefault();
        var link = $(e.target).closest('a'),
            target = link.data('action');

        if (link.hasClass('disabled'))
            return;

        app[target + 'View'].createItem();
    },

    initialize: function() {
        var $this = this;
        this.render();
        this.clockHandler = this.$('#clock');
        this.dateHandler = this.$('#fecha');
        this.btnHandler = this.$('.btn-handler');

        this.renderButton();

        this.clockTimer = setInterval( function(){ $this.clock(); }, 1000 );
        this.dateInterval = setInterval( function() { $this.changeDate(); }, 60000 );
    },

    loadAccountPage: function(e) {
        e.preventDefault();
        $('.settings-layer').find('.profile').trigger('click');
    },

    render: function() {
        var data = {},
            activeUser = app.Usuarios.get( app.userSession),
            date = new Date(),
            fecha = date.toDateString();
        data.user = activeUser.toJSON();
        data.userImage = /*app.uploadsDir +*/ activeUser.get('foto');
        data.sucursal = app.Sucursales.get( data.user.sucursalID ).get('name');
        data.fecha = fecha;

        app.Helper.loadTemplate('footer2', data, this.$el );
        return this;
    },

    renderButton: function() {
        app.Helper.loadTemplate( 'create_button', {}, this.btnHandler );
    },

    clock: function() {
        var d = new Date(),
            t = d.toLocaleTimeString(),
            minutes = 0,
            seconds = 0,
            hours = '',
            ampm = '';

        if(d.getMinutes() <= 9){
            minutes = '0'+ d.getMinutes();
        }else{
            minutes =  d.getMinutes();
        }

        if(d.getSeconds() <= 9){
            seconds = '0'+ d.getSeconds();
        }else{
            seconds =  d.getSeconds();
        }

        if(d.getHours() <= 9){
            hours = '0'+ d.getHours();
            ampm = 'am';
        }else{
            hours =  d.getHours();
            ampm = 'pm';
        }


        t = hours + ':' + minutes + ':' + seconds + ' '+ ampm;
        this.clockHandler.html(t);
    }

});

