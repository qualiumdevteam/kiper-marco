var app = app || {};

app.DayReportView = Backbone.View.extend({
    el: '#day-report',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();
        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();
    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_timing').val()),
            toDate = new Date(this.$form.find('#toDate_timing').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate());
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1);

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },

    getData: function(from,to,month) {
        var dataChart = [];
        var pedidos = [];
        var arrDays = [];
        var monthRange = month ? this.getMonthRange(month) : this.getDayRange(from,to);
        var pedidosByDate = [];

        arrDays[1] = { day: 1, text: 'Lunes', dates: [] };
        arrDays[2] = { day: 2, text: 'Martes', dates: [] };
        arrDays[3] = { day: 3, text: 'Miercoles', dates: [] };
        arrDays[4] = { day: 4, text: 'Jueves', dates: [] };
        arrDays[5] = { day: 5, text: 'Viernes', dates: [] };
        arrDays[6] = { day: 6, text: 'Sábado', dates: [] };
        arrDays[0] = { day: 0, text: 'Domingo', dates: [] };

        arrDays = this.getMergeDates( arrDays, monthRange );

        pedidos = _.filter( app.reportesView.getPedidos(), function(pedido){
            var valid = false;
            if ( pedido.pagado === 1 && pedido.fecha > monthRange.from.getTime() && pedido.fecha < monthRange.to.getTime() )
                valid = true;
            return true;
        });

        pedidosByDate = _.groupBy(pedidos, function(pedido){ return new Date( pedido.fecha).getDate() });

        _.each(arrDays, function(obj){
            var sum, media = 0, len = obj.dates.length, rlen = 0;

            for (var i = 0; i < len; i++) {
                if (pedidosByDate[obj.dates[i]] === undefined) { sum = 0; continue }

                rlen++;

                _.each( pedidosByDate[obj.dates[i]], function(pedido) {
                    sum += parseFloat( pedido.costo.replace(/\$/, '') )
                } );
            }

            media = (rlen === 0) ? 0 : ( sum / rlen ).toFixed(2);

            dataChart.push({
                day: obj.text,
                value: media
            });
        });

        return dataChart;
    },

    getMergeDates: function( arrDays, monthRange ) {
        var year = monthRange.from.getFullYear();
        var month = monthRange.from.getMonth();

        for(var i = 1, len = monthRange.to.getDate(); i <= len; i++ ) {
            var dateObj = new Date(year, month, i);

            //console.log( dateObj.getDay(), dateObj.toDateString() );

            arrDays[dateObj.getDay()].dates.push(i);
        }

        return arrDays;
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = fromMonth ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    initialize: function() {
        this.render();
    },

    render: function() {

        app.Helper.loadTemplate('reports/day_report', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#form-field');
        this.renderChartPerMonth();
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(from,to) {
        var data = this.getData(from,to,null);
        this.$canvas.html('');
        Morris.Bar({
            element: 'day-canvas',
            data: data,
            xkey: 'day',
            ykeys: ['value'],
            labels: ['Ventas promedio'],
            xLabelAngle: 45,
            gridTextColor: '#fff',
            barColors: ['rgba(81,219,120,0.8)'],
            formatter: function(y, data) { return '$ ' + y }
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');
        Morris.Donut({
            element: 'timing-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)'],
            labelColor: '#fff',
            formatter: function(y, data) { return y + ' min' }
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';
        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));
            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');
            monthArr.push(string);
        }
        return monthArr.join('');
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});