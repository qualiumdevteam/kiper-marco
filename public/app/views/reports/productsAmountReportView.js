var app = app || {};

app.ProductsAmountReportView = Backbone.View.extend({
    el: '#products-amount',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();
        var selectedMonth = this.$form.find('#month').val();
        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();

    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_productsAmount').val()),
            toDate = new Date(this.$form.find('#toDate_productsAmount').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate()).getTime();
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1).getTime();

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        
        return false;
    },

    getData: function(fromDate,toDate,fromMonth) {
        var dataChart = [];
        var today = new Date();
        var $this = this;
        var data = [];
        var each;

        _.each(app.Products.toJSON(), function(producto){
            dataChart.push({
                product : producto.name, //debe ir el nombre del producto
                value : $this.getTotalProduct(producto.id,fromDate,toDate,fromMonth)
            });
        });

        return dataChart;
    },

    getTotalProduct : function(productID,from,to,month){
        var day = new Date(),
            from = from /*? from : new Date(day.getFullYear(),day.getMonth(),1).getTime()*/,
            to = to /*? to : new Date(day.getFullYear(),day.getMonth() + 1,0).getTime()*/,
            firstMonthDay = month ? new Date(day.getFullYear(),parseInt(month),1).getTime() : new Date(day.getFullYear(),day.getMonth(),1).getTime(),
            lastMonthDay = month ? new Date(day.getFullYear(),parseInt(month) +1 ,0).getTime() : new Date(day.getFullYear(),day.getMonth() +1 ,0).getTime();
        var collection, sum = 0, prods = [];

        if(firstMonthDay != undefined && firstMonthDay != '' && from == null && to == null && firstMonthDay != null && lastMonthDay != undefined && lastMonthDay != null && lastMonthDay != ''){
            collection = _.filter( app.Pedidos.toJSON(), function(pedido) {
                return (pedido.fecha >= firstMonthDay && pedido.fecha <= lastMonthDay);
            } );
        }else{
            if(from != undefined && to != undefined && from != '' && from != null && to != null && to != '' && month == null){
                collection = _.filter( app.Pedidos.toJSON(), function(pedido) {
                    return (pedido.fecha >= from && pedido.fecha <= to);
                } );
            }
        }


       // console.log(JSON.stringify(collection))  //devuelve los pedidos realizados en los dias del mes

        _.each(collection,function(pedido){
            //console.log(pedido) // devuelve los pedidos realizados pedido.id
            var idPedido = pedido.id;
            _.each(_.where(app.PedidosProductos.toJSON(),{ pedidoID : pedido.id }),function(prod){
                //console.log(prod) // devuelve cada objeto del pedido
                prods.push({
                    productoID : prod.productoID,
                    cantidad : prod.quantity,
                    precio : prod.price
                })
            });
        });

        _.each(prods,function(producto){
            if(producto.productoID == productID){
                sum = (parseInt(producto.cantidad) * parseInt(producto.precio));
               // console.log(sum)
            }
        });

        return sum;
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = (fromMonth !== undefined) ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    initialize: function() {
        this.render();

        this.listenTo( app.Pedidos, "add", this.renderChart );
        this.listenTo( app.Pedidos, "change", this.renderChart );
        this.listenTo( app.Pedidos, "remove", this.renderChart );
    },

    render: function() {
        app.Helper.loadTemplate('reports/products_amount', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        this.renderChart();
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(fromDate,toDate) {
        var data = this.getData(fromDate,toDate,null);

        this.$canvas.html('');
        Morris.Bar({
            element: 'products-amount-canvas',
            data: data,
            xkey: 'product',
            ykeys: ['value'],
            labels: ['Total'],
            barColors: ['rgba(81,219,120,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');
        this.chart = new Morris.Bar({
            element: 'products-amount-canvas',
            data: data,
            xkey: 'product',
            ykeys: ['value'],
            labels: ['Total'],
            labelColor: '#444',
            barColors: ['rgba(81,219,120,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});