var app = app || {};

app.ScheduleReportView = Backbone.View.extend({
    el: '#schedules-report',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();

    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_schedules').val()),
            toDate = new Date(this.$form.find('#toDate_schedules').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate());
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1);

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },


    getData: function(from,to,month) {
        var dataChart = [];
        var arrHours = [];
        var monthRange = month ? this.getMonthRange(month) : this.getDayRange(from,to);
        var pedidosByMonth, pedidosByDay, pedidosByHour = [], arrCalc = [];

        for (var i = 0; i < 24; i++) { arrHours[i] = {} }

        pedidosByMonth = _.filter( app.reportesView.getPedidos(), function(pedido){
            var valid = false;
            if ( pedido.pagado === 1 && pedido.fecha > monthRange.from.getTime() && pedido.fecha < monthRange.to.getTime() )
                valid = true;

            return valid;
        });

        pedidosByDay = _.groupBy( pedidosByMonth, function(pedido) {
            return new Date(pedido.fecha).getDate()
        } );

        _.each(pedidosByDay, function(val, key){
            pedidosByHour[key] = _.groupBy( val, function(pedido) { return new Date(pedido.fecha).getHours() } )
        });

        _.each(pedidosByHour, function(arr, date){
            _.each(arr, function(pedidos, hour){
                _.each(pedidos, function(pedido){
                    if (arrCalc[hour] === undefined) {
                        arrCalc[hour] = {};
                        arrCalc[hour].sum = parseFloat( pedido.costo.replace(/\$/, '') );
                        arrCalc[hour].len = 1;
                    } else {
                        arrCalc[hour].sum += parseFloat( pedido.costo.replace(/\$/, '') );
                        arrCalc[hour].len ++;
                    }
                });
                arrCalc[hour].media = (arrCalc[hour].sum / arrCalc[hour].len).toFixed(2);
            });
        });

        /*
        pedidosByHour = _.groupBy( pedidosByMonth, function(pedido) {
            var date = new Date(pedido.fecha);
            return date.getHours();
        });
        */


        for (var i = 0; i < 24; i++) {
            if ( arrCalc[i] === undefined )
                dataChart.push({ hora: i, ventas: 0 });
            else
                dataChart.push({ hora: i, ventas: arrCalc[i].media });
        }

        return dataChart;
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = (fromMonth !== undefined) ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    initialize: function() {
        this.render();

        this.listenTo(app.Pedidos, "add", this.renderChart);
        this.listenTo(app.Pedidos, "change", this.renderChart);
        this.listenTo(app.Pedidos, "remove", this.renderChart);
    },

    render: function() {

        app.Helper.loadTemplate('reports/schedules', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        this.renderChart();
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(from,to) {
        var data = this.getData(from,to,null);
        this.$canvas.html('');

        Morris.Bar({
            element: 'schedules-canvas',
            data: data,
            xkey: 'hora',
            ykeys: ['ventas'],
            labels: ['Ventas promedio'],
            xLabelAngle: 90,
            gridTextColor: '#fff',
            barColors: ['rgba(81,219,120,0.8)']
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },


    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');

        Morris.Bar({
            element: 'schedules-canvas',
            data: data,
            xkey: 'hora',
            ykeys: ['ventas'],
            labels: ['Ventas promedio'],
            xLabelAngle: 90,
            gridTextColor: '#fff',
            barColors: ['rgba(81,219,120,0.8)']
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    }
});