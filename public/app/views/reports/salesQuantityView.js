var app = app || {};
app.SalesQuantityView = Backbone.View.extend({
    el: '#num-sales',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_salesQuantity').val()),
            toDate = new Date(this.$form.find('#toDate_salesQuantity').val());

        var newfromDate = new Date((fromDate.getMonth()+1)+'/'+(fromDate.getDate()+1)+'/'+fromDate.getFullYear()).getTime();
        var newToDate = new Date((toDate.getMonth()+1)+'/'+(toDate.getDate()+2)+'/'+toDate.getFullYear()).getTime();

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },

    filterSelected: function(e) {
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();
    },

    getData: function(fromDate,toDate,fromMonth) {
        var today = new Date();
        var data = [];
        var chartData = [];
        var monthRange = fromMonth ? this.getMonthRange(fromMonth) : this.getDayRange(fromDate,toDate);

        data = _.filter( app.reportesView.getPedidos(), function(pedido){
            var valid = false;

            if ( pedido.pagado === 1 && pedido.fecha >= monthRange.from.getTime() && pedido.fecha <= monthRange.to.getTime() )
                valid = true;

            return valid;
        });


        for (var i= 1, len = monthRange.to.getDate(); i <= len; i++) {
            var day = new Date(today.getFullYear(), today.getMonth(), i);

            chartData.push( {
                day: i.toString(),
                value: this.getTotalByDay(day, data)
            } );

        }

        return chartData;
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = fromMonth ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    getTotalByDay: function(day, arr) {
        var from = day,
            to = new Date(day.getFullYear(), day.getMonth(), day.getDate() + 1);
        var collection;

        collection = _.filter( arr, function(pedido) {
            return (pedido.fecha >= from.getTime() && pedido.fecha <= to.getTime());
        } );

        return collection.length;
    },

    initialize: function() {
        this.render();

        this.listenTo( app.Pedidos, "add", this.renderChart );
        this.listenTo( app.Pedidos, "change", this.renderChart );
        this.listenTo( app.Pedidos, "remove", this.renderChart );
    },

    render: function() {
        app.Helper.loadTemplate('reports/sales_quantity', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        this.renderChart();
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(fromDate,toDate) {
        var data = this.getData(fromDate,toDate,null);
        this.$canvas.html('');
        this.chart = new Morris.Bar({
            element: 'num-sales-canvas',
            data: data,
            xkey: 'day',
            ykeys: ['value'],
            labels: ['Cantidad'],
            labelColor: '#444',
            barColors: ['rgba(81,219,120,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');
        this.chart = new Morris.Bar({
            element: 'num-sales-canvas',
            data: data,
            xkey: 'day',
            ykeys: ['value'],
            labels: ['Cantidad'],
            labelColor: '#444',
            barColors: ['rgba(81,219,120,0.8)'],
            xLabelAngle: 90,
            gridTextColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});