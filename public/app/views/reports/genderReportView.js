var app = app || {};

app.GenderReportView = Backbone.View.extend({
    el: '#gender-percent',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();
        
    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_genderPercent').val()),
            toDate = new Date(this.$form.find('#toDate_genderPercent').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate());
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1);

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
        return false;
    },

    getClientData : function(){
        var texto = '', sex = '';

        var arr = _.countBy( app.Clientes.toJSON(), function(cliente) {
            return cliente.sexo
        });


        _.each(arr, function(val, key) {
            texto = ((key === "m") ? "Hombre(s)" : "Mujer(es)");
            sex = val;
        });

        return {
            label : texto,
            value : sex
        }
    },

    getData: function(from,to,month) {
        var dataChart = [];
        var monthRange = month ? this.getMonthRange(month) : this.getDayRange(from,to);
        var data = [];
        var $this = this;
        var sexData = {
            "m" : [],
            "f" : []
        };
        data = _.filter(app.Pedidos.toJSON(),function(pedido){
            var valid = false;

            if ( pedido.pagado === 1 && pedido.fecha >= monthRange.from.getTime() && pedido.fecha <= monthRange.to.getTime() )
                valid = true;

            return valid;
        });

        //console.log(data) // regresa los pedidos

        _.each(data,function(pedidos){
            //dataChart.push($this.getClientData()) // obtiene el conteo de sexos de los clientes
            if(app.Clientes.get(pedidos.clienteID).get("sexo") === "m"){
                //alert("masc")
                sexData.m.push(app.Clientes.get(pedidos.clienteID).get("sexo"));
            }else{
                //alert("fem")
                sexData.f.push(app.Clientes.get(pedidos.clienteID).get("sexo"));
            }


        });

       // console.log(sexData);

        _.each(sexData,function(key,val){
            //console.log(key+' => '+val)
            dataChart.push({
                label : ((val === "m") ? "Hombre(s)" : "Mujer(es)"),
                value : ((val === "m") ? sexData.m.length : sexData.f.length)
            });
        })
       // console.log(dataChart)

        return dataChart;
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        app.Helper.loadTemplate('reports/gender_percent', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        this.renderChartPerMonth();
        this.renderFilters();
        app.appView.colorReport();
        
        return this;
    },

    renderChart: function(from,to) {
        var data = this.getData(from,to,null);
        this.$canvas.html('');
        Morris.Donut({
            element: 'gender-percent-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)','rgba(243,122,222)'],
            formatter: function(y, data) { return y + ' clientes' },
            labelColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');

        Morris.Donut({
            element: 'gender-percent-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)','rgba(243,122,222)'],
            formatter: function(y, data) { return y + ' clientes' },
            labelColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = (fromMonth !== undefined) ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );


        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});