var app = app || {};

app.TimingReportView = Backbone.View.extend({
    el: '#timing-report',

    events: {
        "change #month": "filterSelected",
        "submit form" : "filterResults"
    },

    filterSelected : function(e){
        e.preventDefault();

        var selectedMonth = this.$form.find('#month').val();

        this.renderChartPerMonth(selectedMonth);
        app.appView.colorReport();

    },

    filterResults : function(e){
        e.preventDefault();

        var fromDate =  new Date(this.$form.find('#fromDate_timing').val()),
            toDate = new Date(this.$form.find('#toDate_timing').val());

        var newfromDate = new Date(fromDate.getFullYear(),fromDate.getMonth(),fromDate.getDate());
        var newToDate = new Date(toDate.getFullYear(),toDate.getMonth(),toDate.getDate()+1);

        //alert(newfromDate+' , '+newToDate);

        this.renderChart(newfromDate,newToDate);
        app.appView.colorReport();
         
        return false;
    },

    getData: function(from,to,month) {
        var dataChart = [];
        var monthRange = month ? this.getMonthRange(month) : this.getDayRange(from,to);
        var pedidos = [];
        var sum = prom = 0;

       // alert(JSON.stringify(monthRange))

        pedidos = _.filter( app.reportesView.getPedidos(), function(pedido){
            var valid = false;
            if ( pedido.pagado === 1 && pedido.fecha > monthRange.from.getTime() && pedido.fecha < monthRange.to.getTime() )
                valid = true;
            return valid;
        });

        //alert(JSON.stringify(pedidos))

        _.each(pedidos, function(pedido){
            pedido.deliverTime = app.Helper.getTimeDiff(pedido.fecha, pedido.horaEntrega, 'mins');
            sum += pedido.deliverTime;
        });

        prom = sum / pedidos.length;


        dataChart.push({
            label: 'Promedio',
            value: parseFloat(prom.toFixed(2))
        });


       // alert(JSON.stringify(dataChart))

        return dataChart;
    },

    getMonthRange: function(fromMonth) {
        var now   = new Date(),
            fromMonth = parseInt(fromMonth),
            day   = now.getDate(),
            year  = now.getFullYear(),
            month = (fromMonth !== undefined) ? fromMonth : now.getMonth(),
            from  = new Date( year, month, 1),
            to    = new Date( year, month+1, 0 );

        //   alert(from+', '+to)
        return {
            from: from,
            to: to
        }
    },

    getDayRange: function(fromDate, toDate) {
        var now   = new Date(),
            year  = now.getFullYear(),
            month = now.getMonth(),
            from  = fromDate ? new Date(fromDate) : new Date( year, month, 1 ),
            to    = toDate ? new Date(toDate) : new Date( year, month + 1, 0 );

        return {
            from: from,
            to: to
        }
    },

    initialize: function() {
        this.render();
    },

    render: function() {
        app.Helper.loadTemplate('reports/timing', {}, this.$el);
        this.$canvas = this.$('.canvas');
        this.$form = this.$('#filter-form');
        var today = new Date();
        var newfromDate = new Date(today.getFullYear(),today.getMonth(),1);
        var newToDate = new Date(today.getFullYear(),today.getMonth()+1,0);
        this.renderChart(newfromDate,newToDate);
        this.renderFilters();
        app.appView.colorReport();
        return this;
    },

    renderChart: function(from,to) {
        var data = this.getData(from,to,null);

        // alert(JSON.stringify(data))

        this.$canvas.html('');
        Morris.Donut({
            parseTime : false,
            element: 'timing-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)'],
            formatter: function(y, data) { return y + ' min' },
            labelColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    renderChartPerMonth: function(month) {
        var data = this.getData(null,null,month);
        this.$canvas.html('');
        Morris.Donut({
            parseTime : false,
            element: 'timing-canvas',
            data: data,
            colors: ['rgba(243,170,28,0.8)'],
            formatter: function(y, data) { return y + ' min' },
            labelColor: '#fff'
        });

        if ( ! this.$el.hasClass('show'))
            this.$el.addClass('show');
    },

    getMonths: function() {
        var monthArr = [];
        var month = new Date().getMonth();
        var i, len;
        var template = '<option value="{{value}}" {{selected}}>{{text}}</option>';

        for( i = 0, len = month; i <= month; i++) {
            var string = template
                .replace(/{{value}}/ig, i)
                .replace(/{{text}}/ig, app.Helper.getMonthText( i+1 ));

            if ( i === +len )
                string = string.replace(/{{selected}}/ig, 'selected');
            else
                string = string.replace(/{{selected}}/ig, '');

            monthArr.push(string);
        }

        return monthArr.join('');
    },

    renderFilters: function() {
        var monthOptions = this.getMonths();

        this.$form.find('#month').html(monthOptions);
    }
});