var app = app || {};

app.PrinterView = Backbone.View.extend({

    tagName: 'tr',

    events: {
        "click .delete": "clear",
        "click .edit": "edit",
        "click .view": "details",
        "submit form.update-printer": "updatePrinter"
    },

    clear: function() {

        var confirm = app.Helper.showConfirm({
            msg: '¿Esta segur@ que desea eliminar este elemento?',
            msgType: 'notice',
            closable: true
        });
        var self = this;
        confirm.done($.proxy(function() {
            self.model.destroy();
            self.collection.sync("remove");
            self.$el.remove();
        },this))
    },


    details: function() {
        var div = $('<div></div>');
        app.Helper.loadTemplate( 'printer_details_popup', this.model.toJSON(), div );
        app.Helper.showAlert({
            msg: div.html(),
            msgType: 'info',
            closable: false,
            id: 'info-details-' + this.model.get('id')
        });
    },

    edit: function() {
        app.Helper.loadTemplate( 'printer_list_edit_item', this.model.toJSON(), this.$el );
    },

    initialize: function( model ) {
        this.model = model;
    },

    render: function() {
        app.Helper.loadTemplate( 'printer_list_item', this.model.toJSON(), this.$el );
        app.Helper.loadTemplate( 'printer_selection', { printers : app.Printers.toJSON() }, $('#ticket_size'));
        //console.log(this.model.toJSON())
        return this;
    },

    updatePrinter: function( e ) {
        e.preventDefault();
        var form = $(e.target);

        this.model.set( { pepl: form[0].name.value } );

        this.model.save();
        this.render();
    }

});