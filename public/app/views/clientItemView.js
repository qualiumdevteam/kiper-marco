var app = app || {};
app.ClientItemView = Backbone.View.extend({
    tagName: 'tr',
    events: {
        "click .edit": "edit",
        "click .delete": "clearItem",
        "click .preview": "details"
    },
    
      clearItem: function() {
        var detailnoti = this.model.toJSON();
        var form = $('#clientForm').hasClass("update");
        if (form !== true) {
           var confirm = app.Helper.showConfirm({
            msg: '¿Esta seguro@ que desea eliminar este elemento?',
            msgType: 'notice',
            closable: true
            });
            var $this = this;
            confirm
            .done(function(){
                $this.model.destroy();
                $this.collection.sync("remove");
                $this.$el.remove();
                //noti
                //console.log(detailnoti.id);
                detail = app.DetailNotification.findWhere({"id":detailnoti.id});
                //console.log(detail.toJSON());
                detail.destroy(); 
                app.notificationsView.renderDetailNoti();
            //
            })
            .fail(function(){ return }); 
        }else{
             app.Helper.showAlert( {
                msg: "No puede eliminar elementos mientras edita otro",
                msgType: 'notice'
            });
        };
    },

    details: function(e) {
        e.preventDefault();

        if (app.clientDetailsView === undefined) {
            app.clientDetailsView = new app.ClientDetailsView( this.model );
        } else {
            app.clientDetailsView.model = this.model;
            app.clientDetailsView.render();
            app.clientDetailsView.renderPedidos();
        }

        var active = app.layoutView.getActiveSection();
        var next = app.layoutView.getSection('detalles_cliente');
          $(".section").removeClass('activo');
          $(".section").addClass('inactivo');
          $(".section[data-section='detalles_cliente']").addClass('activo');
            // app[ to.data('section') + 'View' ].init();

        if ( active.length > 0 && next.length > 0 )
            app.Helper.animateScreen(active, next, {direction: 'forward'});

    },

    edit: function(e) {
        //console.log("this");
        e.preventDefault();
        app.clientesView.renderForm( this.model.toJSON() );
    },

    initialize: function( model ) {
        if (model !== undefined)
            this.model = model;
    },

    render: function() {
        app.Helper.loadTemplate( 'client_item', this.model.toJSON(), this.$el );
        return this;
    }
});