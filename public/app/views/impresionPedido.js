var app = app || {};
app.ImpresionPedidoView = Backbone.View.extend({

    el: '#main',

    events: {
        "click .print": "print"
    },

    initialize: function() {
        var win = gui.Window.get();
        //this.model = Ti.API.get('pedidoToPrint');
        this.model = win.pedido;
        this.render();

        console.log( this.model );
    },

    print: function(e) {
        e.preventDefault();

        window.print();
    },

    render: function() {
        var model = this.model;

        app.Helper.loadTemplate( 'pedido_ticket', model, this.$el );

        return this;
    }

});

new app.ImpresionPedidoView();