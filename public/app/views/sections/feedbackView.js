var app = app || {};

app.FeedbackView = Backbone.View.extend({
    el: '.section[data-section="feedback"]',

    events: {
        "submit #feedback-form": "sendMessage",
    },
    
    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$form = this.$('form');
    },

    render: function() {
        app.Helper.loadTemplate('sections/feedback', {}, this.$el);
        return this;
    },

    sendMessage: function(e) {
    //alert("si");
        e.preventDefault();
        var $this = this;
        if (this.$form[0].nombre.value != "" && this.$form[0].tipo.value != "Seleccione una opcion (mejora, sugerencia, error)" && this.$form[0].mensaje.value != "") {
        var data = {
            identificador:"feed",
            nombre: this.$form[0].nombre.value,
            nombre: this.$form[0].nombre.value,
            tipo: this.$form[0].tipo.value,
            mensaje: this.$form[0].mensaje.value
        };
        var request = $.ajax({
                data:  data,
                url:   "http://kiper.mx/fedback/",
                type:  'post',
        });
        if (request) {
            this.$form[0].reset();
            app.Helper.showAlert({
                msg: 'Su mensaje se ha enviado correctamente',
                size: 'small',
                type: 'info',
                closable: true
            });
        } else {
            app.Helper.showAlert({
                msg: 'Ha ocurrido un error, intente nuevamente mas tarde ',
                size: 'small',
                type: 'error',
                closable: true
            });
        }//cierre if reques
    }else{//cierre el form
          app.Helper.showAlert({
                msg:'Debe llenar todos los campos',
                size: 'small',
                type: 'error',
                closable: true
            });
    }
    }
});