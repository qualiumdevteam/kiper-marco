var app = app || {};
var targz = require('tar.gz');

app.ExportView = Backbone.View.extend({

    el: '.section[data-section="export"]',

    events: {
        "submit #exportForm": "exportFiles",
        "submit #importForm" : "importFiles",
        "change .FileBD": "inputFileBD",
        "change .FileIMG ": "inputFileIMG"
    },
  inputFileBD:function(e){
     var aray = $(e.target);
       if (aray == null) {
         }else{
            $(".ResponseFiles").text(aray.context.files[0].name);
       };
  },
  inputFileIMG:function(e){
     var aray = $(e.target);
       if (aray == null) {
         }else{
            $(".ResponseIMG").text(aray.context.files[0].name);
       };
  },

    exportFiles : function(e){
        e.preventDefault();

        var check = this.$formExport.find("input[name='exportar']:checked").val();
        var date = new Date();

        if(check == "all"){

            var db = 'Kiper_data_'+date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear()+'.zip';
            var files = 'Kiper_imgs_'+date.getDate()+'-'+date.getMonth()+'-'+date.getFullYear()+'.zip';


            var imagenes = new targz().compress(app.resourcesDir + "/assets/uploads/", app.resourcesDir + "/" + files, function(err){
                if(err)
                    app.Helper.showAlert({
                        msg : "Ha ocurrido un error por favor inténtelo de nuevo más tarde"
                    })

                var ventana = window.open(app.resourcesDir + "/" + files);
                ventana.close();
            });


            var baseDatos = new targz().compress(app.resourcesDir + "/assets/dbfiles/", app.resourcesDir + "/" + db, function(err){
                if(err)
                    app.Helper.showAlert({
                        msg : "Ha ocurrido un error por favor inténtelo de nuevo más tarde"
                    })

                var ventana = window.open(app.resourcesDir + "/" + db);
                ventana.close();
            });
        }else{
            if(check == "clientes"){
                var csvWriter = require('csv-write-stream');
                var writer = csvWriter({ headers: ["Nombres", "Apellidos","Correo","Edad","Direccion","Telefono"], newline : '\n', separator : ',', sync : true});
                var clientsDate = new Date(date.getDate(),date.getMonth(),date.getFullYear()).getTime();
                var fileName = 'clientes'+clientsDate+'.csv';
                writer.pipe(fs.createWriteStream(fileName));

                var info = [];


                for(var i = 0; i < app.Clientes.toJSON().length; i++){
                   info.push(
                       app.Clientes.toJSON()[i].nombres,
                       app.Clientes.toJSON()[i].apellidos,
                       app.Clientes.toJSON()[i].correo,
                       app.Clientes.toJSON()[i].edad,
                       app.Clientes.toJSON()[i].direccion,
                       app.Clientes.toJSON()[i].telefono
                    );

                    writer.write(info);
                    info = [];
                }


                setTimeout(function(){
                    writer.end();
                    //location.href = app.resourcesDir + "/" + fileName;
                    var ventana = window.open(app.resourcesDir + "/" + fileName);
                    ventana.close();
                },100)

            }
        }



    },

    importFiles : function(e){
        e.preventDefault();
        //validar que no esten vacios para evitar errores
       if ( this.$formImport[0].images.value.length > 0  && this.$formImport[0].dbfiles.value.length > 0){
            //confirmar para que el usuario este enterado de lo que sucedera
           var alert = app.Helper.showConfirm( {
                   msg: 'Se perderán todos los datos actuales en Kipper, deseas continuar?',
                   msgType: 'notice',
                   closable: false,
                   id: 'notice-close-export'
               }),
               $this = this;

           alert.done( function() {
               var imagesSrc = app.Helper.uploadDbBackup( $this.$formImport[0].images,"images");
               var dbfiles = app.Helper.uploadDbBackup( $this.$formImport[0].dbfiles,"db");

               //poner disabled a los inputs file mientras se extraen
               $('#images').attr("disabled","disabled");
               $('#dbfiles').attr("disabled","disabled");
               //una vez copiados extraerlos en su respectiva carpeta

               setTimeout(function(){
                   var imagenes = new targz().extract(app.resourcesDir  + "/assets/backup_images.zip", app.resourcesDir + "/assets/", function(err){
                       if(err){
                           app.Helper.showAlert({
                               msg : "Ha ocurrido un error por favor inténtelo de nuevo más tarde"
                           })
                       }else{
                           /*app.Helper.showAlert({
                               msg : "Imágenes de importadas correctamente"
                           })*/

                           //delete files
                           app.Helper.deleteFile(app.resourcesDir  + "/assets/backup_images.zip");
                       }
                   });


                   var baseDatos = new targz().extract(app.resourcesDir  + "/assets/backup_db.zip", app.resourcesDir + "/assets/" , function(err){
                       if(err){
                           app.Helper.showAlert({
                               msg : "Ha ocurrido un error por favor inténtelo de nuevo más tarde"
                           })
                       }else{
                           /*app.Helper.showAlert({
                               msg : "Base de datos importada corectamente"
                           })*/

                           //delete files
                           app.Helper.deleteFile(app.resourcesDir  + "/assets/backup_db.zip");
                       }
                   });

                   $('#images').removeAttr("disabled");
                   $('#dbfiles').removeAttr("disabled");

                   var salir = app.Helper.showMessage( {
                       msg: 'Kipper se reiniciará'
                   });

                   salir.done(function(){
                       window.location = "index.html";
                   })

               },5000)
           } )
               .fail( function() {
                   $('#images').removeAttr("disabled");
                   $('#dbfiles').removeAttr("disabled");
               } );

        }else{
            app.Helper.showAlert({
                msg : "Por favor seleccione las imágenes y los archivos de respaldo antes de continuar"
            })
        }

        return false;

    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$formExport = this.$('#exportForm');
        this.$formImport = this.$('#importForm');
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/exportar', null, this.$el );
        return this;
    }
});