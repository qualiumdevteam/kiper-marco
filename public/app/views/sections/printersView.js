var app = app || {};
app.PrintersView = Backbone.View.extend({
    el: '.section[data-section="printers"]',

    events: {
        "submit form": "saveSettings"
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();

        this.$form = this.$('form');
    },

    render: function() {
        var printerSettings = app.Settings.findWhere({itemName: 'ticket_size'}) || {};
        if (printerSettings.attributes !== undefined) printerSettings = printerSettings.toJSON();
        app.Helper.loadTemplate( 'sections/printers', printerSettings, this.$el );
        console.log(printerSettings);
        return this;
    },

    saveSettings: function(e) {
        e.preventDefault();
        var printerSettings = app.Settings.findWhere({itemName: 'ticket_size'});

        if ( printerSettings ) {
            printerSettings.save({itemValue: this.$form[0].ticket_size.value}, {wait: true});
        } else {
            printerSettings = app.Settings.create({
                itemName: 'ticket_size',
                itemValue: this.$form[0].ticket_size.value
            }, {wait: true});
        }

        if ( printerSettings.validationError )
            app.Helper.showAlert({
                msg: printerSettings.validationError,
                size: 'small',
                type: 'error'
            });
        else {
            app.Helper.showAlert({
                msg: 'Las configuraciones se han guardado correctamete',
                size: 'small',
                type: 'info'
            });
        }
    }
});