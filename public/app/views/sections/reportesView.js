var app = app || {};

app.ReportesView = Backbone.View.extend({

    el: '.section[data-section="reportes"]',

    events: {
        "click .tabs a": "filterReports",
        "click #arrowup": "nextitem"
    },
    renderReports:function(){
    setTimeout(function(){ 
    app.clientsReportView.render();    
    app.salesQuantityView.render();
    app.ageRangeReportView.render();
    app.clientsAmountReportView.render();
    app.dayReportView.render();
    app.fidelityReportView.render();
    app.genderReportView.render();
    //app.geographicReportView.render();
    app.productsAmountReportView.render();
    app.schedulesReportView.render();
    app.timingReportView.render();
     }, 1000);
    },

    filterReports: function(e) {
        e.preventDefault();

        var link = $(e.target).closest('a'),
            li = link.closest('li'),
            target = link.attr('data-target');

        this.showSection(target);

        li.siblings('li').removeClass('active').end().addClass('active');
    },

    getPedidos: function() {
        return _.filter(app.Pedidos.toJSON(), function(pedido){ return pedido.status === 2 })
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        //console.log(addButton)
       // if(addButton != null && addButton != undefined){
            addButton.find('label').text('Agregar Pedido');
            addButton.data('action','pedidos');
        //}


        if (app.clientsReportView === undefined)
            app.clientsReportView = new app.ClientsReportView();
        //else app.clientsReportView.renderChart();

        if (app.salesQuantityView === undefined)
            app.salesQuantityView = new app.SalesQuantityView();
        //else app.salesQuantityView.renderChart();

        if (app.salesAmountReportView === undefined)
            app.salesAmountReportView = new app.SalesAmountReportView();
        //else app.salesAmountReportView.renderChart();

        if (app.productsAmountReportView === undefined)
            app.productsAmountReportView = new app.ProductsAmountReportView();
        //else app.productsAmountReportView.renderChart();

        if (app.clientsAmountReportView === undefined)
            app.clientsAmountReportView = new app.ClientsAmountReportView();
        //else app.clientsAmountReportView.renderChart();

        if (app.ageRangeReportView === undefined)
            app.ageRangeReportView = new app.AgeRangeReportView();
        //else app.ageRangeReportView.renderChart();

        if (app.genderReportView === undefined)
            app.genderReportView = new app.GenderReportView();
        //else app.genderReportView.renderChart();

        if (app.fidelityReportView === undefined)
            app.fidelityReportView = new app.FidelityReportView();
        //else app.fidelityReportView.renderChart();

        //app.geographicReportView     = new app.GeographicReportView();

        if (app.timingReportView === undefined)
            app.timingReportView = new app.TimingReportView();
        //else app.timingReportView.renderChart();

        if (app.dayReportView === undefined)
            app.dayReportView = new app.DayReportView();
        //else app.dayReportView.renderChart();

        if (app.schedulesReportView === undefined)
            app.schedulesReportView = new app.ScheduleReportView();
        //else app.schedulesReportView.renderChart();
    },

    initialize: function() {
        this.render();

        this.$items = this.$('.item-section');
        this.$sections = this.$('.sections');
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/reportes', null, this.$el );
        return this;
    },

    showSection: function(target) {
        switch( target ) {
            case 'all':
                this.$items.removeClass('hide');
                break;
            default:
                this.$items.addClass('hide');
                this.$sections.find('section[data-section="'+ target +'"]').removeClass('hide');
                break;
        }
    },

    // nextitem:function(){
    // var position = $(".sections").scrollTop();
    // var size = $(".sections").height();
    // var numitems = 465/size;
    // console.log(numitems);
    // console.log(item);
    // //4637
    // // $(".sections").animate({
    // //                 scrollTop: position+450
    // //             }, 1000);
    // }

});
