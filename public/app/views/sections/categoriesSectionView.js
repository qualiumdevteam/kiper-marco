var app = app || {};

app.CategoriesSectionView = Backbone.View.extend({

    el: '.section[data-section="categorias"]',

    events: {
        "submit #categoryForm": "createCategory"
    },

    createCategory: function( e ) {
        e.preventDefault();


        if(this.$form[0].name.value === ""){
            app.Helper.showAlert({ msg: "El campo nombre es obligatorio", msgType: 'error', closable: true, id: 'category-error' });
            return false;
        }{
            app.Categories.create( this.newAttributes(), {wait: true} );
            this.$form[0].reset();
        }
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$form = this.$('#categoryForm');
        this.$categoriesHandler = this.$('#category-list tbody');
        this.renderCategories();
        this.listenTo( app.Categories, 'add', this.renderCategories );
    },

    newAttributes: function() {
        var maxId = _.max(app.Categories.toJSON(),function(num){ return num.id; });
        return {
            id : (maxId.id > 0) ? (maxId.id+1) : 1,
            name: this.$form[0].name.value,
            description: this.$form[0].description.value
        }
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/categorias', null, this.$el );
        return this;
    },

    renderCategories: function() {
        var categories = app.Categories.models,
            $this = this;
        this.$categoriesHandler.html('');
        _.each( categories, function(cat) {
            var catView = new app.CategoryView( cat );
            $this.$categoriesHandler.append( catView.render().el );
        } );
    }
});