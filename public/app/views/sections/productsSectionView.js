var app = app || {};

app.ProductsSectionView = Backbone.View.extend({
    el: '.section[data-section="productos"]',

    events: {
        "submit #productForm": "createProduct",
        "click #productForm .cancel": "cancelEdition",
        "click #product-list .edit": "editProduct",
        "change .inputFile": "input",
    },

    cancelEdition: function( e ) {
        e.preventDefault();

        app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON(), section: true }, this.$form );
        this.$form.removeClass('updating');
    },

    createProduct: function( e ) {
        e.preventDefault();
        var self = this;
        if ( this.$form.hasClass('updating') )
            return this.updateProduct();
        //console.log(e.target[1].value);

        if (e.target[1].value === ""
            || e.target[2].value === ""
            || e.target[4].value === ""){
            app.Helper.showAlert({
                msg: "No se han completado todos los campos necesarios",
                msgType: 'error'
            });
            return false;
           }else if(!/^([0-9])*[.]?[0-9]*$/.test(e.target[4].value)){
            app.Helper.showAlert({
                msg: "EL campo de precio solo puede contener numeros",
                msgType: 'error'
            });
            return false;
         }else{
            app.Products.create( this.productAttributes(), { wait: true } );
            $(".response").text("imagen");
            this.$form[0].reset();
            setTimeout(function(){
                self.renderProducts();
            },2000)
            return true;
        }
    },
        input:function(e){
       var aray = $(e.target)[0].files[0].name;
       if (aray == null) {
         }else{
            $(".response").text(aray);
       };
    },


    deleteProduct: function() {
        if ( this.$form.hasClass('updating') )
            app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON(), section: true }, this.$form );
    },

    editProduct: function( e ) {
        e.preventDefault();

        var link = $(e.target).closest('a'),
            productID = link.data('item'),
            product = app.Products.get( productID),
            data = {
                product: product.toJSON(),
                categories: app.Categories.toJSON(),
                section: true
            };

        this.$form.addClass('updating');
        app.Helper.loadTemplate( 'product_list_edit_item', data, this.$form );
    },

    init: function() {
        this.initialize();
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$form = this.$('#productForm');
        this.$productsHandler = this.$('#product-list tbody');
        app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON(), section: true }, this.$form );
        this.renderProducts();

        this.listenTo( app.Products, 'add', this.renderProducts );
        this.listenTo( app.Products, 'remove', this.renderProducts  );
    },
//
 productAttributes: function() {
        if ( this.$form[0].image.value == ""){
           var fileSrc = "";
           }else{
            var fileSrc = app.Helper.uploadFile( this.$form[0].image);
           }
        var max = _.max(app.Products.toJSON(),function(max){ return max.id });
        var attrs = {
                id : (max.id > 0) ? (max.id + 1) : 1 ,
                categoryID: parseInt(this.$form[0].categoryID.value),
                code: this.$form[0].code.value,
                name: this.$form[0].name.value,
                description: this.$form[0].description.value,
                price: this.$form[0].price.value,
                creationDate: new Date().toDateString(),
                updateDate: new Date().toDateString(),
                image: fileSrc
            };
        return attrs;

    },
//
    newAttributes: function(isUpdate,prodId) {
        var fileSrc = "";
        var id = 1;
        var max = _.max(app.Products.toJSON(),function(max){ return max.id });
         id = parseInt(prodId);
         var actualProd = app.Products.get(id);
         var actImg = actualProd.get("image");
         if(this.$form[0].image.value == ""){
              fileSrc = actImg;
            }else{
                if (actImg !== "") {
               var oldImg = app.Helper.fileExist(actImg);
               console.log(oldImg);
               if (oldImg) app.Helper.deleteFile(actImg);
                };
             fileSrc = app.Helper.uploadFile( this.$form[0].image);
           }
        var attrs = {
            id : parseInt(id),
            categoryID: parseInt(this.$form[0].categoryID.value),
            code: this.$form[0].code.value,
            name: this.$form[0].name.value,
            description: this.$form[0].description.value,
            price: this.$form[0].price.value,
            creationDate: actualProd.toJSON().creationDate,
             updateDate: new Date().toDateString(),
            image: fileSrc
        };
        return attrs;
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/productos', {}, this.$el );
        return this;
    },

    renderProducts: function() {
        var products = app.Products.models,
            $this = this;

        this.$productsHandler.html('');

        _.each( products, function( product ){
            var prodView = new app.ProductView( product );
            $this.$productsHandler.append( prodView.render().el );
        } );
    },
    updateProduct: function() {
       if(!/^([0-9])*[.]?[0-9]*$/.test(this.$form[0][5].value)){
            app.Helper.showAlert({
                msg: "EL campo de precio solo puede contener numeros",
                msgType: 'error'
            });
        }else{
        var productID = this.$form[0].productID.value,
        product = app.Products.get( productID );
        //product.set( this.newAttributes() );
        product.save(this.newAttributes(true,productID));
        var self = this;
        setTimeout(function(){
            app.Helper.loadTemplate( 'product_list_edit_item', { categories: app.Categories.toJSON(), section: true }, self.$form );
            $('#productForm').removeClass("updating");
            self.renderProducts();
        },1000);
        }
    }
});