var app = app || {};
app.MetodosPagoSectionView = Backbone.View.extend({
    el: '.section[data-section="metodosPago"]',

    events: {
        "submit #methodForm": "createMethod"
    },

    createMethod: function(e) {
        e.preventDefault();

        if (e.target[0].value === ""){
            app.Helper.showAlert({ msg: "Debe igresar un nombre", msgType: 'error', closable: true, id: 'method-error' });
            return false;
        }
        else{
            app.MetodosPago.create( this.newAttributes(), { wait: true } );
            this.$form[0].reset();
        }
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$form = this.$('#methodForm');
        this.$methodsHandler = this.$('#method-list tbody');
        this.renderMethods();
        this.listenTo( app.MetodosPago, 'add', this.renderMethods );
    },

    newAttributes: function() {
        var max = _.max(app.MetodosPago.toJSON(),function(max){  return max.id });
        return {
            id : (max.id > 0) ? (max.id + 1) : 1,
            name: this.$form[0].name.value
        }
    },

    render: function() {
        app.Helper.loadTemplate( 'sections/metodos_pago', null, this.$el );
        return this;
    },

    renderMethods: function() {
        var metodos = app.MetodosPago.models,
            $this = this;
        this.$methodsHandler.html('');

        _.each( metodos, function(method) {
            var methodView = new app.MethodView( method );
            $this.$methodsHandler.append( methodView.render().el );
        } );
    }
});