var app = app || {};
app.DetailItemNoti = Backbone.View.extend({
    tagName: 'tr',

    events: {
        "click .changestatus": "changestatus",
        "click .sendEmail" : "sendEmail",
        "click .twitterLink" : "twitterLink",

     },
     init: function() {
    },
     render: function() {
        app.Helper.loadTemplate('items_notifications',this.model.toJSON(),  this.$el );
        return this;
    },
    initialize: function( model ) {
    $containerNoti = $("#app-sections").find("#containerNoti");
    if (model !== undefined)
            this.model = model;
    },
    Noconexion:function(){
    app.Helper.showAlert({ msg: "Compruebe Su Conexión A Intenet", msgType: 'error', closable: true, id: 'category-error' });
     return false;
    },
    sendEmail:function(e){
      result= this.checkInternet();
      if (result == true) {
        var div = $('<div></div>');
        var item = parseInt(e.currentTarget.dataset.item);
        var detail = app.DetailNotification.findWhere({id:item});
        var nombre = app.Settings.findWhere({"id":1}).toJSON();
        detail.set({nombrecocina: nombre.itemValue });
        app.Helper.loadTemplate( 'popups/send_correo', detail.toJSON(), div );
         app.Helper.showPopup({
            id: 'popup-send-correo',
            msg: 'SendCorreoView',
            msgType: 'clear',
            closable: true,
            size: 'xlarge',
            data: {detail}
        });
      }else{
      this.Noconexion();  
      };
    },
    twitterLink:function(e){
      result= this.checkInternet();
      if (result == true) {
      var item = e.currentTarget.dataset.item;
      var win = gui.Window.open ('https://twitter.com/intent/tweet?screen_name='+item+'', {
        toolbar: false,
      });
      win.on ('loaded', function(){
        var document = win.window.document;
      });
      }else{
      this.Noconexion();  
      };
    },
    changestatus:function(e){
      var item = parseInt(e.currentTarget.dataset.item);
      var notification = app.DetailNotification.findWhere({id:item});
      notification.set({leido: "true" });
      notification.save();
      app.notificationsView.renderDetailNoti();
    },
    checkInternet:function(){
      if(navigator.onLine){
      return true;
      } else {
      return false;
      }  
    },
});
//
//
var app = app || {};
app.NotificationPOPUP = Backbone.View.extend({
    el: '#containerNoti',

    events: {
        "click #confiAhora": "confiAhora",
        "click #confiLuego": "confiLuego",
        "click #AceptarPOP":"AceptarPOP",
        "click .arrowRigth":"itemDerecho",
        "click .arrowLeft":"itemIzquierdo",
        "click .ignore" : "ignorarall",
        "click #Aceptarfirsttime": "Aceptarfirsttime",
     },
     init: function() {
    },

    initialize: function() {
    $containerNoti = $("#app-sections").find("#containerNoti");
    },
    ignorarall:function(e){
    var id = parseInt(e.currentTarget.dataset.noti);
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("showNoti2");
    $containerNoti.find(".noti[data-noti="+id+"]").addClass("closenoti");
    $containerNoti.find(".arrowRigth").addClass("closenoti");
    $containerNoti.find(".arrowLeft").addClass("closenoti");
    setTimeout(function(){
    $("#containerNoti").html("");
    },1000);
    },
    itemDerecho:function(e){
    var id = parseInt(e.currentTarget.id);
    var items = $("#containerNoti .noti").length;
    var show;
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("showNoti2");
    if (id === items) {show = 1; }else{show = id + 1;};
    $containerNoti.find(".noti[data-noti="+show+"]").addClass("showNoti2");
    $(".arrowRigth").attr("id",show);
    $(".arrowLeft").attr("id",show);
    },
    itemIzquierdo:function(e){
    var id = parseInt(e.currentTarget.id);
    //console.log(id);
    var items = $("#containerNoti .noti").length;
    var show;
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("showNoti2");
    if (id === 1) {show = items;}else{show = id - 1; };
    $containerNoti.find(".noti[data-noti="+show+"]").addClass("showNoti2");
    $(".arrowRigth").attr("id",show);
    $(".arrowLeft").attr("id",show);
    },
    confiLuego:function(e){
    var id = e.currentTarget.dataset.noti;
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").addClass("closenoti");
    setTimeout(function(){
    $containerNoti.find(".noti[data-noti="+id+"]").remove();
    app.notificationsView.secondmessage();
    }, 1000);
    },
    Aceptarfirsttime:function(e){
     var id = e.currentTarget.dataset.noti;
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").addClass("closenoti");
    setTimeout(function(){
    $containerNoti.find(".noti[data-noti="+id+"]").remove();
    //app.notificationsView.secondmessage();
    }, 1000);
    },
    AceptarPOP:function(e){
    var id = e.currentTarget.dataset.noti;
    setTimeout(function(){
    $("a.notifications.pedidos.textazul").click();
    $("#notification-list tbody #"+id+"").addClass('selectitem');
    $("#containerNoti").html("");
    var top = $("#notification-list tbody #"+id+"").position().top;
    //console.log($("#notification-list tbody tr")[0].id+""+id);
    if (id != $("#notification-list tbody tr")[0].id) {
      $("#notification-list_parent").animate({scrollTop:top}, '500'); 
    };
    }, 1000);
     setTimeout(function(){
    $("#notification-list tbody #"+id+"").removeClass('selectitem'); 
    }, 8000);
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").addClass("closenoti");
    $containerNoti.find(".arrowRigth").addClass("closenoti");
    $containerNoti.find(".arrowLeft").addClass("closenoti");
    var notification = app.DetailNotification.findWhere({id:parseInt(id)});
            notification.unset("idPop");
            notification.set({leido: "true" });
            notification.save();
    app.notificationsView.renderDetailNoti();
    },
    confiAhora:function(e){
    $("a.notifications.pedidos.textazul").click();
    app.notificationsView.editarNoti();
    var id = e.currentTarget.dataset.noti;
    $containerNoti.find(".noti[data-noti="+id+"]").removeClass("shownoti");
    $containerNoti.find(".noti[data-noti="+id+"]").addClass("closenoti");
    setTimeout(function(){
    $containerNoti.find(".noti[data-noti="+id+"]").remove();
    var notification = app.Notifications.findWhere({id:1});
            notification.unset("idPop");
            notification.set({firstMessage: "true" });
            notification.save();
    }, 1000);
    },
});
//
var app = app || {};
app.NotificationSectionView = Backbone.View.extend({
    el: '.section[data-section="notifications"]',
    events: {
        "click #editarNoti": "editarNoti",
        "click #cancelarNoti": "renderSettingNoti",
        "submit #notificationForm": "updateNoti",
        "click #exampleCheckboxSwitch": "activeoverlay",
    },

    init: function() {
        var addButton = app.footerView.btnHandler.find('a');
        addButton.find('label').text('Agregar Pedido');
        addButton.data('action','pedidos');
    },

    initialize: function() {
        this.render();
        this.$items = $("#notification-list");
        this.$settings = $('#notificationForm');
        $containerNoti = $("#app-sections").find("#containerNoti");
        this.firstmessage();
    },

    editarNoti:function(){
      $('#notificationForm').html("");
      $(".switchNoti").css({"display":"none"});
      app.Helper.appendTemplate('formNotification', app.Notifications.toJSON()[0],$('#notificationForm'));
       return false;
    },

    activeoverlay:function(e){
       var notification = app.Notifications.findWhere({id:1});
       //console.log(notification.toJSON().activo);
       if (e.currentTarget.checked == false) {
            $("#overlay").addClass("activeoverlay");
                notification.set({activo: "false" });
                notification.save();
                $("#notification-list_parent").removeClass("showNoti2");
                $("#notification-list_parent").addClass("closenoti");
                setTimeout(function(){
                $("#notification-list_parent").removeClass("closenoti");
                $("#notification-list_parent").css({"display":"none"});
                },1000);
           }else{
            $("#overlay").removeClass("activeoverlay");
             notification.set({activo: "true" });
             notification.save();
             $("#notification-list_parent").removeClass("closenoti");
             $("#notification-list_parent").addClass("showNoti2");
                setTimeout(function(){
                  $("#notification-list_parent").removeClass("showNoti2");
                $("#notification-list_parent").css({"display":"block"});
                },1000);
           };
    },

    updateNoti:function(e){
    var time = e.target.time.value;
    var notification = app.Notifications.findWhere({id:1});
    notification.set({time: time });
    notification.set({firstConfig: "true" });
    notification.set({activo: "true" });
    notification.save();
    this.renderSettingNoti();
    $(".switchNoti").css({"display":"block"});
    $("#exampleCheckboxSwitch").attr( 'checked', "true" );
    return false;
    },
    createPOPUP:function(){
    if (app.Notifications.toJSON()[0].activo == "true") {
        $this = this;
        setTimeout(function(){
         $containerNoti.html(' ');
         var idPop = $("#app-sections").find("#containerNoti div").length;
           _.each(app.DetailNotification.models, function(detail) {
                dia = detail.toJSON().dias;
                result = $this.cumplecondicion(dia);
                console.log(result);
                if (result == true) {
                  if (detail.toJSON().leido == "false") {
                  detail.set({activo: "true"});
                  detail.save();
                  idPop++;
                  detail.set({idPop: idPop});
                    app.Helper.appendTemplate('PopDetailNotification',detail.toJSON(), $containerNoti);
                    $containerNoti.find(".noti[data-noti='1']").addClass("shownoti");
                  };
                };
            } );
        $this.renderDetailNoti();
        //console.log(idPop);
        if (idPop > 0) {
          if (idPop != 1) {
          $("#app-sections").find("#containerNoti").append("<img id='1' class='arrowRigth' src='app/images/arrow.png'></img>");
          $("#app-sections").find("#containerNoti").append("<img id='1' class='arrowLeft' src='app/images/arrow.png'></img>");
          $("#app-sections").find("#containerNoti img").addClass("showarrow");//
          };
        };
        }, 3000);
    };
    },

    cumplecondicion:function(dia){
    var diasetting;
    if (app.Notifications.toJSON()[0].time == "7 Dias") { diasetting = 7;
    }else if (app.Notifications.toJSON()[0].time == "15 Dias"){ diasetting = 15;
    }else if (app.Notifications.toJSON()[0].time == "30 Dias") {diasetting = 30;
    }else if (app.Notifications.toJSON()[0].time == "60 Dias") {diasetting = 60;
    }else if (app.Notifications.toJSON()[0].time == "90 Dias") {diasetting = 90;}
    //
    if (dia ==0) {
    return false;
    }else{
       if (dia % diasetting == 0) {
        return true;
      }else{
        return false;
      };
    };
    //
    },
    fechactual:function(){
    var d = new Date();
    var mes = d.getMonth() +1;
    var fecha = d.getDate() + "/" + mes + "/" + d.getFullYear();
    return fecha;
    },
    calculateDias:function(fechaA,fechaU){
     var aFecha1 = fechaU.split('/'); 
     var aFecha2 = fechaA.split('/'); 
     var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
     var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
     var dif = fFecha2 - fFecha1;
     var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
     //console.log(dias);
     return dias;
    },
    createDetailNoti:function(){
     n = this.fechactual();
     var fechaA = n;
     var DetailNoti = app.DetailNotification.toJSON();
     $this = this;
      //
      if (DetailNoti.length > 0) {
        //console.log(n);
        for (var i = DetailNoti.length - 1; i >= 0; i--) {
            //console.log(DetailNoti[0].fecha);
            if (n != DetailNoti[0].fecha) {
            var fechaU = DetailNoti[0].fecha;
            dia = $this.calculateDias(fechaA,fechaU);
            detail = app.DetailNotification.findWhere({"id":DetailNoti[0].id});
            detail.set({dias: dia});
            detail.save();
            };
        };
       var DetailNoti = app.DetailNotification.toJSON();
         _.each(app.DetailNotification.models, function(detail) {
            detail = app.DetailNotification.findWhere({"id":detail.id});
            detail.set({fecha: n});
            detail.save();   
          });
        this.createPOPUP();
        this.renderDetailNoti();
      };
    },
    createNoti:function(clienteID){
      console.log(clienteID);
      var cliente = app.Clientes.findWhere({"id": clienteID}).toJSON();
      var DetailNoti = app.DetailNotification.findWhere({"id_cliente": cliente.id});
      n = this.fechactual();
      if (DetailNoti == undefined) {
            //crear notificacion
            //obtener id
            var Details = app.DetailNotification.toJSON();
            if(Details.length !== 0){
                var result = _.max(Details, function(Detail){
                 return Detail.id; 
                 });
              var iddetailNoti = result;
            }else{
             var iddetailNoti = 1;
             };
             //obtener id
           app.DetailNotification.create({
                id: iddetailNoti,
                id_cliente: cliente.id,
                telefono: cliente.telefono,
                leido: "false",
                correo: cliente.correo,
                twitter: cliente.twitter,
                nombre: cliente.nombres+' '+cliente.apellidos,
                dias: 0,
                fecha: n,
                activo:"false",
          });
          //
      }else{
        console.log(DetailNoti);
        DetailNoti.set({dias:0 });
        DetailNoti.set({activo:"false" });
        DetailNoti.set({leido:"false" });
        DetailNoti.save();
        this.renderDetailNoti();
      };    
    },
    firstmessage:function(){
    var noti = app.Notifications.toJSON();
        if (noti[0].firstMessage == "false") {
            setTimeout(function(){
                var idNoti = 1;
                var data = {
                    texto:"aqui se mostraran las notificaciones de los clientes",
                    subtexto:"¿Deseas configurarlo ahora?",
                    id:idNoti,
                    opc:"twoOpc",
                };
                app.Helper.appendTemplate( 'popupsNotification', data, $containerNoti);
                $containerNoti.find(".noti[data-noti="+idNoti+"]").addClass("shownoti");
            }, 3000);
         var notification = app.Notifications.findWhere({id:1});
         notification.set({firstMessage: "true" });
         notification.save();
        }else{
            if (noti[0].firstConfig == "true") {
            console.log("ya se configuro por primera vez");
                if(noti[0].activo == "true") {
                  console.log("crear notificaciones");
                  this.createDetailNoti();
                };
            };
        };
    },
    secondmessage:function(){
            setTimeout(function(){
                var idNoti = 2;
                var data = {
                    texto:"Para activar y configurar tus notificaciones dale click en la seccion de notificaciones",
                    id:idNoti,
                    opc:"oneOpc",
                };
                app.Helper.appendTemplate( 'popupsNotification', data, $containerNoti);
                $containerNoti.find(".noti[data-noti="+idNoti+"]").addClass("shownoti");
            }, 1000);
    },
    renderSettingNoti:function(){
        $('#notificationForm').html("");
        if (app.Notifications.toJSON()[0].firstConfig !== "") {
          $(".switchNoti").css({"display":"block"});
          $("#exampleCheckboxSwitch").attr( 'checked', "true" );
        };
        app.Helper.loadTemplate( 'settings_noti', app.Notifications.toJSON()[0],$('#notificationForm'));
    },
    render: function() {
        app.Helper.loadTemplate( 'sections/notifications', null, this.$el );
        this.renderSettingNoti();
        if (app.Notifications.toJSON()[0].firstConfig != "") {
            //this.renderDetailNoti();
            if (app.Notifications.toJSON()[0].activo == "true") {
            $("#exampleCheckboxSwitch").attr( 'checked', "true" );
            }else{
            $("#notification-list_parent").css({"display":"none"});
            $("#exampleCheckboxSwitch").prop('checked', false);
            $("#overlay").addClass("activeoverlay");    
            };
        }else{
            $(".switchNoti").css({"display":"none"});
        };
        return this;
    },

    renderDetailNoti:function(){
    $("#notification-list").html("");
     _.each(app.DetailNotification.models, function(detail) {
        if (detail.toJSON().activo == "true") {
                $("#notification-list").append( new app.DetailItemNoti(detail).render().el );
       };  
    });
    },
});