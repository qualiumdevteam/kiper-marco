var app = app || {};
app.ClientDetailsView = Backbone.View.extend({
    el: '.section[data-section="detalles_cliente"]',
    events: {
        "click .back": "goBack"
    },

    goBack: function(e) {
        e.preventDefault();

        var active = app.layoutView.getActiveSection();
        var next = app.layoutView.getSection('clientes');
        $(".section").removeClass('activo');
        $(".section").addClass('inactivo');
        $(".section[data-section='clientes']").addClass('activo');

        if ( active.length > 0 && next.length > 0 )
            app.Helper.animateScreen(active, next, {direction: 'reverse'});
    },

    initialize: function( model ) {
        if (model)
            this.model = model;

        this.render();
        this.renderPedidos();

        this.listenTo( app.Pedidos, "change", this.renderPedidos );
    },

    render: function() {
        var model = this.model.toJSON();
        if (model.fechaNacimiento.length > 0){
            model.edad = app.Helper.getYearsDiff( model.fechaNacimiento );
        } else
            model.edad = undefined;
        app.Helper.loadTemplate( 'client_details_section', model, this.$el );

        return this;
    },

    renderPedidos: function() {
        var pedidos = app.Pedidos.where({ clienteID: this.model.get('id'), status: 2 });
        var fragment = document.createDocumentFragment();
        var handler = this.$('#pedidos-list tbody');

        handler.html('');

        _.each( pedidos, function( pedido ){
            fragment.appendChild( new app.ClientePedidoView( pedido ).render().el );
        } );

        handler.append( fragment );
    }
});