var app = app || {};

app.LayoutView = Backbone.View.extend({
    el: '#app-content',

    events: {
        "click #software-nav a": "sectionSelected",
        "click .Dash": "goToDashboard",
        "click header .settings": "showSettings",
        "click .settings-layer .close-settings": "hideSettings",
        "click .menu-list a": "sectionSelected",
        "mouseover .hover-action": "showSettings",
        //".MENU li" :"clickMenu",
    },
    

    getActiveSection: function() {
        var sections = this.$sectionWrapper.find('.section'),
            active = {};
        _.each( sections, function( section ){
            var obj = $(section);
            if ( obj.css('display') !== 'none' )
                active = obj;
        } );

        return active;
    },

    getNavPosition: function( tag ) {
        var pos = 1,
            sections = this.$('#section-wrapper .section');

        if ( tag === 'dashboard' )
            return 0;

        for (var i = 1, len = sections.length + 1; i < len; i++) {
            pos = i;
            if ( $(sections[i-1]).data('section') === tag )
                break;
        }

        return pos;
    },

    getSection: function( tag ) {
        return this.$sectionWrapper.find('.section[data-section="'+ tag +'"]');
    },

    goToDashboard: function( e ) {
        // e.preventDefault();
        // var from = this.getActiveSection(),
        //     to = this.$sectionWrapper.find('.section[data-section="dashboard"]');

        // if ( from.data('section') === to.data('section') )
        //     return;

        // this.setTitle( 'escritorio' );
        // this.$('#software-nav a').removeClass('active');

        // var anim = app.Helper.animateScreen( from, to, { direction: 'reverse' } );
        // anim.done( function() {
        //     from.hide();
        //     app.dashboardView.init();
        // } );
    },

    hideSettings: function( e ) {
        if (e) e.preventDefault();
        this.$settingsLayer.removeClass('animate');
        this.$settingsLayer.addClass('hidden');
    },

    initialize: function() {

        if (app.Categories.length <= 0 || app.Products.length <= 0) {
            app.appView.loadCategoriesWizard();
            return;
        }
        this.render();
        app.footerView = new app.FooterView();
        this.$title = this.$('#section-title');
        this.$sectionWrapper = this.$('#section-wrapper');
        this.$settingsLayer = this.$('.settings-layer');
        this.$softwareNav = this.$('#software-nav');
        this.$mainLogo = this.$('#main-logo');
        this.loadSections();
        this.statusWacher();

        $(document).foundation();
    },

    loadSections: function() {
        app.footer2 = new app.Footer2();
        app.dashboardView = new app.DashboardView();
        app.usuariosView = new app.UsuariosView();
        app.pedidosView = new app.PedidosView();
        app.clientesView = new app.ClientesView();
        app.reportesView = new app.ReportesView();
        app.feedbackView = new app.FeedbackView();
        app.categoriasView = new app.CategoriesSectionView();
        app.productosView = new app.ProductsSectionView();
        app.metodosPagoView = new app.MetodosPagoSectionView();
        app.profileView = new app.ProfileView();
        app.profileEmpresaView = new app.ProfileEmpresaView();
        app.printersView = new app.PrintersSectionView();
        app.exportView = new app.ExportView();
        app.notificationsView = new app.NotificationSectionView();
        app.notificationPOPUP = new app.NotificationPOPUP();
        //app.detailItemNoti = new app.DetailItemNoti();
    },

    refreshLogo: function() {
        var logotipo = app.Settings.findWhere({itemName: 'logotipo'}).get('itemValue');
        //path = app.uploadDir+logotipo;
        this.$mainLogo.css('background-image','url('+app.uploadDir+logotipo+')');
    },

    render: function() {
        var logotipo = app.Settings.findWhere( { itemName: 'logotipo' }),
            data = {};
        if(logotipo != null || logotipo != undefined){
            data.imageSrc = logotipo.attributes.itemValue;
        }
        data.usuario = app.Usuarios.get(app.userSession).toJSON();
        data.usuario.perfilName = app.Profiles.get( data.usuario.perfil ).get('name');

        app.Helper.loadTemplate( 'layout', data, this.$el );
        return this.el;
    },

    sectionSelected: function( e ) {
        e.preventDefault();
        var parent = $(e.currentTarget).parent();
        
        var parent2 = $(parent).parent();
        var parent3 = $(parent2).parent().attr("class");
        if (parent3 == "MENULEFT") {
             $(".MENU li").removeClass("SelectedMenu2");
             $(".MENU li").removeClass("SelectedMenu");
             $(parent).addClass("SelectedMenu");
        }else if(parent3 == "has-dropdown hidemenu hover"){
            $(".MENU li").removeClass("SelectedMenu2");
             $(".MENU li").removeClass("SelectedMenu");
             $(parent).addClass("SelectedMenu");
        }
        else{
            $(".MENU li").removeClass("SelectedMenu2");
             $(".MENU li").removeClass("SelectedMenu");
             $(parent).addClass("SelectedMenu2");
        };
        //
        var link = $(e.target).closest('a'),
            from = this.getActiveSection(),
            to = this.$sectionWrapper.find('.section[data-section="'+ link.attr('href') +'"]'),
            direction = 'forward',
            $this = this;

        if ( link.attr('href') === 'exit' ) {
            var confirm = app.Helper.showConfirm({
                msg: "Esta seguro que desea cerrar su sesión?",
                msgType: "notice",
                closable: true
            });
            confirm.done( function() { app.appView.logout() } );
            return;
        }

        if ( from.data('section') === to.data('section') )
            return;

        this.setTitle(link.data('title'));
        link.closest('ul').find('a').removeClass('active').end().end().addClass('active');

        var fromPos = this.getNavPosition( from.data('section')),
            toPos = this.getNavPosition( to.data('section') );
        if ( toPos < fromPos )
            direction = 'reverse';

        var anim = app.Helper.animateScreen( from, to, { direction: direction } );
        anim.done( function() {
            from.hide();
            $(".section").removeClass('activo');
            $(".section").addClass('inactivo');
            var se = to.data('section');
           $(".section[data-section='" + se+"']").addClass('activo');
            app[ to.data('section') + 'View' ].init();

            var mainNavLink = $this.$softwareNav.find('a[href="'+to.data('section')+'"]');
            $this.$softwareNav.find('a').removeClass('active');
            if (mainNavLink.length > 0)
                mainNavLink.addClass('active');
            if ($this.$settingsLayer.hasClass('animate'))
                $this.hideSettings();

        } );
    },

    setTitle: function( title ) {
        this.$title.html( title );
    },

    showSettings: function( e ) {
        if (e) e.preventDefault();
        console.log(this.$settingsLayer);
        this.$settingsLayer.removeClass('hidden');
        this.$settingsLayer.addClass('animate');
    },

    statusWacher: function() {
        this.watchStatus();
        this.watcherInterval = setInterval(this.watchStatus, (1000 * 60 * 5));
    },

    watchStatus: function() {
        var pedidos = app.Pedidos.where({status: 1});
        var colorsTime = app.ColorsTime.toJSON();
        var now = new Date();

        _.each(pedidos, function(pedido) {
            var diff =  app.Helper.timeDiff(new Date( pedido.get('fecha') ), now);
            var color = 'green';

            _.each( colorsTime, function(colorTime) {
                if ( diff > colorTime.from ) {
                    if ( colorTime.to === 0 )
                        color = colorTime.color;
                    else {
                        if ( diff < colorTime.to )
                            color = colorTime.color;
                    }
                }
            } );

            if (pedido.get('status') === 2)
                pedido.save({colorStatus: 'white'});
            else
                pedido.save({colorStatus: color});
        });
    }
});