var app = app || {};

app.CategoriesView = Backbone.View.extend({
    el: '#categorias',
    
    templateForm: _.template($('#category-form-template').html()),
    templateDetails: _.template($('#category-details-template').html()),

    events: {
        "click #btn-add-category": "showForm",
        "click #btn-save-category": "createCategory",
        "click #btn-update-category": "updateCategory",
        "click #close-form-cateogry": "restoreList",
        "click .edit": "editCategory",
        "click .details": "showCategory"
    },
    
    initialize: function() {
        this.$handler = this.$('#category-collection ul');
        app.Categories.fetch();
        this.render();
    },
    
    render: function() {
        var $this = this;
        _.each( app.Categories.models, function(model) {
            var category = new app.CategoryView();
            category.model = model;
            $this.$handler.append(category.render().el);
        } );
    },
    
    /* -------- */
    
    newAttributes: function() {
    	this.$form = this.$('#category-form');
        var max = _.max(app.Categories.toJSON(),function(max){ return max.id });
	    return {
            id : ( max.id + 1 ),
		    name: this.$form[0].nombre.value,
            description: this.$form[0].descripcion.value
	    }
    },
    
    createCategory: function(e) {
    	e.preventDefault();

	    app.Categories.create(this.newAttributes());
        this.$('#category-collection').html('<ul></ul>');
        this.initialize();
    },

    editCategory: function(e) {
        e.preventDefault();
        var el = $(e.target),
            categoryID = el.closest('.row').data('id'),
            category = app.Categories.get(categoryID),
            data = {
                titulo: 'Editar Categoría',
                cateoria: category.toJSON()
            };
        this.$('#category-collection').html( this.templateForm(data) );
    },

    restoreList: function(e) {
        e.preventDefault();
        this.$('#category-collection').html('<ul></ul>');
        this.initialize();
    },
    
    showForm: function( e ) {
        e.preventDefault();
        var category = new app.Category(),
            data = {
                titulo: 'Nueva Categoría',
                categoria: category.toJSON()
            };
        this.$('#category-collection').html( this.templateForm(data) );
    },

    showCategory: function(e) {
        e.preventDefault();
        var el = $(e.target),
            categoryID = el.closest('.row').data('id'),
            categoria = app.Categories.get(categoryID).toJSON();
        this.$('#category-collection').html( this.templateDetails(categoria) );
    },

    updateCategory: function(e) {
        e.preventDefault();
        var btn = $(e.target),
            categoryID = btn.closest('form').find('[name=id]').val(),
            category = app.Categories.get(categoryID),
            newAttributes = this.newAttributes();

        category.save(newAttributes);
        this.$('#category-collection').html('<ul></ul>');
        this.initialize();

    }
});