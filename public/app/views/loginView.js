var app = app || {}

app.LoginView = Backbone.View.extend({
    el: '#login-template',

    events: {
        'submit': 'loginValidate'
    },

    initialize: function() {
        this.$username = this.$('input[name="username"]');
        this.$password = this.$('input[name="clave"]');
    },

    loginValidate: function( e )  {
        e.preventDefault();
        var user = app.Usuarios.findWhere({
            username: this.$username.val(),
            password: this.$password.val()
        });

       // alert(user.cid)

        if ( ! user ) {
            app.Helper.showAlert( {
                msg: 'Usuario y contraseña inválido',
                msgType: 'notice',
                closable: false
            } );
        } else {
            //sessionStorage.setItem('togo-session-id', user.cid);
            //console.log()
            app.userSession = user.cid;//sessionStorage.getItem('togo-session-id');

            this.remove();
            app.appView.loadScreens();
            app.appView.setupAutoheight();
        }
    },

    render: function() {}

});