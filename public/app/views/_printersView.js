var app = app || {};

app.PrintersView = Backbone.View.extend({
    el: '#printers',

    templateForm: _.template($('#printers-form-template').html()),
    templateDetails: _.template($('#printers-details-template').html()),

    events: {
        "click #btn-add-printer": "showForm",
        "click #btn-save-printer": "createPrinter",
        "click #btn-update-printer": "updatePrinter",
        "click #close-form-printer": "restoreList",
        "click .edit": "editPrinter",
        "click .details": "showPrinter"
    },

    initialize: function() {
        this.$handler = this.$('#printer-collection ul');
        app.Printers.fetch();
        this.render();
    },

    render: function() {
        var $this = this;
        _.each( app.Printers.models, function(model) {
            var printer = new app.PrinterView();
            printer.model = model;
            $this.$handler.append(printer.render().el);
        } );
    },

    /* -------- */

    newAttributes: function() {
        this.$form = this.$('#printer-form');
        var max = _.max(app.Printers.toJSON(),function(max){ return max.id });
        return {
            id : ( max.id + 1 ),
            name: this.$form[0].papel.value,
        }
    },

    createPrinter: function(e) {
        e.preventDefault();

        app.Printers.create(this.newAttributes());
        this.$('#printer-collection').html('<ul></ul>');
        this.initialize();
    },

    editPrinter: function(e) {
        e.preventDefault();
        var el = $(e.target),
            printerID = el.closest('.row').data('id'),
            printer = app.Printers.get(printerID),
            data = {
                titulo: 'Editar Ticket',
                printer: printer.toJSON()
            };
        this.$('#printercollection').html( this.templateForm(data) );
    },

    restoreList: function(e) {
        e.preventDefault();
        this.$('#printer-collection').html('<ul></ul>');
        this.initialize();
    },

    showForm: function( e ) {
        e.preventDefault();
        var printer = new app.Printer(),
            data = {
                titulo: 'Nuevo Ticket',
                printer: printer.toJSON()
            };
        this.$('#category-collection').html( this.templateForm(data) );
    },

    showCategory: function(e) {
        e.preventDefault();
        var el = $(e.target),
            printerID = el.closest('.row').data('id'),
            printer = app.Printers.get(printerID).toJSON();
        this.$('#printer-collection').html( this.templateDetails(printer) );
    },

    updatePrinter : function(e) {
        e.preventDefault();
        var btn = $(e.target),
            printerID = btn.closest('form').find('[name=id]').val(),
            printer = app.Printers.get(printerID),
            newAttributes = this.newAttributes();

        printer.save(newAttributes);
        this.$('#printer-collection').html('<ul></ul>');
        this.initialize();

    }
});