var app = app || {};

app.SettingsView = Backbone.View.extend({
    el: '#settings',
    events: {
        "submit #settings-form": "submitForm",
        "click .alert-button": "closeWindow",
        "click .cancel": "closeWindow",
        "click .import": "importData",
        "change #foto": "renderFoto",
        "change #logotipo": "renderlogotipo",
         "change #archivos": "renderarchivos",
        "change #imagenes": "renderimagenes"


    },
    initialize: function() {
        this.form = $('#settings-form');
    },
    render: function() {
        return this;
    },
    renderFoto: function(e) {
     var Imgadmin = $(e.target)[0].value.replace(/\\/g, '\\\\');
           if(Imgadmin != ""){
     $(".fotoPerfil").css('background','url('+Imgadmin+')');
            }
    },
    renderlogotipo: function(e) {
    var logo = $(e.target)[0].files[0].name;
     if (logo == null) {
         }else{
            $(".logotipo").text(logo);
       };
},
  renderarchivos: function(e) {
    var arc = $(e.target)[0].files[0].name;
     if (arc == null) {
         }else{
            $(".file").text(arc);
       };
},
 renderimagenes: function(e) {
    var img = $(e.target)[0].files[0].name;
     if (img == null) {
         }else{
            $(".img").text(img);
       };
},
    /* =adminAttributes
    ------------------------- */
    adminAttributes: function(fotoUser) {
        var max = _.max(app.Usuarios.toJSON(),function(max){ return max.id });
        var usuarios = app.Usuarios.toJSON().length;

        if(fotoUser){
            var usuarioPic = fotoUser;
        }else{
            var usuarioPic = "";
        }
        return {
            id : ( usuarios > 0) ? (max.id +1) :  1,
            nombres: this.form[0].nombres.value,
            apellidos: this.form[0].apellidos.value,
            correo: this.form[0].correo.value,
            username: this.form[0].username.value,
            password: this.form[0].password.value,
            confirmar: this.form[0].confirmar.value,
            foto: usuarioPic,
            perfil: app.Profiles.findWhere({name:'admin'}).get('id'),
            sucursalID: this.form[0].sucursalID.value
        }
    },

    /* =CloseWindow
     ------------------------ */
    closeWindow: function( e ) {
        e.preventDefault();
        if ( confirm( '¿Esta seguro que desea salir del sistema?' ) ){
            win.on('close', function() {
                this.hide(); // Pretend to be closed already
                this.close(true);
            });

        win.close();
        }
    },

    importData : function(e){
        e.preventDefault();
        if ( this.form[0].imagenes.value.length > 0  && this.form[0].archivos.value.length > 0)
        {
            //confirmar para que el usuario este enterado de lo que sucedera
            var alert = app.Helper.showConfirm( {
                    msg: 'Después de importar la información Kiper se reiniciará y podrá acceder con su usuario y contraseña anterior, desea continuar?',
                    msgType: 'notice',
                    closable: false,
                    id: 'notice-close-export'
                }),
                $this = this;

            alert.done( function() {
                var imagesSrc = app.Helper.uploadDbBackup( $this.form[0].imagenes,"images");
                var dbfiles = app.Helper.uploadDbBackup( $this.form[0].archivos,"db");
                //poner disabled a los inputs file mientras se extraen
                $('#imagenes').attr("disabled","disabled");
                $('#archivos').attr("disabled","disabled");
                $('.import').attr("disabled","disabled");
                $('.button').attr("disabled","disabled");
                //una vez copiados extraerlos en su respectiva carpeta

                setTimeout(function(){
                    var imagenes = new targz().extract(app.resourcesDir  + "/assets/backup_images.zip", app.resourcesDir + "/assets/", 
                        function(err){
                        if(err){
                            app.Helper.showAlert({
                                msg : "Ha ocurrido un error por favor inténtelo de nuevo más tarde"
                            })
                        }else{
                            /*app.Helper.showAlert({
                             msg : "Imágenes de importadas correctamente"
                             })*/

                            //delete files
                            app.Helper.deleteFile(app.resourcesDir  + "/assets/backup_images.zip");
                        }
                    });
                    var baseDatos = new targz().extract(app.resourcesDir  + "/assets/backup_db.zip", app.resourcesDir + "/assets/" , 
                        function(err){
                        if(err){
                            app.Helper.showAlert({
                                msg : "Ha ocurrido un error por favor inténtelo de nuevo más tarde"
                            })
                        }else{
                            app.Helper.showAlert({
                             msg : "Base de datos importada corectamente"
                             })

                            //delete files
                            app.Helper.deleteFile(app.resourcesDir  + "/assets/backup_db.zip");
                        }
                    });
                    $('#imagenes').removeAttr("disabled","disabled");
                    $('#archivos').removeAttr("disabled","disabled");
                    $('.import').removeAttr("disabled","disabled");
                    $('.button').removeAttr("disabled","disabled");

                    var salir = app.Helper.showMessage( {
                        msg: 'Kipper se reiniciará'
                    });

                    salir.done(function(){
                        window.location = "index.html";
                    })

                },5000)
            }).fail( function() {
                    $('#imagenes').removeAttr("disabled","disabled");
                    $('#archivos').removeAttr("disabled","disabled");
                    $('.import').removeAttr("disabled","disabled");
                    $('.button').removeAttr("disabled","disabled");
                });

        }else{
            app.Helper.showAlert({
                msg : "Por favor seleccione las imágenes y los archivos de respaldo antes de continuar"
            })
        }
        return false;
    },

    /* =EmpresaAttributes
    ------------------------- */
    empresaAttributes: function() {
        var max = _.max(app.Settings.toJSON(),function(max){ return max.id });
        var settings = app.Settings.toJSON().length;
        return {
            id : ( settings > 0) ? (max.id +1) :  1,
            itemName: 'nombre_empresa',
            itemValue: this.form[0].nombre_empresa.value,
        }
    },

    /* =LogoAttributes
    ------------------------- */
    logoAttributes: function(imgPath) {
        var max = _.max(app.Settings.toJSON(),function(max){ return max.id });
        var settings = app.Settings.toJSON().length;
        console.log(imgPath);
        if(imgPath){
            srcPath = imgPath;
        }else{
            srcPath = "";
        }
        return {
            id : ( settings > 0) ? (max.id +1) :  1,
            itemName: 'logotipo',
            itemValue: srcPath//this.form[0].logotipo.value
        }
    },

    /* =SubmitForm
    ------------------------ */
    submitForm: function( e ) {
        e.preventDefault();
        var self = this;
        if ( this.form[0].nombre_empresa.value !== "") {
            if(this.form[0].nombres.value !== "" &&
                this.form[0].apellidos.value !== "" &&
                this.form[0].correo.value !== "" &&
                this.form[0].username.value !== "" &&
                this.form[0].password.value !== "" &&
                this.form[0].confirmar.value !== ""){

                if ( this.form[0].password.value !== this.form[0].confirmar.value) {
                    alert('Las contraseñas no coinciden.');
                    return;
                }else{

                    if(this.form[0].foto.value.length > 0){
                        console.log( this.form[0].foto );
                        var fotoUser = app.Helper.uploadFile( this.form[0].foto );
                        console.log(fotoUser);
                    }
                    setTimeout(function(){
                       app.Settings.create( self.empresaAttributes(), { wait: true });
                       app.Usuarios.create( self.adminAttributes( fotoUser ), { wait: true });
                    },1000);

                }

                if(this.form[0].logotipo.value.length > 0){
                    var newPath = app.Helper.uploadFile( this.form[0].logotipo );
                  }
                    setTimeout(function(){
                        app.Settings.create( self.logoAttributes( newPath ), { wait: true });
                    },1000)
                
                $('#settings-form').attr("disabled","disabled");
                setTimeout(function(){
                    app.appView.loadLogin();
                },1000);
                app.Helper.showAlert( {
                msg: "Los datos se han ingresado correctamente",
                msgType: 'notice'
                });

            }else{
                alert('Debe proporcionar todos los datos de configuración.');
            }
        } else {
            alert('Debe proporcionar todos los datos de configuración.');
        }
        return false;
    }

});