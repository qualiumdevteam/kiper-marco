/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'togo\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-user' : '&#xe000;',
			'icon-users' : '&#xe001;',
			'icon-cart' : '&#xe003;',
			'icon-key' : '&#xe002;',
			'icon-stats' : '&#xe004;',
			'icon-cog' : '&#xe005;',
			'icon-food' : '&#xe006;',
			'icon-briefcase' : '&#xe007;',
			'icon-home' : '&#xe008;',
			'icon-pencil' : '&#xe009;',
			'icon-image' : '&#xe00a;',
			'icon-camera' : '&#xe00b;',
			'icon-connection' : '&#xe00c;',
			'icon-folder' : '&#xe00d;',
			'icon-tags' : '&#xe00e;',
			'icon-print' : '&#xe00f;',
			'icon-disk' : '&#xe010;',
			'icon-checkmark' : '&#xe011;',
			'icon-close' : '&#xe012;',
			'icon-spam' : '&#xe013;',
			'icon-cloud-upload' : '&#xe014;',
			'icon-cloud-download' : '&#xe015;',
			'icon-earth' : '&#xe016;',
			'icon-power-cord' : '&#xe017;',
			'icon-clock' : '&#xe018;',
			'icon-zoom-in' : '&#xe019;',
			'icon-location' : '&#xe01a;',
			'icon-envelop' : '&#xe01b;',
			'icon-phone' : '&#xe01c;',
			'icon-coin' : '&#xe01d;',
			'icon-qrcode' : '&#xe01e;',
			'icon-calendar' : '&#xe01f;',
			'icon-bell' : '&#xe020;',
			'icon-signup' : '&#xe021;',
			'icon-undo' : '&#xe022;',
			'icon-redo' : '&#xe023;',
			'icon-menu' : '&#xe024;',
			'icon-enter' : '&#xe025;',
			'icon-plus' : '&#xe026;',
			'icon-minus' : '&#xe027;',
			'icon-arrow-left' : '&#xe028;',
			'icon-arrow-right' : '&#xe029;',
			'icon-loop' : '&#xe02a;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};